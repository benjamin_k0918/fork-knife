package com.app.forkandknife;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;

import com.app.forkandknife.global.Constant;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

public class SplashActivity extends AppCompatActivity {

    @BindView(R.id.MainLayout)
    LinearLayout splashLayout;
    private static int SPLASH_TIME = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // Get the current locale
        SharedPreferences preferences;
        preferences = getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
        String localString = preferences.getString("Language", "en");

        if (localString.equals("en")){
            Constant.language = 0;
        }
        else {
            Constant.language = 1;
        }

        // Set the saved locale
        Locale locale = new Locale(localString);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;

        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        new Handler().postDelayed(new Runnable() {


            @Override
            public void run() {

                // run() method will be executed when 3 seconds have passed

                Intent intent = new Intent(SplashActivity.this,AccountManageActivity.class);
                startActivity(intent);

                finish();
            }
        }, SPLASH_TIME);
        ButterKnife.bind(this);
    }


}
