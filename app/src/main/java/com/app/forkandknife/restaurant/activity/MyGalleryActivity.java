package com.app.forkandknife.restaurant.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.app.forkandknife.R;
import com.app.forkandknife.adapter.ImageSelectAdapter;
import com.app.forkandknife.global.Constant;
import com.app.forkandknife.model.ImageFileModel;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyGalleryActivity extends AppCompatActivity {

    private File root;
    private ArrayList<File> fileList = new ArrayList<File>();

    @BindView(R.id.gridview)
    GridView gridView;

    ImageSelectAdapter adapter ;

    ArrayList<ImageFileModel> imageFileModelArrayList = new ArrayList<>();
    ArrayList<String> selectedFileList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_gallery);
        ButterKnife.bind(this);


   //     readFromGallery();
        root = new File(Environment.getExternalStorageDirectory()
                .getAbsolutePath());
        getfile(root);

        setListView();
    }

    @OnClick(R.id.btn_confirm)
    public void onConfirm(){
        selectedFileList =new ArrayList<>();
        for (int i = 0;i<imageFileModelArrayList.size();i++)
        {
            if (imageFileModelArrayList.get(i).isSelection())
            {
                selectedFileList.add(imageFileModelArrayList.get(i).getUrl());
            }
        }
        if (selectedFileList.size() > 5) {
            Toast.makeText(getApplicationContext(),this.getString(R.string.MSG_RESTAURANT_FIVE_IMAGES),Toast.LENGTH_LONG).show();
        }
        else {
            Intent intent= new Intent();
            intent.putExtra(Constant.ImageArray,selectedFileList);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    @OnClick(R.id.btn_cancel)
    public  void onCancelClicked(){
        finish();
    }



    private void setListView(){

        for (int i = 0;i<fileList.size();i++)
        {
            imageFileModelArrayList.add(new ImageFileModel(fileList.get(i).getAbsolutePath(),false));
        }
        adapter = new ImageSelectAdapter(this,imageFileModelArrayList);
        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (imageFileModelArrayList.get(position).isSelection())
                {
                    imageFileModelArrayList.get(position).setSelection(false);
                }else {
                    imageFileModelArrayList.get(position).setSelection(true);
                }
                adapter.notifyDataSetChanged();
            }
        });
    }

    public ArrayList<File> getfile(File dir) {
        File listFile[] = dir.listFiles();
        if (listFile != null && listFile.length > 0) {
            for (int i = 0; i < listFile.length; i++) {

                if (listFile[i].isDirectory()) {
                    if (listFile[i].getName().endsWith(".png")
                            || listFile[i].getName().endsWith(".jpg")
                            || listFile[i].getName().endsWith(".jpeg")
                            || listFile[i].getName().endsWith(".gif"))

                    {
                        fileList.add(listFile[i]);
                    }
                    getfile(listFile[i]);


                } else {
                    if (listFile[i].getName().endsWith(".png")
                            || listFile[i].getName().endsWith(".jpg")
                            || listFile[i].getName().endsWith(".jpeg")
                            || listFile[i].getName().endsWith(".gif"))

                    {
                        fileList.add(listFile[i]);
                    }
                }

            }
        }
        return fileList;
    }
}
