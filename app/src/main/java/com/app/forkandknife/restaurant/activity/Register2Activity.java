package com.app.forkandknife.restaurant.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.app.forkandknife.global.Constant;
import com.app.forkandknife.global.GPSTracker;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.app.forkandknife.R;
import com.app.forkandknife.adapter.CuisineAdapter;
import com.app.forkandknife.adapter.FeatureAdapter;
import com.app.forkandknife.custom.MyMapView;
import com.app.forkandknife.model.CuisineData;
import com.app.forkandknife.model.EventBusMessage;
import com.app.forkandknife.model.FeatureData;
import com.app.forkandknife.model.UserData;
import com.app.forkandknife.model.UserInfo;
import com.app.forkandknife.webservice.RestClient;

import org.greenrobot.eventbus.EventBus;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.forkandknife.model.UserInfo.getInstance;
import static com.facebook.FacebookSdk.getApplicationContext;


public class Register2Activity extends AppCompatActivity implements OnMapReadyCallback {

    MyMapView mapView;
    GoogleMap map;

    GPSTracker gps;

    @BindView(R.id.radio_buffet)
    RadioButton radioBuffet;

    @BindView(R.id.radio_perrequest)
    RadioButton radioPerrequest;

    @BindView(R.id.tv_buffet)
    TextView tvBuffet;

    @BindView(R.id.tv_request)
    TextView tvRequest;

    WindowManager.LayoutParams  layoutParams;

    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 1001;
    private static final String TAG = "MapsActivity";
    private String[] permissions = new String[] {android.Manifest.permission.ACCESS_FINE_LOCATION};

    RecyclerView.LayoutManager layoutManagerCuisine;
    RecyclerView.LayoutManager layoutManagerFeature;

    @BindView(R.id.recyclerView_feature)
    RecyclerView recyclerViewFeature;

    @BindView(R.id.recyclerView_cuisine)
    RecyclerView recyclerViewCuisine;

    @BindView(R.id.editText_time)
    EditText editTextTime;

    @BindView(R.id.editText_end_time)
    EditText editTextEndTime;

    ArrayList<CuisineData> cuisineDataArrayList = new ArrayList<>();
    ArrayList<FeatureData> featureDataArrayList;
    CuisineAdapter cuisineAdapter;
    FeatureAdapter featureAdapter;
    private static final int MY_PERMISSIONS_REQUEST_COARSE = 1002;
    private String[] coarse_permissions = new String[] {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
    boolean isMsgShown = false;

    MarkerOptions markerOptions;
    MarkerOptions currentMakerOptions;

    LatLng currentUserLocation = new LatLng(0,0);
    LatLng currentRestauratnLocation = new LatLng(0,0);
    BitmapDescriptor icon;


    HashMap<String,Object> newMap  = new HashMap<>();

    @BindView(R.id.editText_cost)
    EditText editTextCost;

    ProgressDialog progressDialog ;

    HashMap<String, Object> userData = null;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register2);
        ButterKnife.bind(this);
        checkPermissions();
        // get HashMap data from previous screen
        userData = (HashMap<String, Object>) getIntent().getSerializableExtra("register_data");

        // Initialize the Preference
        preferences = getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", android.content.Context.MODE_PRIVATE);

        // Initialize the ProgressDialog
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        initListener();

        layoutParams = new WindowManager.LayoutParams();
        layoutParams.gravity = Gravity.BOTTOM;
        MapsInitializer.initialize(getApplicationContext());

        getAllCuisineList();
//        markerOptions = new MarkerOptions().position(new LatLng(40.0008, 124.3545))
//                .title("Current Location")
//                .snippet("Thinking of finding some thing...")
//                .icon(icon);

        mapView = (MyMapView) findViewById(R.id.mapView);
        // Gets the MapView from the XML layout and creates it
        mapView.onCreate(savedInstanceState);



        // Gets the MapView from the XML layout and creates it


        checkLocationPermissions();
        setRecyclerView();
    }

    // Get current location
    public void getCurrentLocation() {
        // Permission has already been granted
        gps = new GPSTracker(getApplicationContext(), this);

        // Check if GPS enabled
        if (gps.canGetLocation()) {
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();

            UserInfo.getInstance().setLatitude(String.valueOf(latitude));
            UserInfo.getInstance().setLongtitude(String.valueOf(longitude));
        }
        else {
            // Can't get location.
            // GPS or network is not enabled.
            // Ask user to enable GPS/network in settings.
            gps.showSettingsAlert();
        }
    }


    // Check if location permission is granted
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void checkPermissions(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                requestPermissions(coarse_permissions, MY_PERMISSIONS_REQUEST_COARSE);
            }
        } else { // Permission granted
            getCurrentLocation();
        }
    }




    private void initListener(){

        tvBuffet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        radioBuffet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radioPerrequest.setChecked(false);
            }
        });

        radioPerrequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radioBuffet.setChecked(false);
            }
        });
    }


    @OnClick(R.id.iv_select_time)
    public void onEditTextTimeClicked(){
        // TODO Auto-generated method stub
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                editTextTime.setText(  formatNumber(selectedHour) + ":" + formatNumber(selectedMinute));
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.getWindow().setAttributes(layoutParams);

        mTimePicker.show();
    }

    @OnClick(R.id.iv_select_end_time)
    public void editTextEndTimeClicked(){
        // TODO Auto-generated method stub
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                editTextEndTime.setText( formatNumber(selectedHour) + ":" + formatNumber(selectedMinute));
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.getWindow().setAttributes(layoutParams);

        mTimePicker.show();
    }

    private void checkLocationPermissions(){
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, permissions, MY_PERMISSIONS_REQUEST_LOCATION);
            }
        } else {
            // Permission has already been granted
            mapView.getMapAsync(this);
        }
    }


    @OnClick(R.id.iv_back)
    public void onBackClicked(){
        Intent intent = new Intent(Register2Activity.this,Register1Activity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Register2Activity.this,Register1Activity.class);
        startActivity(intent);
        finish();
    }


    private int[] getCuisineList (){
        ArrayList<Integer> integers = new ArrayList<>();
        for (int i = 0;i<cuisineDataArrayList.size();i++)
        {
            if (cuisineDataArrayList.get(i).getSelection())
            {
                integers.add(cuisineDataArrayList.get(i).getId());
            }
        }
        int[] cuisineArray = new int[integers.size()];
        if (!integers.isEmpty())
        {
            for (int i = 0;i<integers.size();i++)
            {
                cuisineArray[i] = integers.get(i);
            }
        }

        return  cuisineArray;

    }

    private void getFeatures(){
        if (featureDataArrayList.get(0).isSelected())
        {
            newMap.put("allowSmoking",true);
        }else {
            newMap.put("allowSmoking",false);
        }
        if (featureDataArrayList.get(1).isSelected())
        {

            newMap.put("acceptVisa",true);
        }else {
            newMap.put("acceptVisa",false);
        }
        if (featureDataArrayList.get(2).isSelected())
        {

            newMap.put("dressCode",true);
        }else {
            newMap.put("dressCode",false);
        }
        if (featureDataArrayList.get(3).isSelected())
        {

            newMap.put("outdoorSeating",true);
        }else {
            newMap.put("outdoorSeating",false);

        }
        if (featureDataArrayList.get(4).isSelected())
        {

            newMap.put("parking",true);
        }else {
            newMap.put("parking",false);

        }
    }

    private boolean isAllValid(){
        if (!(editTextEndTime.getText().toString().trim().isEmpty() || editTextTime.getText().toString().trim().isEmpty() ||editTextCost.getText().toString().isEmpty()))
        {
            if (currentRestauratnLocation != new LatLng(0,0))
            {
                newMap =  UserInfo.getInstance().getNewRestMap();
                newMap.put("cuisines",getCuisineList());
                newMap.put("cost",editTextCost.getText().toString().trim());
                newMap.put("openTime",editTextTime.getText().toString().trim());
                newMap.put("closeTime",editTextEndTime.getText().toString().trim());
                newMap.put("latitude",currentRestauratnLocation.latitude);
                newMap.put("longitude",currentRestauratnLocation.longitude);

                newMap.put("ownerName", userData.get("ownerName").toString());
                newMap.put("password", userData.get("password").toString());
                newMap.put("name", userData.get("name").toString());
                newMap.put("arabicName", userData.get("arabicName").toString());
                newMap.put("email", userData.get("email").toString());
                newMap.put("mobile", userData.get("mobile").toString());
                newMap.put("deviceType", 2);

                newMap.put("requestType", 1);

                if (Constant.token.isEmpty())
                {
                    Constant.token = preferences.getString("token","");
                }
                newMap.put("token", Constant.token);

                getFeatures();
                return true;
            }
            else {
                Toast.makeText(this,getString(R.string.invalid_location),Toast.LENGTH_LONG).show();

                return false;
            }
        }else {
            Toast.makeText(this,getString(R.string.input_all),Toast.LENGTH_LONG).show();
            return false;
        }
    }

    @OnClick(R.id.btn_submit)
    public void onSubmitClicked(){
        if (isAllValid())
        {
            registerRestaurant();
        }

    }

    private void registerRestaurant(){
        RestClient.getInstance().initRestClient(RestClient.API_URL).createRestaurant(newMap).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                progressDialog.dismiss();
                Log.d(TAG,response.toString());
                JsonObject resultObj = response.body();
                if (resultObj != null)
                {
                    if (resultObj.has("data") && resultObj.get("message").toString().contains("success")){
                        JsonObject userJson = resultObj.getAsJsonObject("data");
                        Gson gson = new Gson();
                        Type type = new TypeToken<UserData>() {}.getType();

                        Intent intent = new Intent(Register2Activity.this,LoginActivity.class);
                        startActivity(intent);
                        finish();

                    }
                    else {
                        Toast.makeText(Register2Activity.this, resultObj.get("message").toString(), Toast.LENGTH_LONG);
                    }
                }else {
                    Toast.makeText(Register2Activity.this, response.message(), Toast.LENGTH_LONG);
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(Register2Activity.this, t.getLocalizedMessage(), Toast.LENGTH_LONG);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_COARSE: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    getCurrentLocation();
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "Permission denied to access your location", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    @OnClick(R.id.tv_request)
    public void tvRequest(){
        radioBuffet.setChecked(false);
        radioPerrequest.setChecked(true);
    }

    @OnClick(R.id.tv_buffet)
    public void onBuffetClicked(){
        radioPerrequest.setChecked(false);
        radioBuffet.setChecked(true);
    }

    private void setRecyclerView(){

        layoutManagerCuisine = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        layoutManagerFeature = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        recyclerViewCuisine.setLayoutManager(layoutManagerCuisine);
        recyclerViewFeature.setLayoutManager(layoutManagerFeature);

        cuisineAdapter = new CuisineAdapter(this,cuisineDataArrayList);
        recyclerViewCuisine.setAdapter(cuisineAdapter);



        featureDataArrayList = new ArrayList<>();
        featureDataArrayList.add(new FeatureData("Allow Smoking"));
        featureDataArrayList.add(new FeatureData("Accept Visa"));
        featureDataArrayList.add(new FeatureData("Dress Code"));
        featureDataArrayList.add(new FeatureData("Outdoor Seating"));
        featureDataArrayList.add(new FeatureData("Valet Parking"));

        featureAdapter  = new FeatureAdapter(this,featureDataArrayList);
        recyclerViewFeature.setAdapter(featureAdapter);
    }

    private void getAllCuisineList() {
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();
        RestClient.getInstance().initRestClient(RestClient.API_URL).getAllCuisine().enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject resultObj = response.body();

                progressDialog.dismiss();

                if (resultObj.has("data") && resultObj.get("message").toString().trim().contains("success")) {
                    JsonArray restaurantJson = resultObj.getAsJsonObject("data").getAsJsonArray("rows");

                    Gson gson = new Gson();
                    Type type = new TypeToken<List<CuisineData>>() {
                    }.getType();
                    cuisineDataArrayList = gson.fromJson(restaurantJson, type);


                    //     UserInfo.getInstance().setPointData(cuisineDataArrayList);
                    setRecyclerView();
                    //    EventBus.getDefault().post(new EventBusMessage(EventBusMessage.MessageType.TO_VERIFY_SMS_FRAGMENT));
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    private String formatNumber(int num){
        String str = String.format("%02d", num);
        return  str;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
//        googleMap.addMarker(markerOptions);
        map.getUiSettings().setMyLocationButtonEnabled(true);
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            map.setMyLocationEnabled(true);
        }

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {

                 currentUserLocation =new LatLng(Double.parseDouble(UserInfo.getInstance().getLatitude()),Double.parseDouble(UserInfo.getInstance().getLongtitude()));
                if (currentRestauratnLocation != latLng)
                {
                    map.clear();
                        currentRestauratnLocation = latLng;
                    markerOptions = new MarkerOptions().position(new LatLng(currentRestauratnLocation.latitude, currentRestauratnLocation.longitude))
                   .icon(icon);
                    map.addMarker(markerOptions);
                    currentMakerOptions = new MarkerOptions().position(new LatLng(currentUserLocation.latitude, currentUserLocation.longitude))
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.my_location));
                    map.addMarker(currentMakerOptions);

                }else {
                    map.clear();
                    currentMakerOptions = new MarkerOptions().position(new LatLng(currentUserLocation.latitude, currentUserLocation.longitude))
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.my_location));
                    map.addMarker(currentMakerOptions);
                }

            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}
