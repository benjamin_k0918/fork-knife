package com.app.forkandknife.restaurant.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.app.forkandknife.R;
import com.app.forkandknife.model.UserInfo;
import com.app.forkandknife.webservice.RestClient;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends AppCompatActivity {

    @BindView(R.id.editText_old_password)
    EditText oldPwdEditText;

    @BindView(R.id.editText_password)
    EditText newPwdEditText;

    @BindView(R.id.editText_confirm_password)
    EditText confirmPwdEditText;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
    }

    @OnClick(R.id.btn_change)
    public void onChangeClicked(){
        if (oldPwdEditText.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, getString(R.string.enter_old_password), Toast.LENGTH_SHORT).show();
            return;
        }

        if (newPwdEditText.getText().toString().trim().length() < 4) {
            Toast.makeText(this, getString(R.string.MSG_INVALID_PASSWORD_LENGTH), Toast.LENGTH_SHORT).show();
            return;
        }

        if (!newPwdEditText.getText().toString().trim().equals(confirmPwdEditText.getText().toString().trim())) {
            Toast.makeText(this, getString(R.string.not_match_password), Toast.LENGTH_SHORT).show();
        }

        // Call api to change the password
        progressDialog.show();

        HashMap<String,Object> map = new HashMap<>();
        map.put("userId", UserInfo.getInstance().getRestOwnerDate().getId());
        map.put("oldPassword",oldPwdEditText.getText().toString().trim());
        map.put("password",newPwdEditText.getText().toString().trim());
        RestClient.getInstance().initRestClient(RestClient.API_URL).changeRestaurantPassword(map).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                progressDialog.dismiss();

                JsonObject resultObj = response.body();
                if (resultObj!= null)
                {
                    if (resultObj.get("message").toString().trim().contains("success")) {
                        // go to SMS verification page
                        gotoSMSVerificationPage();
                    }
                    else{
                        Toast.makeText(ChangePasswordActivity.this, resultObj.get("message").toString().trim(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(ChangePasswordActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void gotoSMSVerificationPage(){
        Intent intent = new Intent(ChangePasswordActivity.this, SmsVerifyActivity.class);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.iv_back)
    public void onBackClicked(){
        finish();
    }
}
