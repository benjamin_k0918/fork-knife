package com.app.forkandknife.restaurant.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.app.forkandknife.AccountManageActivity;
import com.app.forkandknife.R;
import com.app.forkandknife.global.Constant;
import com.app.forkandknife.global.GPSTracker;
import com.app.forkandknife.global.Utils;
import com.app.forkandknife.model.EventBusMessage;
import com.app.forkandknife.model.LoginDataModel;
import com.app.forkandknife.model.LoginRequest;
import com.app.forkandknife.model.RestOwnerDate;
import com.app.forkandknife.model.SmsVerifyModel;
import com.app.forkandknife.model.UserInfo;
import com.app.forkandknife.webservice.RestClient;

import org.androidannotations.annotations.AfterTextChange;
import org.greenrobot.eventbus.EventBus;

import java.lang.reflect.Type;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.tv_forgot_password)
    TextView tvForgotPassword;

    @BindView(R.id.tv_new_account)
    TextView tvNewAccount;

    SharedPreferences preferences;


    @BindView(R.id.editText_mobile)
    EditText editTextMobile;

    @BindView(R.id.editText_password)
    EditText editTextPassword;
    ProgressDialog progressDialog;
    GPSTracker gps;

    RestOwnerDate restOwnerDate = new RestOwnerDate();
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 1001;
    private static final int MY_PERMISSIONS_REQUEST_COARSE = 1002;
    private static final String TAG = "MapsActivity";
    private String[] permissions = new String[] {android.Manifest.permission.ACCESS_FINE_LOCATION};
    private String[] coarse_permissions = new String[] {Manifest.permission.ACCESS_COARSE_LOCATION};
    boolean isMsgShown = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        progressDialog  = new ProgressDialog(this);
        checkPermission();

        preferences =getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", android.content.Context.MODE_PRIVATE);

        setPrefix();
    }


    private void setPrefix(){
        editTextMobile.setText("966");
   //     Selection.setSelection(editTextMobile.getText(), editTextMobile.getText().length());

        editTextMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().startsWith("966")){
//                    editTextMobile.setText("966");
//                    Selection.setSelection(editTextMobile.getText(), editTextMobile.getText().length());

                }
            }
        });
    }

    private void setPreference() {

    }

    private void checkPermission(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, coarse_permissions , MY_PERMISSIONS_REQUEST_COARSE);
            }
        } else {
            // Permission has already been granted
            gps = new GPSTracker(getApplicationContext(), this);

            // Check if GPS enabled
            if (gps.canGetLocation()) {
                double latitude = gps.getLatitude();
                double longitude = gps.getLongitude();
            } else {
                // Can't get location.
                // GPS or network is not enabled.
                // Ask user to enable GPS/network in settings.
                gps.showSettingsAlert();
            }
        }
    }



    @OnClick(R.id.iv_back)
    public void onBackClicked(){
        Intent intent = new Intent(LoginActivity.this, AccountManageActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(LoginActivity.this, AccountManageActivity.class);
        startActivity(intent);
        finish();    }

    @OnClick(R.id.relativeLayout_login)
    public void onLoginClicked(){

        isMsgShown = false;
        if (isAllValid()) {
            LoginRequest.getInstance().setAccountType(0);
            LoginRequest.getInstance().setMobile(editTextMobile.getText().toString().trim());
            LoginRequest.getInstance().setDeviceType(2);
            LoginRequest.getInstance().setPassword(editTextPassword.getText().toString().trim());
            LoginRequest.getInstance().setUserType(2);
            checkLocationPermissionsAndCallAPI();

        }

    }

    @OnClick(R.id.tv_forgot_password)
    public void onForgotPasswordClicked(){
        Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.tv_new_account)
    public void onNewAccountClicked(){
        Intent intent = new Intent(LoginActivity.this, Register1Activity.class);
        startActivity(intent);
    }

    private void checkLocationPermissionsAndCallAPI(){

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            Toast.makeText(getApplicationContext(),getString(R.string.error_access_location),Toast.LENGTH_LONG).show();
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, coarse_permissions , MY_PERMISSIONS_REQUEST_COARSE);
            }
        } else {
            // Permission has already been granted
            gps = new GPSTracker(getApplicationContext(),this);

            // Check if GPS enabled
            if (gps.canGetLocation()) {

                progressDialog.setMessage(getString(R.string.loading));
                progressDialog.show();
                double latitude = gps.getLatitude();
                double longitude = gps.getLongitude();
                if (Constant.token.isEmpty())
                {
                    Constant.token = preferences.getString("token","");
                }

                LoginRequest.getInstance().setLatitude(String.valueOf(UserInfo.getInstance().getLatitude()));
                LoginRequest.getInstance().setLongtitude(String.valueOf(UserInfo.getInstance().getLongtitude()));
                LoginRequest.getInstance().setToken(Constant.token);

                RestClient.getInstance().initRestClient(RestClient.API_BASE_URL).login(LoginRequest.getInstance()).enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        //'      Log.d("3e434r3434",response.body().toString());
                        JsonObject resultObj = response.body();
                        progressDialog.dismiss();

                        //   Toast.makeText(getActivity(),resultObj,Toast.LENGTH_LONG).show();
                        //    Toast.makeText(getActivity(),resultObj.get("message").toString(),Toast.LENGTH_LONG).show();

                        if (response.code() == 200)
                        {
                            JsonObject userJson = resultObj.getAsJsonObject("data");
                            Gson gson = new Gson();
                            Type type = new TypeToken<RestOwnerDate>() {
                            }.getType();
                            restOwnerDate = gson.fromJson(userJson, type);
                            UserInfo.getInstance().setRestOwnerDate(restOwnerDate);

                            SharedPreferences preferences = getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", android.content.Context.MODE_PRIVATE);
                            preferences = getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", android.content.Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putString("login", "2");
                            editor.putString("id", String.valueOf(UserInfo.getInstance().getRestOwnerDate().getId()));
                            editor.putString("account_type", String.valueOf(UserInfo.getInstance().getRestOwnerDate().getAccountType()));
                            editor.putString("status", String.valueOf(UserInfo.getInstance().getRestOwnerDate().getStatus()));
                            editor.putString("user_type", String.valueOf(2));
                            editor.putString("access_token", UserInfo.getInstance().getRestOwnerDate().getAccessToken());
                            editor.putString("email", UserInfo.getInstance().getRestOwnerDate().getEmail());
                            editor.putString("mobile", UserInfo.getInstance().getRestOwnerDate().getRestaurant().getMobile());
                            editor.putString("name", UserInfo.getInstance().getRestOwnerDate().getName());
                            if (UserInfo.getInstance().getRestOwnerDate().getUserImage() != null) {
                                editor.putString("image", UserInfo.getInstance().getRestOwnerDate().getUserImage().toString());
                            }
                            editor.commit();
                            Intent intent = new Intent(LoginActivity.this, SmsVerifyActivity.class);
                            startActivity(intent);
                            finish();
                        }else if (response.code() == 422){
                            if (!isMsgShown)
                            {
                                Toast.makeText(getApplicationContext(),getString(R.string.invalid_mobile_pass),Toast.LENGTH_LONG).show();
                            }else {
                                isMsgShown = false;
                            }

                        }

                    }
                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(),getApplication().getString(R.string.no_internet),Toast.LENGTH_LONG).show();

                    }
                });

                // \n is for new line
            } else {
                // Can't get location.
                // GPS or network is not enabled.
                // Ask user to enable GPS/network in settings.
                gps.showSettingsAlert();
            }
        }
    }



    private boolean isAllFilled(){
        if (!editTextMobile.getText().toString().trim().isEmpty() && !editTextPassword.getText().toString().trim().isEmpty() &&isPasswordLengthRight() )
            return  true;
        else if (editTextMobile.getText().toString().trim().length()<4){
            if (!isMsgShown)
            {
                Toast.makeText(this,this.getString(R.string.enter_phone),Toast.LENGTH_SHORT).show();
                isMsgShown = true;
            }

            return false;
        }else {
            if (!isMsgShown)
            {
                Toast.makeText(this,this.getString(R.string.enter_password),Toast.LENGTH_SHORT).show();
                isMsgShown = true;
            }
            return false;

        }
    }


    private boolean isAllValid(){
        if (isAllFilled() && Utils.isMobileValid(editTextMobile.getText().toString().trim()))
        {
            return true;
        }else if (!isPasswordLengthRight()){
            if (!isMsgShown)
            {
                Toast.makeText(this,this.getString(R.string.MSG_INVALID_PASSWORD_LENGTH),Toast.LENGTH_SHORT).show();
                isMsgShown = true;
            }

            return false;
        }else if (!Utils.isEmailValid(editTextMobile.getText().toString().trim()))
        {
            if (!isMsgShown)
            {
                Toast.makeText(this,this.getString(R.string.enter_phone),Toast.LENGTH_SHORT).show();
                isMsgShown = true;
            }
            return false;

        }else {
            return false;
        }
    }
    private boolean isPasswordLengthRight(){
        if (editTextPassword.getText().toString().trim().length()<4)
        {
            return false;
        }else {
            return true;
        }
    }



}
