package com.app.forkandknife.restaurant.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.app.forkandknife.AccountManageActivity;
import com.app.forkandknife.global.Constant;
import com.facebook.login.LoginManager;
import com.google.gson.JsonObject;
import com.app.forkandknife.R;
import com.app.forkandknife.adapter.TimeSelectionAdapter;
import com.app.forkandknife.global.Utils;
import com.app.forkandknife.model.TimeModel;
import com.app.forkandknife.model.UserInfo;
import com.app.forkandknife.webservice.RestClient;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class UpdateTimeFragment extends Fragment {

    ArrayList<TimeModel> sundayModel = new ArrayList<>();
    ArrayList<TimeModel> mondayModel = new ArrayList<>();
    ArrayList<TimeModel> tuesdayModel = new ArrayList<>();
    ArrayList<TimeModel> wednesdayModel = new ArrayList<>();
    ArrayList<TimeModel> thursdayModels = new ArrayList<>();
    ArrayList<TimeModel> fridayModels = new ArrayList<>();
    ArrayList<TimeModel> saturdayModels = new ArrayList<>();

    @BindView(R.id.recyclerView_Friday)
    RecyclerView recyclerViewFriday;

    @BindView(R.id.recyclerView_Monday)
    RecyclerView recyclerViewMonday;

    @BindView(R.id.recyclerView_Tuesday)
    RecyclerView recyclerViewTuesday;

    @BindView(R.id.recyclerView_Wednesday)
    RecyclerView recyclerViewWednesday;

    @BindView(R.id.recyclerView_Thursday)
    RecyclerView recyclerViewThursday;

    @BindView(R.id.recyclerView_Saturday)
    RecyclerView recyclerViewSaturday;

    @BindView(R.id.recyclerView_Sunday)
    RecyclerView recyclerViewSunday;

    TimeSelectionAdapter sundayAdapter,mondayAdapter,tuesdayAdapter,
    wednesdayAdapter, thursdayAdapter,fridayAdapter,saturdayAdapter;

    RecyclerView.LayoutManager layoutManagerSunday,layoutManagerMonday,layoutManagerTuesday,layoutManagerWednesday,
    layoutManagerThursday,
    layoutManagerFriday,
    layoutManagerSaturday;

    String sunday = "";
    String monday = "";
    String tuesday= "";
    String wednesdday = "";
    String thursday = "";
    String friday = "";
    String saturday = "";
    ProgressDialog progressDialog;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View containerView=inflater.inflate(R.layout.fragment_update_time, container, false);
        ButterKnife.bind(this,containerView);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.loading));
        initLayoutManagers();
        initTimeModels();
        setRecyclerView();
//        Toast.makeText(getActivity(),String.valueOf(UserInfo.getInstance().getRestOwnerDate().getRestaurant().getSaturday().length()),Toast.LENGTH_LONG).show();
        return containerView;

    }

    private void initLayoutManagers(){
        layoutManagerSunday = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        layoutManagerMonday = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        layoutManagerTuesday = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        layoutManagerWednesday = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        layoutManagerThursday = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        layoutManagerFriday = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        layoutManagerSaturday = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

        recyclerViewSaturday.setLayoutManager(layoutManagerSaturday);
        recyclerViewSunday.setLayoutManager(layoutManagerSunday);
        recyclerViewMonday.setLayoutManager(layoutManagerMonday);
        recyclerViewTuesday.setLayoutManager(layoutManagerTuesday);
        recyclerViewWednesday.setLayoutManager(layoutManagerWednesday);
        recyclerViewThursday.setLayoutManager(layoutManagerThursday);
        recyclerViewFriday.setLayoutManager(layoutManagerFriday);

    }

    public void setRecyclerView(){
        sundayAdapter = new TimeSelectionAdapter(getActivity(),sundayModel,UserInfo.getInstance().getRestOwnerDate().getRestaurant().getSunday());
        mondayAdapter = new TimeSelectionAdapter(getActivity(),mondayModel,UserInfo.getInstance().getRestOwnerDate().getRestaurant().getMonday());
        tuesdayAdapter = new TimeSelectionAdapter(getActivity(),tuesdayModel,UserInfo.getInstance().getRestOwnerDate().getRestaurant().getTuesday());
        wednesdayAdapter = new TimeSelectionAdapter(getActivity(),wednesdayModel,UserInfo.getInstance().getRestOwnerDate().getRestaurant().getWednesday());
        thursdayAdapter = new TimeSelectionAdapter(getActivity(),thursdayModels,UserInfo.getInstance().getRestOwnerDate().getRestaurant().getThursday());
        fridayAdapter = new TimeSelectionAdapter(getActivity(),fridayModels,UserInfo.getInstance().getRestOwnerDate().getRestaurant().getFriday());
        saturdayAdapter = new TimeSelectionAdapter(getActivity(),saturdayModels,UserInfo.getInstance().getRestOwnerDate().getRestaurant().getSaturday());

        recyclerViewSunday.setAdapter(sundayAdapter);
        recyclerViewMonday.setAdapter(mondayAdapter);
        recyclerViewTuesday.setAdapter(tuesdayAdapter);
        recyclerViewWednesday.setAdapter(wednesdayAdapter);
        recyclerViewThursday.setAdapter(thursdayAdapter);
        recyclerViewFriday.setAdapter(fridayAdapter);
        recyclerViewSaturday.setAdapter(saturdayAdapter);

    }

    private void initTimeModels(){
        sundayModel = displayTimeSlots();
        tuesdayModel = displayTimeSlots();
        mondayModel =displayTimeSlots();
        wednesdayModel= displayTimeSlots();
        thursdayModels=  displayTimeSlots();
        fridayModels  = displayTimeSlots();
        saturdayModels=  displayTimeSlots();
    }

    private ArrayList<TimeModel> displayTimeSlots(){

        ArrayList<TimeModel> timeModels= new ArrayList<>();
        int x = 5; //minutes interval
        int tt = 0; // start time
        //   var ap = ['AM', 'PM']; // AM-PM
//loop to increment the time and push results in array
        for (int i=0;tt<24*60; i++) {
            int hh = tt/60; // getting hours of day in 0-24 format
            int mm = (tt%60); // getting minutes of the hour in 0-55 format
            timeModels.add( new TimeModel(Utils.formatNumber(hh%24) + ":" + Utils.formatNumber(mm),false));
            //   times[i] = ("0" + (hh % 12)).slice(-2) + ':' + ("0" + mm).slice(-2) + ap[Math.floor(hh/12)]; // pushing data in array in [00:00 - 12:00 AM/PM format]
            tt = tt + 15;
        }
        return timeModels;
        //  Toast.makeText(getActivity(),String.valueOf(times.size()),Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.btn_update)
    public void onUpdate(){
        for (int i = 0;i<sundayModel.size();i++)
        {
            if (sundayModel.get(i).isSelection())
            {
                sunday+= "1";

            }else {
                sunday+= "0";

            }
        }
        for (int i = 0;i<mondayModel.size();i++)
        {
            if (mondayModel.get(i).isSelection())
            {
                monday+= "1";

            }else {
                monday+= "0";

            }
        }
        for (int i = 0;i<tuesdayModel.size();i++)
        {
            if (tuesdayModel.get(i).isSelection())
            {
                tuesday+= "1";

            }else {
                tuesday+= "0";

            }
        }
        for (int i = 0;i<wednesdayModel.size();i++)
        {
            if (wednesdayModel.get(i).isSelection())
            {
                wednesdday+= "1";

            }else {
                wednesdday+= "0";

            }
        }
        for (int i = 0;i<thursdayModels.size();i++)
        {
            if (thursdayModels.get(i).isSelection())
            {
                thursday+= "1";

            }else {
                thursday+= "0";

            }
        }
        for (int i = 0;i<fridayModels.size();i++)
        {
            if (fridayModels.get(i).isSelection())
            {
                friday+= "1";

            }else {
                friday+= "0";

            }
        }
        for (int i = 0;i<saturdayModels.size();i++)
        {
            if (saturdayModels.get(i).isSelection())
            {
                saturday+= "1";

            }else {
                saturday+= "0";

            }
        }
        HashMap<String,Object> map = new HashMap<>();
        map.put("saturday",saturday);
        map.put("sunday",sunday);
        map.put("monday",monday);
        map.put("tuesday",tuesday);
        map.put("wednesday",wednesdday);
        map.put("thursday",thursday);
        map.put("friday",friday);
        map.put("restaurantId", UserInfo.getInstance().getRestOwnerDate().getRestaurant().getId());
        progressDialog.show();
        String auth = "Bearer " + UserInfo.getInstance().getRestOwnerDate().getAccessToken();

        RestClient.getInstance().initRestClient(RestClient.API_URL).updateRestHours(auth,map).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                progressDialog.dismiss();
                if (response.code() == 200)
                {
                    UserInfo.getInstance().getRestOwnerDate().getRestaurant().setSunday(sunday);
                    UserInfo.getInstance().getRestOwnerDate().getRestaurant().setMonday(monday);
                    UserInfo.getInstance().getRestOwnerDate().getRestaurant().setTuesday(tuesday);
                    UserInfo.getInstance().getRestOwnerDate().getRestaurant().setWednesday(wednesdday);
                    UserInfo.getInstance().getRestOwnerDate().getRestaurant().setThursday(thursday);
                    UserInfo.getInstance().getRestOwnerDate().getRestaurant().setFriday(friday);
                    UserInfo.getInstance().getRestOwnerDate().getRestaurant().setSaturday(saturday);
                    setFragment(new UpdateDetailFragment());
                }else if (response.code() == 401)
                {
                    SharedPreferences preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor =      preferences.edit();
                    editor.putString("login", "0");
                    editor.commit();
                    if (!Constant.fbToken.isEmpty())
                    {
                        LoginManager.getInstance().logOut();
                    }
                    Intent intent = new Intent(getActivity(), AccountManageActivity.class);
                    startActivity(intent);

                    getActivity().finish();
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    @OnClick(R.id.iv_back)
    public void onBackClicked(){
        setFragment(new UpdateDetailFragment());
    }

    @Override
    public void onResume() {
        super.onResume();

        if(getView() == null){
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    // handle back button's click listener
                    setFragment(new UpdateDetailFragment());
                    return true;
                }
                return false;
            }
        });
    }
    private void setFragment(Fragment fragment){
        FragmentManager fragmentManager= getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =   fragmentManager.beginTransaction();
        Bundle sendData = new Bundle();
        fragmentTransaction.replace(R.id.MainFrame,fragment,null);
        fragmentTransaction.commit();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
