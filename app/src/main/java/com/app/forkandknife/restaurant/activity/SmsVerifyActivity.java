package com.app.forkandknife.restaurant.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import com.google.gson.JsonObject;
import com.app.forkandknife.R;
import com.app.forkandknife.global.Constant;
import com.app.forkandknife.model.SmsVerifyModel;
import com.app.forkandknife.model.UserInfo;
import com.app.forkandknife.user.activity.MainActivity;
import com.app.forkandknife.user.fragment.ProfileFragment;
import com.app.forkandknife.webservice.RestClient;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SmsVerifyActivity extends AppCompatActivity {

    @BindView(R.id.editText_sms_code)
    EditText editTextSMSCode;


    ProgressDialog progressDialog ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms_verify);
        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(getApplicationContext());
        progressDialog.setMessage(getString(R.string.loading));

        sendVerifyMsg();


    }

    private void sendVerifyMsg(){


        RestClient.getInstance().initRestClient(RestClient.API_SMS_URL).sendSms(Constant.loginDataMode.getId()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject resultObj = response.body();


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    @OnClick(R.id.btn_verify)
    public void onVerifyClicked(){


        SmsVerifyModel smsVerifyModel = new SmsVerifyModel();
        if (editTextSMSCode.getText().toString().trim().length() ==4)
        {

            smsVerifyModel.setUserId(UserInfo.getInstance().getRestOwnerDate().getId());
            smsVerifyModel.setValue(editTextSMSCode.getText().toString().trim());
            RestClient.getInstance().initRestClient(RestClient.API_SMS_URL).verifySMS(smsVerifyModel).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    JsonObject resultObj = response.body();
                    if (resultObj != null)
                    {
                        if (resultObj.get("message").toString().trim().contains("success") && resultObj.has("data")){
                            Intent intent = new Intent(SmsVerifyActivity.this, RestaurantActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }


                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                }
            });
        }

        Intent intent = new Intent(SmsVerifyActivity.this,RestaurantActivity.class);
            startActivity(intent);
            finish();
    }

    @OnClick(R.id.iv_back)
    public void onBackClicked(){
        startLoginActivity();
    }

    @Override
    public void onBackPressed() {
        startLoginActivity();
    }

    private void startLoginActivity(){
        Intent intent = new Intent(SmsVerifyActivity.this,LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
