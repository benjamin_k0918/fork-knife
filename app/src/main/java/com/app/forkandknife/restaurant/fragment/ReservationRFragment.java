package com.app.forkandknife.restaurant.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.app.forkandknife.AccountManageActivity;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;


import com.orangegangsters.github.swipyrefreshlayout.library.R.*;
import com.orangegangsters.github.swipyrefreshlayout.library.*;

import com.app.forkandknife.R;
import com.app.forkandknife.adapter.ReservationAdapter;
import com.app.forkandknife.adapter.RestaurtantReservationAdapter;
import com.app.forkandknife.global.Constant;
import com.app.forkandknife.model.CuisineData;
import com.app.forkandknife.model.EventBusMessage;
import com.app.forkandknife.model.ReservationData;
import com.app.forkandknife.model.RestOwnerDate;
import com.app.forkandknife.model.RestaurantReservationModel;
import com.app.forkandknife.model.UserInfo;
import com.app.forkandknife.user.activity.TermsAndrConditionActivity;
import com.app.forkandknife.user.fragment.ReservationDetailFragment;
import com.app.forkandknife.webservice.RestClient;

import org.greenrobot.eventbus.EventBus;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;


public class ReservationRFragment extends Fragment {

    @BindView(R.id.listView)
    SwipeMenuListView listView;

    RestaurtantReservationAdapter reservationAdapter;
    ArrayList<RestaurantReservationModel> reservationDataArrayList;

    @BindView(R.id.editText_search_key)
    EditText editTextSearchKey;

    ArrayList<CuisineData> cuisineDataArrayList=  new ArrayList<>();

    @BindView(R.id.refresh_layout)
    SwipyRefreshLayout swipeRefreshLayout;

    ProgressDialog progressDialog;
    int pageNumber = 1;
    SharedPreferences preferences;

    ArrayList<RestaurantReservationModel> currentArrayList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View containerView=inflater.inflate(R.layout.fragment_reservation_r, container, false);
        ButterKnife.bind(this,containerView);

        progressDialog = new ProgressDialog(getActivity());

        progressDialog.setMessage(getString(R.string.loading));
        if (UserInfo.getInstance().getRestOwnerDate().getRestaurant()== null)
        {
            getRestaurantInfo();
        }
        else {
            getReservationList();
        }

        addSearchModule();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                UserInfo.getInstance().setCurrentRestReservation(currentArrayList.get(position));
                setFragment(new ReservationDetailRFragment());
            }
        });

        setCreator();

        swipeRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                if (direction == SwipyRefreshLayoutDirection.BOTTOM)
                {
                    if (currentArrayList.size() % 20 == 0)
                    {
                        pageNumber+= 1;
                        getReservationList();
                    }else {
                        getReservationList();
                    }
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });

        return  containerView;

    }

    @Override
    public void onResume() {
        super.onResume();

        if(getView() == null){
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    // handle back button's click listener

                    return true;
                }
                return false;
            }
        });
    }

    private void getRestaurantInfo(){
        progressDialog.show();
        String auth = "Bearer " + UserInfo.getInstance().getRestOwnerDate().getAccessToken();

        RestClient.getInstance().initRestClient(RestClient.API_URL).getRestaurantByOwner(auth,UserInfo.getInstance().getRestOwnerDate().getId()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                JsonObject resultObj = response.body();


                //   Toast.makeText(getActivity(),resultObj,Toast.LENGTH_LONG).show();
                //    Toast.makeText(getActivity(),resultObj.get("message").toString(),Toast.LENGTH_LONG).show();

                    if (response.code() == 200){
                        JsonObject userJson = resultObj.getAsJsonObject("data");
                        Gson gson = new Gson();
                        Type type = new TypeToken<RestOwnerDate.Restaurant>() {}.getType();
                        RestOwnerDate.Restaurant restaurant = gson.fromJson(userJson,type);


                        UserInfo.getInstance().getRestOwnerDate().setRestaurant(restaurant);

                        getReservationList();

                    }else if (response.code() == 401)
                    {
                        SharedPreferences preferences;
                        preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor =      preferences.edit();
                        editor.putString("login", "0");
                        editor.commit();
                        Constant.isReloaded = false;
                        if (!Constant.fbToken.isEmpty())
                        {
                            LoginManager.getInstance().logOut();
                        }
                        Intent intent = new Intent(getActivity(), AccountManageActivity.class);
                        startActivity(intent);

                        getActivity().finish();
                    }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    private void setCreator(){
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(0xffcd0606));

                // set item width
                deleteItem.setWidth(200);
                // set a icon
                deleteItem.setTitleColor(getActivity().getResources().getColor(R.color.color_white));
                deleteItem.setTitle(getString(R.string.delete));

                deleteItem.setTitleSize(17);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };

        listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {

                    case 0:
                        // delete

                        hideReservation(position);
                        break;
                }
                // false : close the menu; true : not close the menu
                return false;
            }
        });



// set creator
        listView.setMenuCreator(creator);

        // left
        if (Constant.language ==1)
        {
            listView.setSwipeDirection(SwipeMenuListView.DIRECTION_RIGHT);

        }else {
            listView.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);

        }
    }

    private void hideReservation(final int position){
        String auth = "Bearer " + UserInfo.getInstance().getRestOwnerDate().getAccessToken();

        reservationAdapter.notifyDataSetChanged();
        HashMap <String,Object> map = new HashMap<>();
        map.put("userId",UserInfo.getInstance().getRestOwnerDate().getId());
        map.put("reservationId",currentArrayList.get(position).getId());
        RestClient.getInstance().initRestClient(RestClient.API_URL).hideReservation(auth,map).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 200)
                {
                    currentArrayList.remove(position);
                }else              if (response.code() == 401)
                {
                    SharedPreferences preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor =      preferences.edit();
                    editor.putString("login", "0");
                    editor.commit();
                    if (!Constant.fbToken.isEmpty())
                    {
                        LoginManager.getInstance().logOut();
                    }
                    Intent intent = new Intent(getActivity(), AccountManageActivity.class);
                    startActivity(intent);

                    getActivity().finish();
                }


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    private void getAllCuisineList() {
        progressDialog.show();
        RestClient.getInstance().initRestClient(RestClient.API_URL).getAllCuisine().enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject resultObj = response.body();

                progressDialog.dismiss();
                if (response.code() == 200) {
                    JsonArray restaurantJson = resultObj.getAsJsonObject("data").getAsJsonArray("rows");

                    Gson gson = new Gson();
                    Type type = new TypeToken<List<CuisineData>>() {
                    }.getType();
                    cuisineDataArrayList = gson.fromJson(restaurantJson, type);
                    UserInfo.getInstance().setCuisineDataArrayList(cuisineDataArrayList);

                    //     UserInfo.getInstance().setPointData(cuisineDataArrayList);

                    //    EventBus.getDefault().post(new EventBusMessage(EventBusMessage.MessageType.TO_VERIFY_SMS_FRAGMENT));
                }else if (response.code() == 401)
                {
                    SharedPreferences preferences;
                    preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor =      preferences.edit();
                    editor.putString("login", "0");
                    editor.commit();
                    Constant.isReloaded = false;
                    if (!Constant.fbToken.isEmpty())
                    {
                        LoginManager.getInstance().logOut();
                    }
                    Intent intent = new Intent(getActivity(), AccountManageActivity.class);
                    startActivity(intent);

                    getActivity().finish();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    private void setListView(){
        reservationAdapter = new RestaurtantReservationAdapter(getActivity(),currentArrayList);
        listView.setAdapter(reservationAdapter);
    }


    private void setFragment(Fragment fragment){
        FragmentManager fragmentManager= getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =   fragmentManager.beginTransaction();
        Bundle sendData = new Bundle();
        fragmentTransaction.replace(R.id.MainFrame,fragment,null);
        fragmentTransaction.commit();
    }

    private  void addSearchModule(){
        editTextSearchKey.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String currentString =  s.toString().trim();

                currentArrayList = new ArrayList<>();

                for (int i = 0;i<reservationDataArrayList.size();i++)
                    if (reservationDataArrayList.get(i).getClient().getName().toLowerCase().contains(currentString.toLowerCase())){
                        currentArrayList.add(reservationDataArrayList.get(i));
                    }

               setListView();

            }
        });
    }





    private void getReservationList(){
        if (!progressDialog.isShowing())
        {
            progressDialog.show();
        }

        HashMap<String,Object> map = new HashMap<>();
        map.put("restaurantId", UserInfo.getInstance().getRestOwnerDate().getRestaurant().getId());
        map.put("pageNumber",pageNumber);
        map.put("pageSize",20);
      //  map.put("token",UserInfo.getInstance().getRestOwnerDate().getAccessToken());

        RestClient.getInstance().initRestClient(RestClient.API_URL).searchAllReservationsRestaurant(map).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject resultObj = response.body();
                progressDialog.dismiss();

                if (response.code() == 200) {
                    JsonArray restaurantJson = resultObj.getAsJsonObject("data").getAsJsonArray("rows");
                    Gson gson = new Gson();
                    Type type = new TypeToken<List<RestaurantReservationModel>>() {
                    }.getType();
                    reservationDataArrayList = new ArrayList<>();

                    reservationDataArrayList = gson.fromJson(restaurantJson, type);
                    if (currentArrayList.size() %20 == 0)
                    {
                        currentArrayList.addAll(reservationDataArrayList);
                    }else {
                            if (currentArrayList.size() / 20 == 0)
                            {
                                currentArrayList = new ArrayList<>();
                                currentArrayList.addAll(reservationDataArrayList);
                            }else {
                                int size = currentArrayList.size()/20;
                                currentArrayList = new ArrayList<RestaurantReservationModel>( currentArrayList.subList(0,20*size-1));
                                currentArrayList.addAll(reservationDataArrayList);
                            }
                    }

                    setListView();
                    if (UserInfo.getInstance().getCuisineDataArrayList().isEmpty())
                    {
                        getAllCuisineList();
                    }

                    //   UserInfo.getInstance().setTopRestModels(restaurantModels);

                }else if (response.code() == 401)
                {
                    SharedPreferences preferences;
                    preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor =      preferences.edit();
                    editor.putString("login", "0");
                    editor.commit();
                    Constant.isReloaded = false;
                    if (!Constant.fbToken.isEmpty())
                    {
                        LoginManager.getInstance().logOut();
                    }
                    Intent intent = new Intent(getActivity(), AccountManageActivity.class);
                    startActivity(intent);

                    getActivity().finish();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
