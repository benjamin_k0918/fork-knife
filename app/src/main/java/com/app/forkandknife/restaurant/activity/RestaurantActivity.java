package com.app.forkandknife.restaurant.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.app.forkandknife.R;
import com.app.forkandknife.global.Constant;
import com.app.forkandknife.model.EventBusMessage;
import com.app.forkandknife.model.ReservationData;
import com.app.forkandknife.model.RestOwnerDate;
import com.app.forkandknife.model.UserInfo;
import com.app.forkandknife.restaurant.fragment.ProfileRFragment;
import com.app.forkandknife.restaurant.fragment.ReservationRFragment;
import com.app.forkandknife.user.fragment.MainFragment;
import com.app.forkandknife.user.fragment.ProfileFragment;
import com.app.forkandknife.user.fragment.ReservationFragment;
import com.app.forkandknife.webservice.RestClient;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.reflect.Type;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RestaurantActivity extends AppCompatActivity {
    BottomNavigationView navigation;
    ProgressDialog progressDialog;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {

                case R.id.navigation_reservtion:
                    setChildFragment(new ReservationRFragment());
                    return true;
                case R.id.navigation_notifications:
                    setChildFragment(new ProfileRFragment());
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant);
        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(getApplicationContext());

        EventBus.getDefault().register(this);
         navigation = (BottomNavigationView) findViewById(R.id.bottom_navigation_bar);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        if (Constant.isReloaded)
        {
            navigation.setSelectedItemId(R.id.navigation_notifications);
        }
        else {
            navigation.setSelectedItemId(R.id.navigation_reservtion);
        }

    }



    private void setChildFragment(Fragment fragment){
        FragmentTransaction fragmentTranaction= getSupportFragmentManager().beginTransaction();
        fragmentTranaction.replace(R.id.MainFrame, fragment, null);
        fragmentTranaction.commit();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void Event(EventBusMessage messageEvent) throws Exception {
        if (messageEvent != null) {
            int messageType = messageEvent.getMessageType();
            if (messageType == EventBusMessage.MessageType.MODIFY_LOCALE) {
                if (Constant.language == 1)
                {
                    navigation.getMenu().getItem(0).setTitle(getString(R.string.title_home_ar));
                    navigation.getMenu().getItem(1).setTitle(getString(R.string.reservation_ar));
                    navigation.getMenu().getItem(2).setTitle(getString(R.string.profile_ar));
                }else {
                    navigation.getMenu().getItem(0).setTitle(getString(R.string.title_home_en));
                    navigation.getMenu().getItem(1).setTitle(getString(R.string.reservation_en));
                    navigation.getMenu().getItem(2).setTitle(getString(R.string.profile_en));
                }
            }
        }
    }


    @Override
    protected void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }
}
