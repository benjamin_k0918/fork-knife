package com.app.forkandknife.restaurant.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.app.forkandknife.R;
import com.app.forkandknife.global.Utils;
import com.app.forkandknife.model.UserInfo;

import org.w3c.dom.Text;

import java.io.Serializable;
import java.util.HashMap;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Register1Activity extends AppCompatActivity {

    @BindView(R.id.editText_mobile)
    EditText editTextMobile;


    @BindView(R.id.editText_restaurant_name)
    EditText editTextRestName;

    @BindView(R.id.editText_arabic_name)
    EditText editTextArabicName;

    @BindView(R.id.editText_ownername)
    EditText editTextOwnerName;

    @BindView(R.id.editText_email)
    EditText editTextEmail;


    @BindView(R.id.editText_password)
    EditText editTextPassword;

    @BindView(R.id.editText_confirm_password)
    EditText editTextConfirmPassword;
    String restName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register1);

        ButterKnife.bind(this);

        editTextMobile.setText("966");
        Selection.setSelection(editTextMobile.getText(), editTextMobile.getText().length());

        editTextMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().startsWith("966")){
                    editTextMobile.setText("966");
                    Selection.setSelection(editTextMobile.getText(), editTextMobile.getText().length());

                }

            }
        });
    }


    private void checkValidation(){
         restName = editTextRestName.getText().toString().trim();
        String arabicName = editTextArabicName.getText().toString().trim();
        String strOwnderName = editTextOwnerName.getText().toString().trim();
        String strEmail = editTextEmail.getText().toString().trim();
        String strMobile = editTextMobile.getText().toString().trim();
        String strPassword = editTextPassword.getText().toString().trim();
        String strConfirmPassword = editTextConfirmPassword.getText().toString().trim();

        if (!(restName.isEmpty() ||  arabicName.isEmpty() || strOwnderName.isEmpty() || strEmail.isEmpty() || strMobile.isEmpty() || strPassword.isEmpty()|| strConfirmPassword.isEmpty()))
        {
            if (!(Utils.isEmailValid(strEmail)))
            {
                Toast.makeText(this,getString(R.string.ivalid_email),Toast.LENGTH_LONG).show();
            }
            else if (!strConfirmPassword.equals(strPassword))
            {
                Toast.makeText(this,getString(R.string.not_match_password),Toast.LENGTH_LONG).show();
            }
            else if (strPassword.length()<4)
            {
                Toast.makeText(this,getString(R.string.MSG_INVALID_PASSWORD_LENGTH),Toast.LENGTH_LONG).show();
            }
            else if (strMobile.length()<12)
            {
                Toast.makeText(this,getString(R.string.mobile_length_error),Toast.LENGTH_LONG).show();
            }
            else {
                HashMap <String,Object> map = new HashMap<>();
                map.put("ownerName",strOwnderName);
                map.put("deviceType",2);
                map.put("password",strPassword);
                map.put("name",restName);
                map.put("arabicName",arabicName);
                map.put("email",strEmail);
                map.put("mobile",strMobile);
                UserInfo.getInstance().setNewRestMap(map);

                Intent intent = new Intent(Register1Activity.this,Register2Activity.class);
                intent.putExtra("register_data", map);
                startActivity(intent);
            }
        }else {
            Toast.makeText(this,getString(R.string.input_all),Toast.LENGTH_LONG).show();
        }
    }


    @OnClick(R.id.btn_submit)
    public void onSubmitClicked(){
        checkValidation();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @OnClick(R.id.iv_back)
    public void onIvBackClicked(){
        finish();
    }
}
