package com.app.forkandknife.restaurant.fragment;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.app.forkandknife.AccountManageActivity;
import com.app.forkandknife.global.Constant;
import com.facebook.login.LoginManager;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonObject;
import com.app.forkandknife.R;
import com.app.forkandknife.adapter.CuisineAdapter;
import com.app.forkandknife.adapter.FeatureAdapter;
import com.app.forkandknife.custom.MyMapView;
import com.app.forkandknife.model.CuisineData;
import com.app.forkandknife.model.FeatureData;
import com.app.forkandknife.model.UserInfo;
import com.app.forkandknife.user.fragment.ProfileFragment;
import com.app.forkandknife.webservice.RestClient;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class UpdateDetailFragment extends Fragment  implements OnMapReadyCallback {

    RecyclerView.LayoutManager layoutManagerCuisine;
    RecyclerView.LayoutManager layoutManagerFeature;

    @BindView(R.id.recyclerView_feature)
    RecyclerView recyclerViewFeature;

    @BindView(R.id.iv_switch)
    ImageView ivSwitch;

    @BindView(R.id.recyclerView_cuisine)
    RecyclerView recyclerViewCuisine;
    ArrayList<CuisineData> cuisineDataArrayList;
    ArrayList<FeatureData> featureDataArrayList;
    CuisineAdapter cuisineAdapter;
    FeatureAdapter featureAdapter;

    @BindView(R.id.editText_mobile)
    EditText editTextMobile;


    @BindView(R.id.editText_full_name)
    EditText editTextRestName;

    @BindView(R.id.editText_email)
    EditText editTextEmail;

    @BindView(R.id.editText_cost)
    EditText editTextCost;


    @BindView(R.id.editText_end_time)
    EditText editTextEndTime;

    @BindView(R.id.editText_time)
    EditText editTextTime;

    WindowManager.LayoutParams  layoutParams;

    MapView mapView;
    GoogleMap map;

    @BindView(R.id.radio_buffet)
    RadioButton radioBuffet;

    @BindView(R.id.radio_perrequest)
    RadioButton radioPerrequest;

    @BindView(R.id.tv_buffet)
    TextView tvBuffet;

    @BindView(R.id.tv_request)
    TextView tvRequest;

    @BindView(R.id.editText_arabic_name)
            EditText editTextArabicName;

    MarkerOptions markerOptions;
    MarkerOptions currentMakerOptions;

    boolean isBusy = false;

    LatLng currentUserLocation = new LatLng(0,0);
    LatLng currentRestauratnLocation = new LatLng(0,0);
    BitmapDescriptor icon;

    HashMap<String,Object> newMap = new HashMap<>();

    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 1001;
    private static final String TAG = "MapsActivity";
    private String[] permissions = new String[] {android.Manifest.permission.ACCESS_FINE_LOCATION};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View containerView=inflater.inflate(R.layout.fragment_update_detail, container, false);
        ButterKnife.bind(this,containerView);

        layoutParams = new WindowManager.LayoutParams();
        layoutParams.gravity = Gravity.BOTTOM;


        editTextRestName.setText(UserInfo.getInstance().getRestOwnerDate().getRestaurant().getName());
        editTextEmail.setText(UserInfo.getInstance().getRestOwnerDate().getRestaurant().getEmail());
        editTextMobile.setText(UserInfo.getInstance().getRestOwnerDate().getRestaurant().getMobile());
        editTextTime.setText(UserInfo.getInstance().getRestOwnerDate().getRestaurant().getOpenTime());
        editTextEndTime.setText(UserInfo.getInstance().getRestOwnerDate().getRestaurant().getCloseTime());
        editTextCost.setText(String.valueOf(UserInfo.getInstance().getRestOwnerDate().getRestaurant().getCost()));
        editTextArabicName.setTextLocale(new Locale("ar"));

        if (UserInfo.getInstance().getRestOwnerDate().getRestaurant().getRequestType() == 1)
        {
            radioBuffet.setSelected(true);
            radioPerrequest.setSelected(false);
        }else {
            radioBuffet.setSelected(false);
            radioPerrequest.setSelected(true);
        }

        if (UserInfo.getInstance().getRestOwnerDate().getRestaurant().getArabicName()!= null)
        {
            editTextArabicName.setText(String.valueOf(UserInfo.getInstance().getRestOwnerDate().getRestaurant().getArabicName()));

        }
        setRecyclerView();

        MapsInitializer.initialize(getActivity());
        icon = BitmapDescriptorFactory.fromResource(R.drawable.marker);

//        markerOptions = new MarkerOptions().position(new LatLng(40.0008, 124.3545))
//                .title("Current Location")
//                .snippet("Thinking of finding some thing...")
//                .icon(icon);

        mapView = (MyMapView) containerView.findViewById(R.id.mapView);
        // Gets the MapView from the XML layout and creates it
        mapView.onCreate(savedInstanceState);

        mapView = (MapView)containerView. findViewById(R.id.mapView);
        // Gets the MapView from the XML layout and creates it
        mapView.onCreate(savedInstanceState);

        checkLocationPermissions();

        initListener();

        return containerView;
    }

    private void initFeature(){
        if (UserInfo.getInstance().getRestOwnerDate().getRestaurant().getAllowSmoking()==1)
        {
                 featureDataArrayList.get(0).setSelected(true);
        }
        else {
            featureDataArrayList.get(0).setSelected(false);
        }
        if (UserInfo.getInstance().getRestOwnerDate().getRestaurant().getAcceptVisa()==1)
        {
            featureDataArrayList.get(1).setSelected(true);
        }
        else {
            featureDataArrayList.get(1).setSelected(false);
        }
        if (UserInfo.getInstance().getRestOwnerDate().getRestaurant().getDressCode()==1)
        {
            featureDataArrayList.get(2).setSelected(true);
        }
        else {
            featureDataArrayList.get(2).setSelected(false);
        }
        if (UserInfo.getInstance().getRestOwnerDate().getRestaurant().getOutdoorSeating()==1)
        {
            featureDataArrayList.get(3).setSelected(true);
        }
        else {
            featureDataArrayList.get(3).setSelected(false);
        }
        if (UserInfo.getInstance().getRestOwnerDate().getRestaurant().getParking()==1)
        {
            featureDataArrayList.get(4).setSelected(true);
        }
        else {
            featureDataArrayList.get(4).setSelected(false);
        }

        if (!UserInfo.getInstance().getRestOwnerDate().getRestaurant().getCuisines().isEmpty())
        {
            for (int i =0;i<UserInfo.getInstance().getRestOwnerDate().getRestaurant().getCuisines().size();i++)
            {
                for (int j  = 0;j<UserInfo.getInstance().getCuisineDataArrayList().size();j++)
                {
                    if (UserInfo.getInstance().getCuisineDataArrayList().get(j).getId().equals(UserInfo.getInstance().getRestOwnerDate().getRestaurant().getCuisines().get(i)))
                    {
                        UserInfo.getInstance().getCuisineDataArrayList().get(j).setSelection(true);
                    }
                }
            //    UserInfo.getInstance().getCuisineDataArrayList().get(UserInfo.getInstance().getRestOwnerDate().getRestaurant().getCuisines().get(i)).setSelection(true);
            }
        }

    }

    @OnClick(R.id.iv_switch)
    public void onSwitchClicked(){
        if (isBusy)
        {
            ivSwitch.setImageResource(R.drawable.icon_off);
            isBusy = false;
            enableRestaurant();
        }else {
            ivSwitch.setImageResource(R.drawable.icon_on);
            isBusy = true;
            makeRestaurantBusy();
        }

    }

    @OnClick(R.id.layout_workinghours)
    public void onWorkingHoursClicked(){
        setFragment(new UpdateTimeFragment());
    }

    private void updateUserProfile(){
        String auth = "Bearer " + UserInfo.getInstance().getRestOwnerDate().getAccessToken();

        RestClient.getInstance().initRestClient(RestClient.API_URL).updateRestProfile(auth,newMap).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 200)
                {
                    setFragment(new ProfileRFragment());
                }else if (response.code() == 401)
                {
                    SharedPreferences preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor =      preferences.edit();
                    editor.putString("login", "0");
                    editor.commit();
                    if (!Constant.fbToken.isEmpty())
                    {
                        LoginManager.getInstance().logOut();
                    }
                    Intent intent = new Intent(getActivity(), AccountManageActivity.class);
                    startActivity(intent);

                    getActivity().finish();
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    private void enableRestaurant(){
        RestClient.getInstance().initRestClient(RestClient.API_URL).enableRestaurant(UserInfo.getInstance().getRestOwnerDate().getRestaurant().getId()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
              if (response.code() == 401)
                {
                    SharedPreferences preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor =      preferences.edit();
                    editor.putString("login", "0");
                    editor.commit();
                    if (!Constant.fbToken.isEmpty())
                    {
                        LoginManager.getInstance().logOut();
                    }
                    Intent intent = new Intent(getActivity(), AccountManageActivity.class);
                    startActivity(intent);

                    getActivity().finish();
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    private void makeRestaurantBusy(){
        RestClient.getInstance().initRestClient(RestClient.API_URL).makeRestaurantBusy(UserInfo.getInstance().getRestOwnerDate().getRestaurant().getId()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                 if (response.code() == 401)
                {
                    SharedPreferences preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor =      preferences.edit();
                    editor.putString("login", "0");
                    editor.commit();
                    if (!Constant.fbToken.isEmpty())
                    {
                        LoginManager.getInstance().logOut();
                    }
                    Intent intent = new Intent(getActivity(), AccountManageActivity.class);
                    startActivity(intent);

                    getActivity().finish();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }


    private void initListener(){

        tvBuffet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        radioBuffet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radioPerrequest.setChecked(false);
            }
        });

        radioPerrequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radioBuffet.setChecked(false);
            }
        });
    }


    private String formatNumber(int num){
        String str = String.format("%02d", num);
        return  str;
    }


    private void checkLocationPermissions(){
        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(), permissions, MY_PERMISSIONS_REQUEST_LOCATION);
            }
        } else {
            // Permission has already been granted
            mapView.getMapAsync(this);
        }
    }


    private void getFeatures(){
        if (featureDataArrayList.get(0).isSelected())
        {
            newMap.put("allowSmoking",true);
        }else {
            newMap.put("allowSmoking",false);
        }
        if (featureDataArrayList.get(1).isSelected())
        {

            newMap.put("acceptVisa",true);
        }else {
            newMap.put("acceptVisa",false);
        }
        if (featureDataArrayList.get(2).isSelected())
        {

            newMap.put("dressCode",true);
        }else {
            newMap.put("dressCode",false);
        }
        if (featureDataArrayList.get(3).isSelected())
        {

            newMap.put("outdoorSeating",true);
        }else {
            newMap.put("outdoorSeating",false);

        }
        if (featureDataArrayList.get(4).isSelected())
        {

            newMap.put("parking",true);
        }else {
            newMap.put("parking",false);

        }
    }

    private void setRecyclerView(){
        layoutManagerCuisine = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        layoutManagerFeature = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

        recyclerViewCuisine.setLayoutManager(layoutManagerCuisine);
        recyclerViewFeature.setLayoutManager(layoutManagerFeature);
        cuisineDataArrayList = new ArrayList<>();
        featureDataArrayList = new ArrayList<>();
        featureDataArrayList.add(new FeatureData("Allow Smoking"));
        featureDataArrayList.add(new FeatureData("Accept Visa"));
        featureDataArrayList.add(new FeatureData("Dress Code"));
        featureDataArrayList.add(new FeatureData("Outdoor Seating"));
        featureDataArrayList.add(new FeatureData("Valet Parking"));

        initFeature();

        cuisineAdapter = new CuisineAdapter(getActivity(),UserInfo.getInstance().getCuisineDataArrayList());
        recyclerViewCuisine.setAdapter(cuisineAdapter);

        featureAdapter  = new FeatureAdapter(getActivity(),featureDataArrayList);
        recyclerViewFeature.setAdapter(featureAdapter);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @OnClick(R.id.iv_select_time)
    public void onEditTextTimeClicked(){
        // TODO Auto-generated method stub
        Calendar mcurrentTime = Calendar.getInstance();
        final int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        final int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                editTextTime.setText( formatNumber(selectedHour) + ":" + formatNumber(selectedMinute));
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.getWindow().setAttributes(layoutParams);

        mTimePicker.show();
    }

    @OnClick(R.id.iv_select_end_time)
    public void editTextEndTimeClicked(){
        // TODO Auto-generated method stub
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                editTextEndTime.setText( formatNumber(selectedHour) + ":" + formatNumber(selectedMinute));
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.getWindow().setAttributes(layoutParams);

        mTimePicker.show();
    }
    private int[] getCuisineList (){
        ArrayList<Integer> integers = new ArrayList<>();
        for (int i = 0;i<cuisineDataArrayList.size();i++)
        {
            if (cuisineDataArrayList.get(i).getSelection())
            {
                integers.add(i);
            }
        }
        int[] cuisineArray = new int[integers.size()];
        if (!integers.isEmpty())
        {
            for (int i = 0;i<integers.size();i++)
            {
                cuisineArray[i] = integers.get(i);
            }
        }

        return  cuisineArray;

    }

    @OnClick(R.id.btn_submit)
    public void onSubmitClicked()
    {
        getFeatures();

        newMap.put("restaurantId",UserInfo.getInstance().getRestOwnerDate().getRestaurant().getId());
        newMap.put("name",editTextRestName.getText().toString().trim());
        newMap.put("email",editTextEmail.getText().toString().trim());
        newMap.put("mobile",editTextMobile.getText().toString().trim());
        newMap.put("cuisines",getCuisineList());
        newMap.put("cost",editTextCost.getText().toString().trim());
        newMap.put("openTime",editTextTime.getText().toString().trim());
        newMap.put("closeTime",editTextEndTime.getText().toString().trim());
        newMap.put("requestType",2);
        newMap.put("latitude",String.valueOf(currentUserLocation.latitude));
        newMap.put("longitude",String.valueOf(currentUserLocation.longitude));
        newMap.put("arabicName",editTextArabicName.getText().toString().trim());
        newMap.put("loyaltyPoints",UserInfo.getInstance().getRestOwnerDate().getRestaurant().getLoyaltyPoints());
   //     newMap.put("token",UserInfo.getInstance().getRestOwnerDate().getAccessToken());
        updateUserProfile();


    }

    private void setFragment(Fragment fragment){
        FragmentManager fragmentManager= getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =   fragmentManager.beginTransaction();
        Bundle sendData = new Bundle();
        fragmentTransaction.replace(R.id.MainFrame,fragment,null);
        fragmentTransaction.commit();
    }

    @OnClick(R.id.iv_back)
    public void onBackClicked(){
        setFragment(new ProfileRFragment());
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
        if(getView() == null){
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    // handle back button's click listener
                    setFragment(new ProfileRFragment());
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        map = googleMap;

        map.getUiSettings().setMyLocationButtonEnabled(false);

        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            map.setMyLocationEnabled(true);
        }
        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (currentRestauratnLocation != latLng)
                {
                    map.clear();
                    currentRestauratnLocation = latLng;
                    markerOptions = new MarkerOptions().position(new LatLng(currentRestauratnLocation.latitude, currentRestauratnLocation.longitude))
                            .title("Current Location")
                            .snippet("Thinking of finding some thing...")
                            .icon(icon);
                    map.addMarker(markerOptions);
                    currentMakerOptions = new MarkerOptions().position(new LatLng(currentUserLocation.latitude, currentUserLocation.longitude))
                            .title("Current Location")
                            .snippet("Thinking of finding some thing...")
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.my_location));
                    map.addMarker(currentMakerOptions);

                }else {
                    map.clear();
                    currentMakerOptions = new MarkerOptions().position(new LatLng(currentUserLocation.latitude, currentUserLocation.longitude))
                            .title("Current Location")
                            .snippet("Thinking of finding some thing...")
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.my_location));
                    map.addMarker(currentMakerOptions);
                }

            }
        });
        map.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location location) {

                currentUserLocation = new LatLng(location.getLatitude(), location.getLongitude());

                currentMakerOptions = new MarkerOptions().position(new LatLng(currentUserLocation.latitude, currentUserLocation.longitude))
                        .title("Current Location")
                        .snippet("Thinking of finding some thing...")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.my_location));
                map.addMarker(currentMakerOptions);
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentUserLocation, 12));
                map.setOnMyLocationChangeListener(null); // Remove the listener
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}
