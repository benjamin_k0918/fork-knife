package com.app.forkandknife.restaurant.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.forkandknife.AccountManageActivity;
import com.app.forkandknife.global.Constant;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;
import com.app.forkandknife.R;
import com.app.forkandknife.adapter.ImageSliderAdapter;
import com.app.forkandknife.model.ImageModel;
import com.app.forkandknife.model.ReservationData;
import com.app.forkandknife.model.UserInfo;
import com.app.forkandknife.service.PicassoImageLoadingService;
import com.app.forkandknife.user.fragment.ReservationFragment;
import com.app.forkandknife.webservice.RestClient;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ss.com.bannerslider.Slider;

public class ReservationDetailRFragment extends Fragment {


    @BindView(R.id.carouselView)
    CarouselView carouselView;


    @BindView(R.id.tv_name)
    TextView tvName;

    @BindView(R.id.tv_guest_number)
    TextView tvGuestNumber;

    @BindView(R.id.tv_date)
    TextView tvReservationDate;

    @BindView(R.id.tv_discount)
    TextView tvDiscount;

    @BindView(R.id.tv_comment)
    TextView tvComment;

    String[] restImageList ;

    ProgressDialog progressDialog ;

    @BindView(R.id.layout_positive)
    LinearLayout positiveLayout;

    @BindView(R.id.layout_negative)
    LinearLayout negativeLayout;

    @BindView(R.id.tv_mobile)
            TextView tvMobile;

    ArrayList<ImageModel> imageModels = new ArrayList<>();
    String imageBaseURl  = RestClient.API_URL+  "restaurant/image/";

    private RequestOptions requestOptions = new RequestOptions();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View containerView=inflater.inflate(R.layout.fragment_reservation_detail_r, container, false);
        ButterKnife.bind(this,containerView);
        requestOptions.placeholder(R.drawable.restaurant_logo);
        requestOptions.error(R.drawable.restaurant_logo);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.loading));
        initData();
        getAllRestaurantImage();

        initOperation();

        return containerView;
    }

    private void initOperation(){
        if (UserInfo.getInstance().getCurrentRestReservation().getStatus() == 1)
        {
            positiveLayout.setVisibility(View.VISIBLE);
            negativeLayout.setVisibility(View.GONE);
        }else if (UserInfo.getInstance().getCurrentRestReservation().getStatus() == 4){
            positiveLayout.setVisibility(View.GONE);
            negativeLayout.setVisibility(View.VISIBLE);
        }else {
            positiveLayout.setVisibility(View.GONE);
            negativeLayout.setVisibility(View.GONE);
        }
    }

    private void initData(){
        tvReservationDate.setText(UserInfo.getInstance().getCurrentRestReservation().getReservationDate());
        tvName.setText(UserInfo.getInstance().getCurrentRestReservation().getClient().getName());
        tvGuestNumber.setText(String.valueOf(UserInfo.getInstance().getCurrentRestReservation().getNumberOfPeople()));
        tvDiscount.setText(String.valueOf(UserInfo.getInstance().getCurrentRestReservation().getDiscount()) + " " + getString(R.string.currency_sar));
        tvComment.setText(UserInfo.getInstance().getCurrentRestReservation().getComment());
        if (UserInfo.getInstance().getCurrentRestReservation().getClient().getMobile() != null)
        {
            tvMobile.setText(UserInfo.getInstance().getCurrentRestReservation().getClient().getMobile());
        }
    }

    private void setFragment(Fragment fragment){
        FragmentManager fragmentManager= getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =   fragmentManager.beginTransaction();
        Bundle sendData = new Bundle();
        fragmentTransaction.replace(R.id.MainFrame,fragment,null);
        fragmentTransaction.commit();
    }

    @OnClick(R.id.btn_complete)
    public void onCompleteClicked(){
        showDialog();

    }

    @OnClick(R.id.btn_accept)
    public void onAcceptClicked(){
        acceptReservation();
    }

    @OnClick(R.id.btn_reject)
    public void onRejectClicked(){
        rejectReservation();
    }

    @OnClick(R.id.btn_cancel)
    public void onCancelClicked(){
        completeReservation(1,0);
    }


    private void getAllRestaurantImage(){
        HashMap<String,Object> map =  new HashMap<>();
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();
        String auth = "Bearer " + UserInfo.getInstance().getRestOwnerDate().getAccessToken();

        map.put("restaurantId",UserInfo.getInstance().getCurrentRestReservation().getId());
        RestClient.getInstance().initRestClient(RestClient.API_URL).getAllRestaurantImage(auth,map).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject resultObj = response.body();
                progressDialog.dismiss();

                if (response.code()== 200){
                    JsonArray userJson = resultObj.getAsJsonArray("data");
                    Gson gson = new Gson();
                    Type type = new TypeToken<List<ImageModel>>() {}.getType();
                    imageModels = gson.fromJson(userJson,type);
                    if (!imageModels.isEmpty()){
                        restImageList = new String[imageModels.size()];
                        for (int i = 0;i<imageModels.size();i++)
                        {
                            restImageList[i]= imageBaseURl +UserInfo.getInstance().getCurrentRestaurant().getId() + "/index/" + imageModels.get(i).getIndex();
                        }
                    }else {
                        restImageList = new String[1];

                    }
                    carouselView.setImageListener(imageListener);
                    carouselView.setPageCount(restImageList.length);
                }else if (response.code() == 401)
                {
                    SharedPreferences preferences;
                    preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor =      preferences.edit();
                    editor.putString("login", "0");
                    editor.commit();
                    Constant.isReloaded = false;
                    if (!Constant.fbToken.isEmpty())
                    {
                        LoginManager.getInstance().logOut();
                    }
                    Intent intent = new Intent(getActivity(), AccountManageActivity.class);
                    startActivity(intent);

                    getActivity().finish();
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }


    private void showDialog(){

        final Dialog dialog = new Dialog(getActivity());
        final View view  = getActivity().getLayoutInflater().inflate(R.layout.dialog_input_number, null);

        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(view);
        Window window = dialog.getWindow();

        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.BOTTOM;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        Button btnSubmit = view.findViewById(R.id.btn_submit);
        final EditText editTextGuestNumber = view.findViewById(R.id.editText_guest_number);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!editTextGuestNumber.getText().toString().trim().isEmpty())
                {
                    completeReservation(2, Integer.parseInt(editTextGuestNumber.getText().toString().trim()));
                    dialog.dismiss();
                }

            }
        });
        dialog.show();
    }


    private void acceptReservation(){
        HashMap <String,Object> map  = new HashMap<>();
        String auth = "Bearer " + UserInfo.getInstance().getRestOwnerDate().getAccessToken();

        map.put("userId",UserInfo.getInstance().getRestOwnerDate().getId());
        map.put("reservationId",UserInfo.getInstance().getCurrentRestReservation().getId());
        RestClient.getInstance().initRestClient(RestClient.API_URL).acceptReservation(auth,map).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 200)
                {
                    setFragment(new ReservationRFragment());
                }
                else if (response.code() == 401)
                {
                    SharedPreferences preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor =      preferences.edit();
                    editor.putString("login", "0");
                    editor.commit();
                    if (!Constant.fbToken.isEmpty())
                    {
                        LoginManager.getInstance().logOut();
                    }
                    Intent intent = new Intent(getActivity(), AccountManageActivity.class);
                    startActivity(intent);

                    getActivity().finish();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    private void rejectReservation(){
        HashMap <String,Object> map  = new HashMap<>();
        String auth = "Bearer " + UserInfo.getInstance().getRestOwnerDate().getAccessToken();

        map.put("userId",UserInfo.getInstance().getRestOwnerDate().getId());
        map.put("reservationId",UserInfo.getInstance().getCurrentRestReservation().getId());

        RestClient.getInstance().initRestClient(RestClient.API_URL).rejectReservation(auth,map).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                setFragment(new ReservationRFragment());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    private void completeReservation (final int type, int number)
    {
        String auth = "Bearer " + UserInfo.getInstance().getRestOwnerDate().getAccessToken();

        HashMap <String,Object> map  = new HashMap<>();
        map.put("userId",UserInfo.getInstance().getRestOwnerDate().getId());
        map.put("reservationId",UserInfo.getInstance().getCurrentRestReservation().getId());
        map.put("completionType",type);

        map.put("numberOfVistor",number);



        RestClient.getInstance().initRestClient(RestClient.API_URL).completeReseravtion(auth,map).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 200)
                {
                    setFragment(new ReservationRFragment());

                }else  if (response.code() == 401)
                {
                    SharedPreferences preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor =      preferences.edit();
                    editor.putString("login", "0");
                    editor.commit();
                    if (!Constant.fbToken.isEmpty())
                    {
                        LoginManager.getInstance().logOut();
                    }
                    Intent intent = new Intent(getActivity(), AccountManageActivity.class);
                    startActivity(intent);

                    getActivity().finish();
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });

    }

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            Glide.with(getActivity()).setDefaultRequestOptions(requestOptions).load(restImageList[position]).into(imageView);

        }
    };


    @OnClick(R.id.iv_back)
    public void onBackClicked(){
        setFragment(new ReservationRFragment());
    }

    @Override
    public void onResume() {
        super.onResume();

        if(getView() == null){
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    // handle back button's click listener
                    setFragment(new ReservationRFragment());

                    return true;
                }
                return false;
            }
        });
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
