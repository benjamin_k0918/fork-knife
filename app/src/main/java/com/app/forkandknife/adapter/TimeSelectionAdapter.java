package com.app.forkandknife.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.app.forkandknife.R;
import com.app.forkandknife.global.Constant;
import com.app.forkandknife.model.TimeModel;
import com.app.forkandknife.user.activity.MainActivity;
import com.app.forkandknife.user.fragment.BookingFragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class TimeSelectionAdapter extends RecyclerView.Adapter<TimeSelectionAdapter.SimpleViewHolder>{

    private Context mContext;
    private ArrayList<TimeModel> timeArray;
    private String ruleArray;

    public TimeSelectionAdapter(Context mContext, ArrayList<TimeModel> timeArray,String arrayRule) {
        this.mContext = mContext;
        this.timeArray = timeArray;
        this.ruleArray = arrayRule;

    }

    @NonNull
    @Override
    public SimpleViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, final int i) {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.item_time, viewGroup, false);



        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SimpleViewHolder holder, final int i) {

        holder.name.setText(timeArray.get(i).getTime());

        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeArray.get(i).setSelection(!timeArray.get(i).isSelection());
                if (timeArray.get(i).isSelection()) {
                    holder.name.setBackground(mContext.getResources().getDrawable(R.drawable.ic_rounded_button_red));
                }
                else{
                    holder.name.setBackground(mContext.getResources().getDrawable(R.drawable.ic_rounded_button_gray));
                }

                // reverse the value
                int currentValue = Integer.valueOf(String.valueOf(ruleArray.charAt(i)));
                char[] chars = ruleArray.toCharArray();
                if (chars[i] == ruleArray.charAt(i)) {
                    chars[i] = String.valueOf(1 - currentValue).charAt(0);
                }

                ruleArray = String.valueOf(chars);

            }
        });

        if (ruleArray.charAt(i) == '1')
        {
            holder.name.setBackground(mContext.getResources().getDrawable(R.drawable.ic_rounded_button_red));
            timeArray.get(i).setSelection(true);
        }else {
            holder.name.setBackground(mContext.getResources().getDrawable(R.drawable.ic_rounded_button_gray));
            timeArray.get(i).setSelection(false);
        }


    }

    @Override
    public int getItemCount() {
        return timeArray.size();
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        TextView name ;
        public SimpleViewHolder(View view) {
            super(view);
            this.name = view.findViewById(R.id.tv_time);
        }
    }
}