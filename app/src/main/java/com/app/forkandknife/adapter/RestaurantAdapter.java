package com.app.forkandknife.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.forkandknife.webservice.RestClient;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.app.forkandknife.R;
import com.app.forkandknife.global.Constant;
import com.app.forkandknife.model.RestaurantModel;

import java.util.ArrayList;

public class RestaurantAdapter extends BaseAdapter {

    Context mContext;
    private ArrayList<RestaurantModel> restaurantDataArrayList;
    private LayoutInflater layoutInflater = null;
    private RequestOptions requestOptions = new RequestOptions();


    public RestaurantAdapter(Context mContext, ArrayList<RestaurantModel> restaurantDataArrayList) {
        this.mContext = mContext;
        this.restaurantDataArrayList = restaurantDataArrayList;
    }

    @Override
    public int getCount() {
        return restaurantDataArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return restaurantDataArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("StringFormatInvalid")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        requestOptions.placeholder(R.drawable.placeholder);
        requestOptions.error(R.drawable.placeholder);

        View vi = convertView;
        ViewHolder holder =  null;
        if (vi == null){
            holder = new ViewHolder();
            this.layoutInflater = (LayoutInflater)this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = this.layoutInflater.inflate(R.layout.item_restaurant, null);
            holder.imageView = vi.findViewById(R.id.iv_restaurant);
            holder.tvName = vi.findViewById(R.id.tv_name);
            holder.tvReview = vi.findViewById(R.id.tv_review_count);
            holder.ratingBar = vi.findViewById(R.id.iv_rating);

            vi.setTag(holder);
        }
        else {
            holder = (ViewHolder) vi.getTag();
        }

        //Glide
        if (restaurantDataArrayList.get(position).getDefaultImage() != null && !restaurantDataArrayList.get(position).getDefaultImage().isEmpty()) {
            Glide.with(mContext)
                    .setDefaultRequestOptions(requestOptions)
                    .load(RestClient.API_BASE_URL + "restaurant/image/" + String.valueOf(restaurantDataArrayList.get(position).getId())+ "/index/1")
                    .into(holder.imageView);
        }

      //  holder.tvReview.setText(mContext.getResources().getString(R.string.reviews,restaurantDataArrayList.get(position).getReviews()));
       holder.tvReview.setText(String.valueOf(restaurantDataArrayList.get(position).getReviews()) + " " + mContext.getString(R.string.review));

        if (Constant.language == 0)
        {
            holder.tvName.setText(restaurantDataArrayList.get(position).getName());
        }else {
            holder.tvName.setText(restaurantDataArrayList.get(position).getArabicName());
        }

        holder.ratingBar.setRating(restaurantDataArrayList.get(position).getRating());
        holder.ratingBar.setEnabled(false);
        return vi;
    }

    public static class ViewHolder {
        ImageView imageView;
        TextView tvName;
        TextView tvReview;
        SimpleRatingBar ratingBar;
    }
}
