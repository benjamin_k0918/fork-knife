package com.app.forkandknife.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.forkandknife.R;
import com.app.forkandknife.global.Constant;
import com.app.forkandknife.user.activity.MainActivity;
import com.app.forkandknife.user.fragment.BookingFragment;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class TimeAdapter  extends RecyclerView.Adapter<TimeAdapter.SimpleViewHolder>{

    private Context mContext;
    private ArrayList<String> timeArray;
    private String ruleArray;

    public TimeAdapter(Context mContext, ArrayList<String> timeArray,String arrayRule) {
        this.mContext = mContext;
        this.timeArray = timeArray;
        this.ruleArray = arrayRule;
    }

    @NonNull
    @Override
    public SimpleViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup,final int i) {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.item_time, viewGroup, false);



        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SimpleViewHolder holder, final int i) {


        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ruleArray.charAt(i) == '1')
                {
                    String dateTime = Constant.selectedDate + " " + timeArray.get(i);
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                    try {
                        Date date = format.parse(dateTime);
                        if (!new Date().after(date))
                        {
                            BookingFragment fragment = new BookingFragment();
                            FragmentManager fragmentManager=    ((MainActivity)mContext).getSupportFragmentManager();
                            FragmentTransaction fragmentTransaction =   fragmentManager.beginTransaction();
                            Bundle sendData = new Bundle();

                            sendData.putString("time",timeArray.get(i));
                            fragment.setArguments(sendData);

                            fragmentTransaction.replace(R.id.MainFrame,fragment,null);
                            fragmentTransaction.commit();
                        }
                        else {
                            Toast.makeText(mContext,mContext.getString(R.string.book_future),Toast.LENGTH_LONG).show();
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                       // Toast.makeText(mContext,e.getMessage(),Toast.LENGTH_LONG).show();
                    }



                }
            }
        });

        if (ruleArray.charAt(i) == '1')
        {
            holder.name.setBackground(mContext.getResources().getDrawable(R.drawable.ic_rounded_button_red));
            holder.name.setText(timeArray.get(i));
        }else {
            holder.name.setBackground(mContext.getResources().getDrawable(R.drawable.ic_rounded_button_gray));
            holder.name.setText("");
        }
    }

    @Override
    public int getItemCount() {
        return timeArray.size();
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        TextView name ;
        public SimpleViewHolder(View view) {
            super(view);
            this.name = view.findViewById(R.id.tv_time);
        }
    }
}