package com.app.forkandknife.adapter;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.app.forkandknife.R;
import com.app.forkandknife.model.ImageFileModel;

import java.util.ArrayList;

public class ImageSelectAdapter extends BaseAdapter {

    Context mContext;
    private ArrayList<ImageFileModel> imageFileModels;
    private LayoutInflater layoutInflater = null;
    private RequestOptions requestOptions = new RequestOptions();

    public ImageSelectAdapter(Context mContext, ArrayList<ImageFileModel> imageFileModels) {
        this.mContext = mContext;
        this.imageFileModels = imageFileModels;
    }

    @Override
    public int getCount() {
        return imageFileModels.size();
    }

    @Override
    public Object getItem(int position) {
        return imageFileModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        ViewHolder holder =  null;
        if (vi == null){
            holder = new ViewHolder();
            this.layoutInflater = (LayoutInflater)this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = this.layoutInflater.inflate(R.layout.item_gallery_image, null);
            holder.imageView = vi.findViewById(R.id.iv_image);
            holder.ivSelection= vi.findViewById(R.id.iv_selection);


            vi.setTag(holder);
        }
        else {
            holder = (ViewHolder) vi.getTag();
        }

        requestOptions.placeholder(R.drawable.reservation);
        requestOptions.error(R.drawable.reservation);
        Glide.with(mContext).setDefaultRequestOptions(requestOptions).load(imageFileModels.get(position).getUrl()).into(holder.imageView);
        if (imageFileModels.get(position).isSelection())
        {
            holder.ivSelection.setVisibility(View.VISIBLE);
        }else
        {
            holder.ivSelection.setVisibility(View.GONE);
        }

        return vi;
    }

    public static class ViewHolder {
        ImageView imageView;
        ImageView ivSelection;

    }
}
