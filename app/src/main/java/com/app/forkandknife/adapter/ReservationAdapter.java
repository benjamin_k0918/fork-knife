package com.app.forkandknife.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.forkandknife.R;
import com.app.forkandknife.global.Constant;
import com.app.forkandknife.model.ReservationData;

import java.util.ArrayList;

public class ReservationAdapter extends BaseAdapter {

    Context mContext;
    ArrayList<ReservationData> reservationDataArrayList;
    private LayoutInflater layoutInflater = null;

    public ReservationAdapter(Context mContext, ArrayList<ReservationData> reservationDataArrayList) {
        this.mContext = mContext;
        this.reservationDataArrayList = reservationDataArrayList;
    }

    @Override
    public int getCount() {
        return reservationDataArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return reservationDataArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        ViewHolder holder =  null;
        if (vi == null){
            holder = new ViewHolder();
            this.layoutInflater = (LayoutInflater)this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = this.layoutInflater.inflate(R.layout.item_reservation, null);

            holder.imageView = vi.findViewById(R.id.iv_image);

            holder.tvName = vi.findViewById(R.id.tv_name);
            holder.tvDate = vi.findViewById(R.id.tv_date);
            holder.tvStatus = vi.findViewById(R.id.tv_status);
            holder.tvDiscount = vi.findViewById(R.id.tv_discount);
            vi.setTag(holder);
        }
        else {
            holder = (ViewHolder) vi.getTag();
        }

        if (reservationDataArrayList.get(position).getStatus() ==1)
        {
            holder.tvStatus.setText(R.string.created);
        }else if (reservationDataArrayList.get(position).getStatus() ==2){
            holder.tvStatus.setText(R.string.canceled);
        }else if (reservationDataArrayList.get(position).getStatus() ==3){
            holder.tvStatus.setText(R.string.completed  );
        }else if (reservationDataArrayList.get(position).getStatus() ==4){
            holder.tvStatus.setText(R.string.accepted);
        }else if (reservationDataArrayList.get(position).getStatus() == 5)
        {
            holder.tvStatus.setText(R.string.rejected);
        }else if (reservationDataArrayList.get(position).getStatus() == 6)
        {
            holder.tvStatus.setText(R.string.closed);
        }

        holder.imageView.setImageResource(R.drawable.placeholder);

        holder.tvName.setText(reservationDataArrayList.get(position).getRestaurant().getName());
        holder.tvDate.setText(reservationDataArrayList.get(position).getReservationDate());
        holder.tvDiscount.setText(mContext.getString(R.string.discount) + " "  +String.valueOf(reservationDataArrayList.get(position).getDiscount()) + " " + mContext.getString(R.string.currency_sar) );

        //Glide

        return vi;
    }


    public static class ViewHolder {
        ImageView imageView;
        TextView tvName;
        TextView tvDate;
        TextView tvStatus;
        TextView tvDiscount;
    }
}
