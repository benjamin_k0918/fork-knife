package com.app.forkandknife.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestFutureTarget;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.squareup.picasso.Picasso;
import com.app.forkandknife.R;

import java.util.ArrayList;

public class RestaurantThumbAdapter extends RecyclerView.Adapter<RestaurantThumbAdapter.SimpleViewHolder>{

    private Context mContext;
    private ArrayList<String> imageArray;
    private RequestOptions requestOptions = new RequestOptions();

    public RestaurantThumbAdapter(Context mContext, ArrayList<String> imageArray) {
        this.mContext = mContext;
        this.imageArray = imageArray;
    }

    @NonNull
    @Override
    public SimpleViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.item_thumb, viewGroup, false);


        return new RestaurantThumbAdapter.SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SimpleViewHolder holder, final int i) {
       requestOptions.placeholder(R.drawable.thumb);
       requestOptions.error(R.drawable.thumb);
       Glide.with(mContext).setDefaultRequestOptions(requestOptions).load(imageArray.get(i)).listener(new RequestListener<Drawable>() {
            @Override
           public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                imageArray.set(i,"");
                Log.d("emptyplaceIs",imageArray.get(i));
                return false;
            }

           @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        }).into(holder.imageThumb);

//        Picasso.with(mContext).load(imageArray.get(i)).placeholder(R.drawable.thumb).error(R.drawable.thumb).into(holder.imageThumb);

    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        ImageView imageThumb;
        public SimpleViewHolder(View view) {
            super(view);
            this.imageThumb = view.findViewById(R.id.iv_thumb);
        }
    }
}
