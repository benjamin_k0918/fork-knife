package com.app.forkandknife.adapter;


import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.app.forkandknife.R;

import java.util.ArrayList;

public class CustomSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {

    private final Context activity;
    private ArrayList<String> asr;

    public CustomSpinnerAdapter(Context context, ArrayList<String> asr) {
        this.asr = asr;
        activity = context;
    }

    public int getCount() {
        return asr.size();
    }

    public Object getItem(int i) {
        return asr.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView txt = new TextView(activity);

        txt.setGravity(Gravity.CENTER | Gravity.CENTER_VERTICAL);
        txt.setPadding(0, 5, 8, 5);
        txt.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        txt.setGravity(Gravity.CENTER);
        txt.setHeight(70);
        txt.setWidth(70);
        txt.setMaxLines(1);
        txt.setText(asr.get(position));
        txt.setTextColor(Color.BLACK);


        return txt;
    }

    public View getView(int i, View view, ViewGroup viewgroup) {
        final TextView txt = new TextView(activity);
        txt.setGravity(Gravity.CENTER | Gravity.CENTER_VERTICAL);
        txt.setPadding(0, 5, 12, 5);
        txt.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        txt.setCompoundDrawablePadding(16);
        txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.drop_down_arrow, 0);
        txt.setText(asr.get(i));
        txt.setTextColor(Color.BLACK);
        return txt;
    }

}
