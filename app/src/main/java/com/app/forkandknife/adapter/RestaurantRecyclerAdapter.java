package com.app.forkandknife.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.forkandknife.webservice.RestClient;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.app.forkandknife.R;
import com.app.forkandknife.model.RestaurantModel;
import com.app.forkandknife.model.UserInfo;
import com.app.forkandknife.user.activity.MainActivity;
import com.app.forkandknife.user.fragment.BookingRestaurantFragment;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RestaurantRecyclerAdapter extends RecyclerView.Adapter<RestaurantRecyclerAdapter.SimpleViewHolder>{

    private Context mContext;
    private ArrayList<RestaurantModel> restaurantDataArrayList;
    private RequestOptions requestOptions = new RequestOptions();

    public RestaurantRecyclerAdapter(Context mContext, ArrayList<RestaurantModel> cuisineDataArrayList) {
        this.mContext = mContext;
        this.restaurantDataArrayList = cuisineDataArrayList;
        requestOptions.placeholder(R.drawable.placeholder);
        requestOptions.error(R.drawable.placeholder);
    }

    @NonNull
    @Override
    public SimpleViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
    final View view = LayoutInflater.from(mContext).inflate(R.layout.item_horizonal_restaurant, viewGroup, false);


            return new RestaurantRecyclerAdapter.SimpleViewHolder(view);    }

    @Override
    public void onBindViewHolder(@NonNull final SimpleViewHolder holder, final int i) {
        // Decode the filepath with BitmapFactory followed by the position
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BookingRestaurantFragment fragment = new BookingRestaurantFragment();
                FragmentManager fragmentManager=    ((MainActivity)mContext).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction =   fragmentManager.beginTransaction();
                UserInfo.getInstance().setCurrentRestaurant(restaurantDataArrayList.get(i));
                fragmentTransaction.replace(R.id.MainFrame,fragment,null);
                fragmentTransaction.commit();
            }
        });
        holder.imageView.setImageResource(R.drawable.placeholder);
        holder.tvReview.setText(String.valueOf(restaurantDataArrayList.get(i).getReviews()) + "  Reviews");
        holder.tvName.setText(restaurantDataArrayList.get(i).getName());

        holder.ratingBar.setRating((float)restaurantDataArrayList.get(i).getRating());
        holder.ratingBar.setEnabled(false);

        if (restaurantDataArrayList.get(i).getDefaultImage() != null && !restaurantDataArrayList.get(i).getDefaultImage().isEmpty()){
            String imageLink = RestClient.API_BASE_URL + "restaurant/image/" + String.valueOf(restaurantDataArrayList.get(i).getId())
                    + "/index/1";
            Glide.with(mContext).setDefaultRequestOptions(requestOptions).load(imageLink).into(holder.imageView);
        }
    }


    @Override
    public int getItemCount() {
            return restaurantDataArrayList.size();
            }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView tvName;
        TextView tvReview;
        SimpleRatingBar ratingBar;
        LinearLayout mainLayout;


        public SimpleViewHolder(View view) {
            super(view);
            this.imageView = view.findViewById(R.id.iv_restaurant);
            this.tvName = view.findViewById(R.id.tv_name);
            this.tvReview = view.findViewById(R.id.tv_review_count);
            this.ratingBar = view.findViewById(R.id.iv_rating);
            this.mainLayout = view.findViewById(R.id.mainLayout);
        }
    }
}
