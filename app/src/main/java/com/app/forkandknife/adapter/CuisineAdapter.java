package com.app.forkandknife.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.forkandknife.R;
import com.app.forkandknife.global.Constant;
import com.app.forkandknife.model.CuisineData;

import java.util.ArrayList;

public class CuisineAdapter extends RecyclerView.Adapter<CuisineAdapter.SimpleViewHolder>{

    private  Context mContext;
    private ArrayList<CuisineData> cuisineDataArrayList;

    public CuisineAdapter(Context mContext, ArrayList<CuisineData> cuisineDataArrayList) {
        this.mContext = mContext;
        this.cuisineDataArrayList = cuisineDataArrayList;
    }

    @NonNull
    @Override
    public SimpleViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.item_cuisine, viewGroup, false);


        return new CuisineAdapter.SimpleViewHolder(view);    }

    @Override
    public void onBindViewHolder(@NonNull final SimpleViewHolder holder, final int i) {
        if (Constant.language == 0)
        {
            holder.name.setText(cuisineDataArrayList.get(i).getName());
        }else {
            holder.name.setText(cuisineDataArrayList.get(i).getArabicName());
        }

        holder.layoutBackground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cuisineDataArrayList.get(i).getSelection())
                {
                    cuisineDataArrayList.get(i).setSelection(false);
                    holder.layoutBackground.setBackgroundResource(R.drawable.ic_circle_image);
                    holder.name.setTextColor(mContext.getResources().getColor(R.color.color_black));
                }else {
                    cuisineDataArrayList.get(i).setSelection(true);
                    holder.layoutBackground.setBackgroundResource(R.drawable.ic_circle_image_red);
                    holder.name.setTextColor(mContext.getResources().getColor(R.color.color_white));
                }
                notifyDataSetChanged();

            }
        });

        if (cuisineDataArrayList.get(i).getSelection())
        {
            holder.layoutBackground.setBackgroundResource(R.drawable.ic_circle_image_red);
            holder.name.setTextColor(mContext.getResources().getColor(R.color.color_white));

        }else {
            holder.layoutBackground.setBackgroundResource(R.drawable.ic_circle_image);
            holder.name.setTextColor(mContext.getResources().getColor(R.color.color_black));
        }
    }

    @Override
    public int getItemCount() {
        return cuisineDataArrayList.size();
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        TextView name ;
        LinearLayout layoutBackground;
        public SimpleViewHolder(View view) {
            super(view);
            this.name = view.findViewById(R.id.tv_name);
            this.layoutBackground = view.findViewById(R.id.layout_background);
        }
    }
}
