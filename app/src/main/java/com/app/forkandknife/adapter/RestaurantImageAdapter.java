package com.app.forkandknife.adapter;

import com.app.forkandknife.R;

import ss.com.bannerslider.adapters.SliderAdapter;
import ss.com.bannerslider.viewholder.ImageSlideViewHolder;

public class RestaurantImageAdapter extends SliderAdapter {

    @Override
    public int getItemCount() {
        return 3;
    }

    @Override
    public void onBindImageSlide(int position, ImageSlideViewHolder imageSlideViewHolder) {
        switch (position) {
            case 0:
                //http://guatecreative.com/colegios/uploads/slider/slider1.png
                imageSlideViewHolder.bindImageSlide(R.drawable.restaurant_logo);
                break;
            case 1:
                imageSlideViewHolder.bindImageSlide(R.drawable.restaurant_logo);
                break;
        }
    }
}
