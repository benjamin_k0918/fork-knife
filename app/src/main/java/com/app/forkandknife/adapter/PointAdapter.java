package com.app.forkandknife.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.forkandknife.R;
import com.app.forkandknife.global.Constant;
import com.app.forkandknife.model.PointData;

import java.util.ArrayList;

public class PointAdapter extends BaseAdapter{

    private Context mContext;
    private ArrayList<PointData> pointDataArrayList;

    private LayoutInflater layoutInflater = null;


    public PointAdapter(Context mContext, ArrayList<PointData> cuisineDataArrayList) {
        this.mContext = mContext;
        this.pointDataArrayList = cuisineDataArrayList;
    }

    @Override
    public int getCount() {
        return pointDataArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return pointDataArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        ViewHolder holder =  null;
        if (vi == null){
            holder = new ViewHolder();
            this.layoutInflater = (LayoutInflater)this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = this.layoutInflater.inflate(R.layout.item_point, null);

            holder.tvPoint = vi.findViewById(R.id.tv_point);
            holder.tvDescription = vi.findViewById(R.id.tv_description);

            vi.setTag(holder);
        }
        else {
            holder = (ViewHolder) vi.getTag();
        }

        if (Constant.language == 0)
        {
            holder.tvDescription.setText(  pointDataArrayList.get(position).getDescription());
            holder.tvPoint.setText("+" + String.valueOf(pointDataArrayList.get(position).getValue()) + mContext.getString(R.string.point));
        }else {
            holder.tvDescription.setText(pointDataArrayList.get(position).getArabicDescription());
            holder.tvPoint.setText("+" + String.valueOf(pointDataArrayList.get(position).getValue()) + mContext.getString(R.string.point));
        }


        return vi;
    }

    public static class ViewHolder {
       TextView tvPoint;
       TextView tvDescription;
    }
}
