package com.app.forkandknife.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.forkandknife.webservice.RestClient;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.app.forkandknife.R;
import com.app.forkandknife.global.Constant;
import com.app.forkandknife.model.RestaurantModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RestaurantSearchResultAdapter extends BaseAdapter {
    Context mContext;
    private ArrayList<RestaurantModel> restaurantDataArrayList;
    private LayoutInflater layoutInflater = null;
    private RequestOptions requestOptions = new RequestOptions();


    public RestaurantSearchResultAdapter(Context mContext, ArrayList<RestaurantModel> restaurantDataArrayList) {
        this.mContext = mContext;
        this.restaurantDataArrayList = restaurantDataArrayList;
        requestOptions.placeholder(R.drawable.placeholder);
        requestOptions.error(R.drawable.placeholder);
    }

    @Override
    public int getCount() {
        return restaurantDataArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return restaurantDataArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("StringFormatInvalid")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        ViewHolder holder =  null;
        if (vi == null){
            holder = new ViewHolder();
            this.layoutInflater = (LayoutInflater)this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = this.layoutInflater.inflate(R.layout.item_restaurant, null);

            holder.imageView = vi.findViewById(R.id.iv_restaurant);
            holder.tvName = vi.findViewById(R.id.tv_name);
            holder.tvReview = vi.findViewById(R.id.tv_review_count);
            holder.ratingBar = vi.findViewById(R.id.iv_rating);

            vi.setTag(holder);
        }
        else {
            holder = (ViewHolder) vi.getTag();
        }

        //Glide
        //     requestOptions.placeholder(R.drawable.img_photo_sample);
        //     requestOptions.error(R.drawable.img_photo_sample);
        // Set file name to the TextView followed by the position
        //  text.setText(CardArray.get(position).getBackImagepath());

        // Decode the filepath with BitmapFactory followed by the position
        holder.imageView.setImageResource(R.drawable.placeholder);
        holder.tvReview.setText(mContext.getResources().getString(R.string.reviews,restaurantDataArrayList.get(position).getReviews()));
        if (Constant.language == 0)
        {
            holder.tvName.setText(restaurantDataArrayList.get(position).getName());
        }else {
            holder.tvName.setText(restaurantDataArrayList.get(position).getArabicName());
        }

        holder.ratingBar.setRating(restaurantDataArrayList.get(position).getRating());
        holder.ratingBar.setEnabled(false);

        if (restaurantDataArrayList.get(position).getDefaultImage() != null && !restaurantDataArrayList.get(position).getDefaultImage().isEmpty()){

            String imageLink = RestClient.API_BASE_URL + "restaurant/image/" + String.valueOf(restaurantDataArrayList.get(position).getId())
                    + "/index/1";
            Glide.with(mContext).setDefaultRequestOptions(requestOptions).load(imageLink).into(holder.imageView);
        }

        return vi;
    }

    public static class ViewHolder {
        ImageView imageView;
        TextView tvName;
        TextView tvReview;
        SimpleRatingBar ratingBar;
    }
}
