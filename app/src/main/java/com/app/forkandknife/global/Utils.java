package com.app.forkandknife.global;

import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public  class Utils {
    public  static boolean isEmailValid(String strEmail) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(strEmail);
        return matcher.matches();
    }

    public static boolean isMobileValid(String mobile){
    //    if (mobile.length()<12)
        {
      //      return false;
        }
      //  else
            return true;
    }

    public static String formatNumber(int num){


        String str = String.format(new Locale("en","US"),"%02d", num);
        return  str;
    }

    public static String calendarToString(Calendar mCalendar){
        String strDate = "";
        strDate = formatNumber( mCalendar.YEAR) + ":" + formatNumber(mCalendar.MONTH) + ":" + formatNumber(mCalendar.DAY_OF_MONTH);
        return  strDate;
    }
}
