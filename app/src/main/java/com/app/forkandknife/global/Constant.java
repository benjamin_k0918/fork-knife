package com.app.forkandknife.global;


import com.app.forkandknife.model.LoginDataModel;
import com.app.forkandknife.model.RestaurantModel;
import com.app.forkandknife.model.UserData;

public class Constant {

    public static class CurrentFragmentType{
        public static int LoginFragment = 0x01;
        public static int RegisterFragment = 0x02;
        public static int ForgotPasswordFragment = 0x03;
        public static int SMSVerifyFragment = 0x04;
    }

    public static int currentFragment = 0x01;


    public static String ImageArray = "ImageArray";

    public static String strMobile = "mobile";
    public static String strPassword = "password";
    public static String strName = "name";
    public static String latitude = "latitude";
    public static String longtitude = "longtitude";
    public static String strEmail = "email";
    public static String strDeviceType = "deviceType";
    public static String strUserType = "userType";

    public static LoginDataModel loginDataMode = new LoginDataModel();

    public static String selectedDate = "";

    public static int selectedPoint = 0;
    public static int language = 0;

    public static boolean isReloaded  = false;

    public static String fbToken = "";
    public static String token = "";
    public static String accessToken = "";

    public static boolean isRestLoggedin = false;

    public static boolean isFirstTimeToOpenReservation = true;

}
