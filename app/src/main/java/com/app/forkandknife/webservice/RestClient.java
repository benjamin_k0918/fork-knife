package com.app.forkandknife.webservice;


import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class    RestClient {
    private static RestClient restClient = null;

    public static final String API_BASE_URL = "http://139.59.125.227:3001/";
    public static final String API_SMS_URL = "http://139.59.125.227:3001/sms/";
    public static final String API_URL = "http://139.59.125.227:3001/";

    private static API api;

    public static RestClient getInstance(){
        if (restClient == null){
            restClient = new RestClient();
        }

        return restClient;
    }

    /**
     * Constructor
     */
    private RestClient(){
    }

    /**
     * Initialize the Retrofit library
     */
    public API initRestClient(String baseUrl){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).readTimeout(10, TimeUnit.MINUTES)
                .connectTimeout(15, TimeUnit.MINUTES).build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        api = retrofit.create(API.class);

        return api;
    }

    public API getApi(){
        return api;
    }
}
