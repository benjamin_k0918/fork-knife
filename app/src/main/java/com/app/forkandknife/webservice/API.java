package com.app.forkandknife.webservice;


import android.support.annotation.Nullable;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.synnapps.carouselview.CarouselView;
import com.app.forkandknife.model.LimitationModel;
import com.app.forkandknife.model.LoginRequest;
import com.app.forkandknife.model.PointRequestModel;
import com.app.forkandknife.model.SmsVerifyModel;
import com.app.forkandknife.model.UserInfo;

import org.androidannotations.annotations.rest.Head;
import org.androidannotations.annotations.rest.Post;
import org.json.JSONObject;

import java.io.File;
import java.sql.Struct;
import java.util.Calendar;
import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface API {

    @POST("user/register")
    Call<JsonObject> registerUser(@Body UserInfo body);

    @POST("user/login")
    Call<JsonObject> login(@Body LoginRequest body);

    @GET("send/{user_id}")
    Call<JsonObject> sendSms(@Path("user_id")  int userId);

    @POST("verify")
    Call<JsonObject> verifySMS(@Body SmsVerifyModel body);

    @POST("reservation/list/top")
    Call<JsonObject> getTopRestaurant(@Header("Authorization") String head,@Body LimitationModel body);

    @POST("point/list")
    Call <JsonObject> getPointList(@Header("Authorization") String head,@Body PointRequestModel pointRequestModel);

    @POST("reservation")
    Call <JsonObject> reservation(@Header("Authorization") String head,@Body Map requestJson);

    @POST("user/resetPassword")
    Call <JsonObject> changePassword(@Body Map requestMap);

    @POST("user/changePassword")
    Call <JsonObject> changeRestaurantPassword(@Body Map requestMap);

    @POST("cuisine/list")
    Call <JsonObject> getAllCuisine();

    @POST("cuisine/listByRestaurant")
    Call <JsonObject> getCusineByRestaurant();

    @POST("restaurant/list")
    Call<JsonObject>  getAllRestaurtant(@Header("Authorization") String head,@Body Map requestMap);


    @POST("restaurant/search")
    Call<JsonObject>  searchRestaurant(@Header("Authorization") String head,@Body Map requestMap);

    @POST("reservation/list/user")
    Call<JsonObject> searchAllReservations(@Header("Authorization") String head,@Body Map requestMap);

    @POST("reservation/cancel")
    Call<JsonObject> cancelReservation(@Header("Authorization") String head,@Body Map requestMap );

    @POST("review/new")
    Call<JsonObject> reviewRestaurant(@Header("Authorization") String head,@Body Map requestMap );

    @POST("contact/new")
    Call<JsonObject> contactSupport(@Header("Authorization") String head,@Body Map requestMap );

    @POST("user/updateProfile")
    Call <JsonObject> changeProfile(@Header("Authorization") String head,@Body Map requestMap);

    @Multipart
    @POST("/user/image/{user_id}")
    Call<JsonObject> uploadImage(@Header("Authorization") String head,@Path("user_id") int userId,@Part MultipartBody.Part file);

    @POST("restaurantPhoto/list")
    Call<JsonObject> getAllRestaurantImage(@Header("Authorization") String head,@Body Map requestMap);


    @POST("reservation/user/hide")
    Call<JsonObject> removeReservation(@Header("Authorization") String head,@Body Map requestMap);

    @POST("reservation/list/restaurant")
    Call<JsonObject> searchAllReservationsRestaurant(@Body Map requestMap);

    @POST("reservation/reject")
    Call<JsonObject> rejectReservation(@Header("Authorization") String head,@Body Map requestMap);

    @POST("reservation/accept")
    Call<JsonObject> acceptReservation (@Header("Authorization") String head,@Body Map requestMap);

    @POST("reservation/complete")
    Call<JsonObject> completeReseravtion (@Header("Authorization") String head,@Body Map requestMap);

    @POST("restaurant/new")
    Call<JsonObject> createRestaurant (@Body Map requestMap);

    @POST("restaurant/enable/{restaurant_id}")
    Call<JsonObject> enableRestaurant(@Path("restaurant_id")  int restaurantId);

    @POST("restaurant/busy/{restaurant_id}")
    Call<JsonObject> makeRestaurantBusy(@Path("restaurant_id")  int restaurantId);

    @Multipart
    @POST("restaurant/image/{restaurant_id}")
    Call<JsonObject> uploadRestImage(@Header("Authorization") String head,@Path("restaurant_id") String restId ,@Part MultipartBody.Part[] file);

    @POST("restaurant/hours")
    Call<JsonObject> updateRestHours(@Header("Authorization") String head,@Body Map reqeustMap);

    @POST("restaurant/updateProfile")
    Call<JsonObject> updateRestProfile(@Header("Authorization") String head,@Body Map reqeustMap);

    @GET("restaurant/owner/{user_id}")
    Call<JsonObject> getRestaurantByOwner(@Header("Authorization") String head,@Path("user_id") int user_id);


    @POST("/reservation/restaurant/hide")
    Call<JsonObject> hideReservation(@Header("Authorization") String head,@Body Map requestMap);

    @POST("restaurant/searchByLocation")
    Call<JsonObject> searchByLocation(@Header("Authorization") String head,@Body Map requestMap);
}
