package com.app.forkandknife.user.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.app.forkandknife.AccountManageActivity;
import com.facebook.login.LoginManager;
import com.google.gson.JsonObject;
import com.app.forkandknife.R;
import com.app.forkandknife.adapter.CustomSpinnerAdapter;
import com.app.forkandknife.global.BaseFragment;
import com.app.forkandknife.global.Constant;
import com.app.forkandknife.model.UserInfo;
import com.app.forkandknife.webservice.RestClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookingFragment extends BaseFragment {
    int currentValie;

    @BindView(R.id.editText_guest_number)
    EditText editTextGuestNumber;

    @BindView(R.id.editText_date)
    EditText editTextDate;

    @BindView(R.id.editText_time)
    EditText editTextTime;

    private Calendar mCalendar;

    @BindView(R.id.editText_comment)
    EditText editTextComment;

    @BindView(R.id.iv_select_date)
    ImageView ivSelectDate;
    WindowManager.LayoutParams  layoutParams;

    @BindView(R.id.spinner_point)
    Spinner pointSpinner;

    DatePickerDialog.OnDateSetListener dateSetListener;
    ArrayList<String> pointArray ;
    CustomSpinnerAdapter customSpinnerAdapter;
    HashMap<String, Object> map = new HashMap<>();
    SharedPreferences preferences;
    ProgressDialog progressDialog ;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View containerView=inflater.inflate(R.layout.fragment_booking, container, false);
        ButterKnife.bind(this,containerView);

        progressDialog = new ProgressDialog(getActivity());
        editTextGuestNumber.setClickable(false);
        editTextGuestNumber.setEnabled(false);
        layoutParams = new WindowManager.LayoutParams();
        layoutParams.gravity = Gravity.BOTTOM;

        mCalendar = Calendar.getInstance();
        editTextDate.setText(Constant.selectedDate);

        String time = getArguments().getString("time");
        editTextTime.setText(time);
        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mCalendar.set(Calendar.YEAR, year);
                mCalendar.set(Calendar.MONTH, month);
                mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateDate();
            }
        };

        pointArray = new ArrayList<>();
        for (int i =0;i< UserInfo.getInstance().getPointData().size();i++)
        {
            pointArray.add(String.valueOf(UserInfo.getInstance().getPointData().get(i).getValue()));
        }

        customSpinnerAdapter = new CustomSpinnerAdapter(getActivity(),pointArray);
        pointSpinner.setAdapter(customSpinnerAdapter);
        // Should control the selected points
        pointSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d("Booking", "Selected points = " + pointArray.get(i));
                if (Integer.parseInt(pointArray.get(i)) > UserInfo.getInstance().getLoginDataModel().getPoints())
                {
                    // Show points of loyalty error message
                    Toast.makeText(getActivity(),getString(R.string.not_enough_points),Toast.LENGTH_LONG).show();
                    // set it into Zero again
                    pointSpinner.setSelection(0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        return containerView;
    }

    @OnClick(R.id.iv_back)
    public void onBack(){

    }

    @OnClick(R.id.MainLayout)
    public void onMainLayoutClicked(){
        hideKeyboard();
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @OnClick(R.id.iv_back)
    public void onBackClicked(){
        setFragment(new BookingRestaurantFragment());
    }

    @Override
    public void onResume() {
        super.onResume();

        if(getView() == null){
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    // handle back button's click listener
                    setFragment(new BookingRestaurantFragment());

                    return true;
                }
                return false;
            }
        });
    }

    @OnClick(R.id.btn_book)
    public void onBookButtonClicked(){
        // Show the booking confirm dialog
        showDialog();
    }

    private void showDialog(){

        {
            final Dialog dialog = new Dialog(getActivity());
            final View view  = getActivity().getLayoutInflater().inflate(R.layout.dialog_confirm_book, null);
            Window window = dialog.getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();
            wlp.dimAmount = 0.7f;
            wlp.gravity = Gravity.BOTTOM;
            //   wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
            window.setAttributes(wlp);

            Button btnCancel = view.findViewById(R.id.btr_no);
            Button btnConfirm= view.findViewById(R.id.btn_yes);

            // Cancel
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            // Book
            btnConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    bookRestaurant();
                }
            });
            dialog.getWindow();
            dialog.setContentView(view);

            dialog.show();
        }



    }

    // Book the restaurant
    private void bookRestaurant(){
        String auth = "Bearer " + UserInfo.getInstance().getLoginDataModel().getAccessToken();
        map.put("userId",UserInfo.getInstance().getLoginDataModel().getId());
        map.put("restaurantId",UserInfo.getInstance().getCurrentRestaurant().getId());
        map.put("replacePoints",Integer.parseInt(pointSpinner.getSelectedItem().toString()));
        map.put("numberOfPeople",Integer.parseInt(editTextGuestNumber.getText().toString().trim()));
        map.put("reservationDate",editTextDate.getText().toString() + " " + editTextTime.getText().toString());
        map.put("comment",editTextComment.getText().toString().trim());
      //  map.put("token",UserInfo.getInstance().getLoginDataModel().getAccessToken());

        // show the progress dialog
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();

        RestClient.getInstance().getApi().reservation(auth,map).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                progressDialog.dismiss();
                if (response.code() == 200)
                {
                    setFragment(new ReservationFragment());
                }  else if (response.code() == 401)
                {
                    SharedPreferences preferences;
                    preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor =      preferences.edit();
                    editor.putString("login", "0");
                    editor.commit();
                    Constant.isReloaded = false;
                    if (!Constant.fbToken.isEmpty())
                    {
                        LoginManager.getInstance().logOut();
                    }
                    Intent intent = new Intent(getActivity(), AccountManageActivity.class);
                    startActivity(intent);

                    getActivity().finish();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    private void setFragment(Fragment fragment){
      FragmentManager fragmentManager= getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =   fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.MainFrame,fragment,null);
        fragmentTransaction.commit();
    }

    @OnClick(R.id.layout_plus)
    public void onPlusClicked(){
        if (!editTextGuestNumber.getText().toString().trim().isEmpty())
        {
            currentValie = Integer.parseInt(editTextGuestNumber.getText().toString().trim())+1;
            editTextGuestNumber.setText(String.valueOf(currentValie));
        }
    }


    @OnClick(R.id.layout_minus)
    public void onMinusClicked(){
        if (!editTextGuestNumber.getText().toString().trim().isEmpty())
        {
            currentValie = Integer.parseInt(editTextGuestNumber.getText().toString().trim())-1;
            if (currentValie >0){
                editTextGuestNumber.setText(String.valueOf(currentValie));
            }
        }
    }


    public void updateDate(){
        String myFormat = "MM/dd/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        editTextDate.setText(sdf.format(mCalendar.getTime()));
    }

}
