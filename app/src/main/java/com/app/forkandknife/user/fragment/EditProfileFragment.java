package com.app.forkandknife.user.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.app.forkandknife.AccountManageActivity;
import com.facebook.login.LoginManager;
import com.google.gson.JsonObject;
import com.app.forkandknife.R;
import com.app.forkandknife.global.BaseFragment;
import com.app.forkandknife.global.Constant;
import com.app.forkandknife.global.Utils;
import com.app.forkandknife.model.UserInfo;
import com.app.forkandknife.webservice.RestClient;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileFragment extends BaseFragment {

    @BindView(R.id.editText_full_name)
    EditText editTextFullName;

    @BindView(R.id.editText_email)
    EditText editTextEmail;

    @BindView(R.id.editText_mobile)
    EditText editTextMobile;

    ProgressDialog progressDialog;
    SharedPreferences preferences;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View containerView=inflater.inflate(R.layout.fragment_edit_profile, container, false);
        ButterKnife.bind(this,containerView);

        editTextEmail.setText(UserInfo.getInstance().getLoginDataModel().getEmail());
        editTextFullName.setText(UserInfo.getInstance().getLoginDataModel().getName());
        editTextMobile.setText(UserInfo.getInstance().getLoginDataModel().getMobile());

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.loading));

        return containerView;
    }

    private boolean isAllFieldValid() {
        if (editTextMobile.getText().toString().length() == 12 && !editTextFullName.getText().toString().trim().isEmpty() && Utils.isEmailValid(editTextEmail.getText().toString().trim()))
        {
            return true;
        }else if (!Utils.isEmailValid(editTextEmail.getText().toString().trim())){
            Toast.makeText(getActivity(),getString(R.string.ivalid_email),Toast.LENGTH_LONG).show();
            return false;
        }else if (editTextMobile.length()<12)
        {
            Toast.makeText(getActivity(),getString(R.string.enter_phone),Toast.LENGTH_LONG).show();

            return false;
        }else {
            Toast.makeText(getActivity(),getString(R.string.invalied_userName),Toast.LENGTH_LONG).show();

            return false;
        }

    }
    @OnClick(R.id.MainLayout)
    public void onMainLayoutClicked(){
        hideKeyboard();
    }

    private void updateProfile(){
        if (isAllFieldValid())
        {
            String auth = "Bearer " + UserInfo.getInstance().getLoginDataModel().getAccessToken();

            HashMap<String,Object> map = new HashMap<>();
            map.put("mobile",editTextMobile.getText().toString().trim());
            map.put("name",editTextFullName.getText().toString().trim());
            map.put("email",editTextEmail.getText().toString().trim());
            map.put("status","1");
            map.put("userId",UserInfo.getInstance().getLoginDataModel().getId());
            map.put("points", Constant.selectedPoint);
        //    map.put("token",UserInfo.getInstance().getLoginDataModel().getAccessToken());

            progressDialog.show();
            RestClient.getInstance().initRestClient(RestClient.API_URL).changeProfile(auth,map).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    progressDialog.dismiss();
                    JsonObject resultObj = response.body();
                        if (response.code() == 200) {
                            setFragment(new ProfileFragment());
                        }else if (response.code() == 401)
                        {
                            SharedPreferences preferences;
                            preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor =      preferences.edit();
                            editor.putString("login", "0");
                            editor.commit();
                            Constant.isReloaded = false;
                            if (!Constant.fbToken.isEmpty())
                            {
                                LoginManager.getInstance().logOut();
                            }
                            Intent intent = new Intent(getActivity(), AccountManageActivity.class);
                            startActivity(intent);

                            getActivity().finish();
                        }

                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }

    }




    @OnClick(R.id.tv_save)
    public void onSaveClicked(){
        updateProfile();
    }

    @OnClick(R.id.iv_back)
    public void onBackClicked(){
        setFragment(new ProfileFragment());
    }

    private void setFragment(Fragment fragment){
        FragmentManager fragmentManager= getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =   fragmentManager.beginTransaction();
        Bundle sendData = new Bundle();
        fragmentTransaction.replace(R.id.MainFrame,fragment,null);
        fragmentTransaction.commit();
    }

    @Override
    public void onResume() {
        super.onResume();

        if(getView() == null){
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    // handle back button's click listener
                    setFragment(new ProfileFragment());

                    return true;
                }
                return false;
            }
        });
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
