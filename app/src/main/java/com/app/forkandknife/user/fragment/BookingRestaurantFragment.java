package com.app.forkandknife.user.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.app.forkandknife.AccountManageActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;
import com.app.forkandknife.R;
import com.app.forkandknife.adapter.TimeAdapter;

import com.app.forkandknife.global.BaseFragment;
import com.app.forkandknife.global.Constant;
import com.app.forkandknife.global.Utils;
import com.app.forkandknife.model.FeatureData;
import com.app.forkandknife.model.ImageModel;

import com.app.forkandknife.model.UserInfo;
import com.app.forkandknife.webservice.RestClient;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ss.com.bannerslider.Slider;

public class BookingRestaurantFragment extends BaseFragment {

    @BindView(R.id.carouselView)
    CarouselView carouselView;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    RecyclerView.LayoutManager layoutManager;
    ArrayList<String> timeArray;
    TimeAdapter adapter ;

    @BindView(R.id.layout_bookfuture)
    LinearLayout bookFurure;

    @BindView(R.id.iv_rating)
    SimpleRatingBar ratingBar;

    @BindView(R.id.tv_open_time)
    TextView tvOpenTime;

    @BindView(R.id.tv_review_count)
    TextView tvReviewCount;

    @BindView(R.id.tv_rating_detail)
    SimpleRatingBar ratingDetail;

    @BindView(R.id.tv_score)
    TextView tvScore;

    @BindView(R.id.tv_close_time)
    TextView tvCloseTime;
    ArrayList<String> times = new ArrayList<>(); // time array

    @BindView(R.id.tv_feature)
            TextView tvFeature;


    String imageBaseURl  =RestClient.API_URL+  "restaurant/image/";

    int weekday = 0;
    private RequestOptions requestOptions = new RequestOptions();
    String[] restImageList ;
    ArrayList<ImageModel> imageModels = new ArrayList<>();

    @BindView(R.id.tv_loaction)
    TextView tvLocation;

    @BindView(R.id.tv_cost)
    TextView tvCost;


    @BindView(R.id.tv_name)
    TextView tvName;

    @BindView(R.id.tv_distance)
    TextView tvDistance;

    @BindView(R.id.iv_arrow)
    ImageView ivArrow;

    ProgressDialog progressDialog;


    private Calendar mCalendar;
    WindowManager.LayoutParams  layoutParams;
    DatePickerDialog.OnDateSetListener dateSetListener;
    ArrayList<String> arrayListDayOfRule = new ArrayList<>();
    DatePickerDialog datePickerDialog;

    ArrayList<String> featureDataArrayList = new ArrayList<>();
    SharedPreferences preferences;


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View containerView=inflater.inflate(R.layout.fragment_booking_restaurant, container, false);
        ButterKnife.bind(this,containerView);
        displayTimeSlots();

        progressDialog=  new ProgressDialog(getActivity());

        requestOptions.placeholder(R.drawable.restaurant_logo);
        requestOptions.error(R.drawable.restaurant_logo);

        if (Constant.language ==1)
        {
            tvName.setText(UserInfo.getInstance().getCurrentRestaurant().getArabicName());
            ivArrow.setVisibility(View.GONE);
        }else {
            tvName.setText(UserInfo.getInstance().getCurrentRestaurant().getName());
            ivArrow.setVisibility(View.VISIBLE);
        }
        getAllRestaurantImage();

        Calendar c = Calendar.getInstance();
        final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd",new Locale("en"));
        Constant.selectedDate = df.format(c.getTime());

        timeArray = new ArrayList<>();
        mCalendar = Calendar.getInstance();
        ratingBar.setRating(UserInfo.getInstance().getCurrentRestaurant().getRating());

        ratingDetail.setRating(UserInfo.getInstance().getCurrentRestaurant().getRating());
     //   tvScore.setText(getString(R.string.scores,UserInfo.getInstance().getCurrentRestaurant().getRating()));
        tvScore.setText(String.valueOf(UserInfo.getInstance().getCurrentRestaurant().getRating()) + " " + getString(R.string.score));

    //    tvReviewCount.setText(getString(R.string.reviews,UserInfo.getInstance().getCurrentRestaurant().getReviews()));

        tvReviewCount.setText(String.valueOf(UserInfo.getInstance().getCurrentRestaurant().getReviews()) + " " + getString(R.string.review));

        tvCloseTime.setText(UserInfo.getInstance().getCurrentRestaurant().getCloseTime());
        tvOpenTime.setText(UserInfo.getInstance().getCurrentRestaurant().getOpenTime());
        //init carousel

        Location loc1 = new Location("");
        loc1.setLatitude(Double.parseDouble(UserInfo.getInstance().getLatitude()));
        loc1.setLongitude(Double.parseDouble(UserInfo.getInstance().getLongtitude()));

        Location loc2 = new Location("");
        loc2.setLatitude(Double.parseDouble(UserInfo.getInstance().getCurrentRestaurant().getLatitude()));
        loc2.setLongitude(Double.parseDouble(UserInfo.getInstance().getCurrentRestaurant().getLongitude()));
        float disinKm = loc1.distanceTo(loc2) / 1000;

        tvDistance.setText(String.format("%.1f", disinKm) + " km");

        featureDataArrayList.add("Allow Smoking, ");
        featureDataArrayList.add("Accept Visa, ");
        featureDataArrayList.add("Dress Code, ");
        featureDataArrayList.add("Outdoor Seating, ");
        featureDataArrayList.add("Valet Parking");

        String featureText = "";
        if (UserInfo.getInstance().getInstance().getCurrentRestaurant().getAllowSmoking() ==  1)
        {
            featureText += featureDataArrayList.get(0);
        }
        if (UserInfo.getInstance().getInstance().getCurrentRestaurant().getAcceptVisa() ==  1)
        {
            featureText += featureDataArrayList.get(1);
        }
        if (UserInfo.getInstance().getInstance().getCurrentRestaurant().getDressCode() ==  1)
        {
            featureText += featureDataArrayList.get(2);
        } if (UserInfo.getInstance().getInstance().getCurrentRestaurant().getOutdoorSeating() ==  1)
        {
            featureText += featureDataArrayList.get(3);
        } if (UserInfo.getInstance().getInstance().getCurrentRestaurant().getParking() ==  1)
        {
            featureText += featureDataArrayList.get(4);
        }

        tvFeature.setText(featureText);
//        tvCost.setText(getString(R.string.sar,Integer.parseInt(UserInfo.getInstance().getCurrentRestaurant().getCost())));
        tvCost.setText(String.valueOf(UserInfo.getInstance().getCurrentRestaurant().getCost()) + " " + getString(R.string.currency_sar));

        getLocationFromLatLng();

        layoutParams = new WindowManager.LayoutParams();
        layoutParams.gravity = Gravity.BOTTOM;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());



        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                Calendar c = Calendar.getInstance();
                final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd",new Locale("en"));
                Constant.selectedDate = df.format(c.getTime());
                mCalendar.set(Calendar.YEAR, year);
                mCalendar.set(Calendar.MONTH, month);
                mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                weekday = mCalendar.get(Calendar.DAY_OF_WEEK);
                adapter = new TimeAdapter(getActivity(),times,arrayListDayOfRule.get(weekday-1));
                recyclerView.setAdapter(adapter);
                Constant.selectedDate = df.format(mCalendar.getTime());
            }
        };


        setRecyclerView();
        return  containerView;
    }

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            imageView.setAdjustViewBounds(true);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            Glide.with(getActivity()).setDefaultRequestOptions(requestOptions).load(restImageList[position]).into(imageView);

        }
    };

    @OnClick(R.id.MainLayout)
    public void onMainLayoutClicked(){
        hideKeyboard();
    }



    private void getLocationFromLatLng(){
        Geocoder geocoder;

        geocoder = new Geocoder(getActivity(), Locale.getDefault());

        try {
            List<Address> addresses = geocoder.getFromLocation(Double.parseDouble(UserInfo.getInstance().getCurrentRestaurant().getLatitude()), Double.parseDouble(UserInfo.getInstance().getCurrentRestaurant().getLongitude()), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            if (addresses.size()>0)
            {
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();
                tvLocation.setText(address);
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getAllRestaurantImage(){
        String auth = "Bearer " + UserInfo.getInstance().getLoginDataModel().getAccessToken();

        HashMap<String,Object> map =  new HashMap<>();
        map.put("restaurantId",UserInfo.getInstance().getCurrentRestaurant().getId());
     //   map.put("token",UserInfo.getInstance().getLoginDataModel().getAccessToken());
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();
        RestClient.getInstance().initRestClient(RestClient.API_URL).getAllRestaurantImage(auth,map).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                JsonObject resultObj = response.body();
                progressDialog.dismiss();

                if (response.code() == 200){
                    JsonArray userJson = resultObj.getAsJsonArray("data");
                    Gson gson = new Gson();
                    Type type = new TypeToken<List<ImageModel>>() {}.getType();
                    imageModels = gson.fromJson(userJson,type);
                    if (!imageModels.isEmpty()){
                        restImageList = new String[imageModels.size()];
                        for (int i = 0;i<imageModels.size();i++)
                        {
                            restImageList[i]= imageBaseURl +UserInfo.getInstance().getCurrentRestaurant().getId() + "/index/" + imageModels.get(i).getIndex();
                        }
                    }else {
                        restImageList = new String[1];

                    }
                    carouselView.setImageListener(imageListener);
                    carouselView.setPageCount(restImageList.length);
                }else if (response.code() == 401)
                {
                    SharedPreferences preferences;
                    preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor =      preferences.edit();
                    editor.putString("login", "0");
                    editor.commit();
                    Constant.isReloaded = false;
                    if (!Constant.fbToken.isEmpty())
                    {
                        LoginManager.getInstance().logOut();
                    }
                    Intent intent = new Intent(getActivity(), AccountManageActivity.class);
                    startActivity(intent);

                    getActivity().finish();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }



    private int getWeekdayOfToday(){
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        return  day;
    }

    private void setRecyclerView(){
        arrayListDayOfRule = new ArrayList<>();
        arrayListDayOfRule.add(UserInfo.getInstance().getCurrentRestaurant().getSunday());
        arrayListDayOfRule.add(UserInfo.getInstance().getCurrentRestaurant().getMonday());
        arrayListDayOfRule.add(UserInfo.getInstance().getCurrentRestaurant().getTuesday());
        arrayListDayOfRule.add(UserInfo.getInstance().getCurrentRestaurant().getWednesday());
        arrayListDayOfRule.add(UserInfo.getInstance().getCurrentRestaurant().getThursday());
        arrayListDayOfRule.add(UserInfo.getInstance().getCurrentRestaurant().getFriday());
        arrayListDayOfRule.add(UserInfo.getInstance().getCurrentRestaurant().getSaturday());
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

        adapter = new TimeAdapter(getActivity(),times,arrayListDayOfRule.get(getWeekdayOfToday()-1));

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void displayTimeSlots(){
        int x = 5; //minutes interval
        int tt = 0; // start time
     //   var ap = ['AM', 'PM']; // AM-PM
//loop to increment the time and push results in array
        for (int i=0;tt<24*60; i++) {
            int hh = tt/60; // getting hours of day in 0-24 format
            int mm = (tt%60); // getting minutes of the hour in 0-55 format
            times.add( Utils.formatNumber(hh%24) + ":" + Utils.formatNumber(mm));
         //   times[i] = ("0" + (hh % 12)).slice(-2) + ':' + ("0" + mm).slice(-2) + ap[Math.floor(hh/12)]; // pushing data in array in [00:00 - 12:00 AM/PM format]
            tt = tt + 15;
        }
      //  Toast.makeText(getActivity(),String.valueOf(times.size()),Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.recyclerView)
    public void onRecyclerViewClicked(){
        setFragment(new BookingFragment());
    }


    @OnClick(R.id.iv_back)
    public void onBackClicked(){
        setFragment(new MainFragment());
    }

    @Override
    public void onResume() {
        super.onResume();

        if(getView() == null){
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    // handle back button's click listener
                    setFragment(new MainFragment());

                    return true;
                }
                return false;
            }
        });
    }

    @OnClick(R.id.layout_bookfuture)
    public void onBookFutureClicked(){
    //    setFragment(new BookingFragment());
        Locale locale = new Locale("en");
        Resources res = getResources();
        Configuration conf = res.getConfiguration();
        DisplayMetrics dm = res.getDisplayMetrics();
        conf.locale = locale;
        Calendar c = Calendar.getInstance();
        res.updateConfiguration(conf, dm);
         datePickerDialog =
                new DatePickerDialog( getActivity(), dateSetListener,
                        mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());

         if (Constant.language == 1)
         {
             datePickerDialog.setButton(DatePickerDialog.BUTTON_POSITIVE, getString(R.string.ok_ar), datePickerDialog);
             datePickerDialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, getString(R.string.cancel_ar), datePickerDialog);
         }else {
             datePickerDialog.setButton(DatePickerDialog.BUTTON_POSITIVE, getString(R.string.ok_en), datePickerDialog);
             datePickerDialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, getString(R.string.cancel_en), datePickerDialog);
         }
         datePickerDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
             @Override
             public void onDismiss(DialogInterface dialog) {
                 if (Constant.language == 1)
                 {
                     Locale locale = new Locale("ar");
                     Locale.setDefault(locale);
                     Configuration config = new Configuration();
                     config.locale = locale;
                     getActivity().getBaseContext().getResources().updateConfiguration(config, getActivity().getBaseContext().getResources().getDisplayMetrics());
                     }
                 else {
                     Locale locale = new Locale("en");
                     Locale.setDefault(locale);
                     Configuration config = new Configuration();
                     config.locale = locale;
                     getActivity().getBaseContext().getResources().updateConfiguration(config, getActivity().getBaseContext().getResources().getDisplayMetrics());
                 }
             }
         });


        datePickerDialog.getWindow().setAttributes(layoutParams);
        datePickerDialog.show();

    }

    @OnClick(R.id.layout_rate)
    public void onRateClicked(){
        RateRestaurantFragment.formerFragment = 1;
        setFragment(new RateRestaurantFragment());
    }

    @OnClick(R.id.layout_share)
    public void onShareClicked(){
        Intent i=new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT,"Subject test");
        i.putExtra(Intent.EXTRA_TEXT, "extra text that you want to put");
        startActivity(Intent.createChooser(i,"Share via"));
    }

    private void showDialog(){

        final Dialog dialog = new Dialog(getActivity());
        final View view  = getActivity().getLayoutInflater().inflate(R.layout.dialog_calendar, null);

        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(view);
        Window window = dialog.getWindow();

        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.BOTTOM;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        Button btnCancel = view.findViewById(R.id.btn_cancel);
        Button btnSelect= view.findViewById(R.id.btn_select);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    private void setFragment(Fragment fragment){
        FragmentManager fragmentManager= getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =   fragmentManager.beginTransaction();
        Bundle sendData = new Bundle();
        fragmentTransaction.replace(R.id.MainFrame,fragment,null);
        fragmentTransaction.commit();
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
