package com.app.forkandknife.user.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.app.forkandknife.AccountManageActivity;
import com.app.forkandknife.global.Constant;
import com.facebook.login.LoginManager;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.app.forkandknife.R;
import com.app.forkandknife.adapter.CuisineAdapterMain;
import com.app.forkandknife.adapter.RestaurantAdapter;
import com.app.forkandknife.custom.ExpandableHeightGridView;
import com.app.forkandknife.custom.MyMapView;
import com.app.forkandknife.global.BaseFragment;
import com.app.forkandknife.model.CuisineData;
import com.app.forkandknife.model.LimitationModel;
import com.app.forkandknife.model.PointData;
import com.app.forkandknife.model.PointRequestModel;
import com.app.forkandknife.model.RestaurantModel;
import com.app.forkandknife.model.UserInfo;
import com.app.forkandknife.webservice.RestClient;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;


public class MainFragment extends BaseFragment implements OnMapReadyCallback {

    MyMapView mapView;
    GoogleMap map;

    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 1001;
    private static final String TAG = "MapsActivity";
    private String[] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};

    private static final double RIYAD_COOR_LATITUDE = 24.7136;
    private static final double RIYAD_COOR_LONGITUDE = 46.6753;

    @BindView(R.id.listView)
    ExpandableHeightGridView listView;
    RestaurantAdapter restaurantAdapter;
    ArrayList<CuisineData> cuisineDataArrayList;
    LinearLayoutManager layoutManager;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    CuisineAdapterMain cuisineAdapter;
    MarkerOptions markerOptions;
    MarkerOptions markerOptions2;
    MarkerOptions markerOptions3;
    MarkerOptions currentMakerOptions;
    ProgressDialog progressDialog;
    ArrayList<PointData> pointDataArrayList = new ArrayList<>();


    ArrayList<RestaurantModel> restaurantModels = new ArrayList<>();

    LatLng latLng = new LatLng(0, 0);

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        MapsInitializer.initialize(getApplicationContext());

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View containerView = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, containerView);
        setPreference();
        progressDialog = new ProgressDialog(getActivity());
        getTopRestaurant();
        if (UserInfo.getInstance().getCuisineDataArrayList().isEmpty()) {
            getAllCuisineList();
        } else {
            setRecyclerView();
        }

        LatLng location = getCurrentLocation();
        UserInfo.getInstance().setLatitude(String.valueOf(location.latitude));
        UserInfo.getInstance().setLongtitude(String.valueOf(location.longitude));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                UserInfo.getInstance().setCurrentRestaurant(restaurantModels.get(position));
                setFragment(new BookingRestaurantFragment());
            }
        });


        // Gets the MapView from the XML layout and creates it
        mapView = (MyMapView) containerView.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);

        checkLocationPermissions();


        return containerView;
    }

    private void getAllRestaurant(){
        progressDialog.show();
        String auth = "Bearer " + UserInfo.getInstance().getLoginDataModel().getAccessToken();

        HashMap<String,Object> map = new HashMap<>();
        map.put("pageNumber",1);
        map.put("pageSize",100);
        RestClient.getInstance().initRestClient(RestClient.API_URL).getAllRestaurtant(auth,map).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                ArrayList<RestaurantModel> restaurantModels = new ArrayList<>();
                JsonObject resultObj = response.body();
                progressDialog.dismiss();
                if (response.code() == 200){
                    JsonArray restaurantJson = resultObj.getAsJsonObject("data").getAsJsonArray("rows");
                    Gson gson = new Gson();
                    Type type = new TypeToken<List<RestaurantModel>>() {}.getType();
                    restaurantModels = gson.fromJson(restaurantJson,type);

                    UserInfo.getInstance().setAllRestModel(restaurantModels);
                    //    EventBus.getDefault().post(new EventBusMessage(EventBusMessage.MessageType.TO_VERIFY_SMS_FRAGMENT));
                }else if (response.code() == 401)
                {
                    SharedPreferences preferences;
                    preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor =      preferences.edit();
                    editor.putString("login", "0");
                    editor.commit();
                    Constant.isReloaded = false;
                    if (!Constant.fbToken.isEmpty())
                    {
                        LoginManager.getInstance().logOut();
                    }
                    Intent intent = new Intent(getActivity(), AccountManageActivity.class);
                    startActivity(intent);

                    getActivity().finish();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    private void setPreference() {
        SharedPreferences preferences = getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
        preferences = getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("login", "1");
        editor.putString("id", String.valueOf(UserInfo.getInstance().getLoginDataModel().getId()));
        editor.putString("account_type", String.valueOf(UserInfo.getInstance().getLoginDataModel().getAccountType()));
        editor.putString("device_type", String.valueOf(UserInfo.getInstance().getLoginDataModel().getDeviceType()));
        editor.putString("points", String.valueOf(UserInfo.getInstance().getLoginDataModel().getPoints()));
        editor.putString("status", String.valueOf(UserInfo.getInstance().getLoginDataModel().getStatus()));
        editor.putString("user_type", String.valueOf(UserInfo.getInstance().getLoginDataModel().getUserType()));
        editor.putString("access_token", UserInfo.getInstance().getLoginDataModel().getAccessToken());
        editor.putString("email", UserInfo.getInstance().getLoginDataModel().getEmail());
        editor.putString("mobile", UserInfo.getInstance().getLoginDataModel().getMobile());
        editor.putString("name", UserInfo.getInstance().getLoginDataModel().getName());
        editor.putString("image", UserInfo.getInstance().getLoginDataModel().getUserImage());
        editor.commit();
    }

    @OnClick(R.id.MainLayout)
    public void onMainLayoutClicked() {
        hideKeyboard();
    }


    private void getTopRestaurant() {
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();
        String auth = "Bearer " + UserInfo.getInstance().getLoginDataModel().getAccessToken();

        RestClient.getInstance().initRestClient(RestClient.API_URL).getTopRestaurant(auth,new LimitationModel(3)).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject resultObj = response.body();
                progressDialog.dismiss();
                if (response.code() == 200) {
                    JsonArray restaurantJson = resultObj.getAsJsonArray("data");
                    Gson gson = new Gson();
                    Type type = new TypeToken<List<RestaurantModel>>() {
                    }.getType();
                    restaurantModels = gson.fromJson(restaurantJson, type);

                    UserInfo.getInstance().setTopRestModels(restaurantModels);
                    getAllRestaurant();
                    setListView();
                    getPointList();
                    //    EventBus.getDefault().post(new EventBusMessage(EventBusMessage.MessageType.TO_VERIFY_SMS_FRAGMENT));

                    // init Markers
                    initMaker();

                    // update Google Maps
                    updateMarkers();
                } else if (response.code() == 401)
                {
                    SharedPreferences preferences;
                    preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor =      preferences.edit();
                    editor.putString("login", "0");
                    editor.commit();
                    Constant.isReloaded = false;
                    if (!Constant.fbToken.isEmpty())
                    {
                        LoginManager.getInstance().logOut();
                    }
                    Intent intent = new Intent(getActivity(), AccountManageActivity.class);
                    startActivity(intent);

                    getActivity().finish();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    private void initMaker() {


        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.marker);

        if (!UserInfo.getInstance().getTopRestModels().isEmpty()) {
            markerOptions = new MarkerOptions().position(new LatLng(Double.parseDouble(UserInfo.getInstance().getTopRestModels().get(0).getLatitude()), Double.parseDouble(UserInfo.getInstance().getTopRestModels().get(0).getLongitude())))
                    .icon(icon);

            markerOptions2 = new MarkerOptions().position(new LatLng(Double.parseDouble(UserInfo.getInstance().getTopRestModels().get(1).getLatitude()), Double.parseDouble(UserInfo.getInstance().getTopRestModels().get(1).getLongitude())))
                    .icon(icon);

            markerOptions3 = new MarkerOptions().position(new LatLng(Double.parseDouble(UserInfo.getInstance().getTopRestModels().get(2).getLatitude()), Double.parseDouble(UserInfo.getInstance().getTopRestModels().get(2).getLongitude())))
                    .icon(icon);
        }


    }


    private void checkLocationPermissions() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(), permissions, MY_PERMISSIONS_REQUEST_LOCATION);
            }
        } else {
            // Permission has already been granted
            mapView.getMapAsync(this);
        }
    }


    private void setFragment(Fragment fragment) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Bundle sendData = new Bundle();
        fragmentTransaction.replace(R.id.MainFrame, fragment, null);
        fragmentTransaction.commit();
    }

    @OnClick(R.id.iv_search)
    public void onSearchClicked() {
        setFragment(new SearchRestaurantFragment());
    }

    private void setListView() {

        progressDialog.dismiss();
        restaurantAdapter = new RestaurantAdapter(getActivity(), UserInfo.getInstance().getTopRestModels());
        listView.setAdapter(restaurantAdapter);
        listView.setExpanded(true);
    }


    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            latLng = new LatLng(location.getLatitude(), location.getLatitude());

            //your code here
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    private void getAllCuisineList() {
        RestClient.getInstance().initRestClient(RestClient.API_URL).getAllCuisine().enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject resultObj = response.body();


                if (response.code() == 200) {
                    JsonArray restaurantJson = resultObj.getAsJsonObject("data").getAsJsonArray("rows");

                    Gson gson = new Gson();
                    Type type = new TypeToken<List<CuisineData>>() {
                    }.getType();
                    cuisineDataArrayList = gson.fromJson(restaurantJson, type);
                    UserInfo.getInstance().setCuisineDataArrayList(cuisineDataArrayList);

                    //     UserInfo.getInstance().setPointData(cuisineDataArrayList);
                    setRecyclerView();
                    //    EventBus.getDefault().post(new EventBusMessage(EventBusMessage.MessageType.TO_VERIFY_SMS_FRAGMENT));
                }else if (response.code() == 401)
                {
                    SharedPreferences preferences;
                    preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor =      preferences.edit();
                    editor.putString("login", "0");
                    editor.commit();
                    Constant.isReloaded = false;
                    if (!Constant.fbToken.isEmpty())
                    {
                        LoginManager.getInstance().logOut();
                    }
                    Intent intent = new Intent(getActivity(), AccountManageActivity.class);
                    startActivity(intent);

                    getActivity().finish();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    @OnClick(R.id.iv_globe)
    public void onGlobeClicked() {
        setFragment(new GlobeFragment());
    }

    private void setRecyclerView() {
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);

        cuisineAdapter = new CuisineAdapterMain(getActivity(), UserInfo.getInstance().getCuisineDataArrayList());
        recyclerView.setAdapter(cuisineAdapter);
    }

    // Update markers and show all markers in rect
    private void updateMarkers() {


        if (UserInfo.getInstance().getTopRestModels() != null && UserInfo.getInstance().getTopRestModels().size() != 0)
        {
            if (map != null) {
                // clear all markers
                map.clear();

                // add the top restaurants
                map.addMarker(markerOptions);
                map.addMarker(markerOptions2);
                map.addMarker(markerOptions3);
                map.getUiSettings().setMyLocationButtonEnabled(true);

                // Show all markers in rect
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(markerOptions.getPosition());
                builder.include(markerOptions2.getPosition());
                builder.include(markerOptions3.getPosition());

                final LatLngBounds bounds = builder.build();

//        int width = getResources().getDisplayMetrics().widthPixels;
//        int height = getResources().getDisplayMetrics().heightPixels;
                int padding = 100; // offset from edges of the map 10% of screen

                final CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {
//                        map.animateCamera(cu);
                    }
                });
            }
        }


    }

    private void getPointList() {
        String auth = "Bearer " + UserInfo.getInstance().getLoginDataModel().getAccessToken();

        final PointRequestModel pointRequestModel = new PointRequestModel(1, 10);
        RestClient.getInstance().initRestClient(RestClient.API_URL).getPointList(auth,pointRequestModel).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject resultObj = response.body();


                if (response.code() == 200) {
                    JsonArray restaurantJson = resultObj.getAsJsonObject("data").getAsJsonArray("rows");

                    Gson gson = new Gson();
                    Type type = new TypeToken<List<PointData>>() {
                    }.getType();
                    pointDataArrayList = gson.fromJson(restaurantJson, type);
                    UserInfo.getInstance().setPointData(pointDataArrayList);
                    //    EventBus.getDefault().post(new EventBusMessage(EventBusMessage.MessageType.TO_VERIFY_SMS_FRAGMENT));
                }else if (response.code() == 401)
                {
                    SharedPreferences preferences;
                    preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor =      preferences.edit();
                    editor.putString("login", "0");
                    editor.commit();
                    Constant.isReloaded = false;
                    if (!Constant.fbToken.isEmpty())
                    {
                        LoginManager.getInstance().logOut();
                    }
                    Intent intent = new Intent(getActivity(), AccountManageActivity.class);
                    startActivity(intent);

                    getActivity().finish();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(RIYAD_COOR_LATITUDE, RIYAD_COOR_LONGITUDE), 10));
    }

    public LatLng getCurrentLocation() {
        try {
            LocationManager locMgr = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            Criteria criteria = new Criteria();
            String locProvider = locMgr.getBestProvider(criteria, false);
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.

            }
            Location location = locMgr.getLastKnownLocation(locProvider);

            // getting GPS status
            boolean isGPSEnabled = locMgr.isProviderEnabled(LocationManager.GPS_PROVIDER);
            // getting network status
            boolean isNWEnabled = locMgr.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNWEnabled)
            {
                // no network provider is enabled
                return null;
            }
            else
            {
                // First get location from Network Provider
                if (isNWEnabled)
                    if (locMgr != null)
                        location = locMgr.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled)
                    if (location == null)
                        if (locMgr != null)
                            location = locMgr.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }

            return new LatLng(location.getLatitude(), location.getLongitude());
        }
        catch (NullPointerException ne)
        {
            Log.e("Current Location", "Current Lat Lng is Null");
            return new LatLng(0, 0);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return new LatLng(0, 0);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
        if(getView() == null){
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    // handle back button's click listener

                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

  
}
