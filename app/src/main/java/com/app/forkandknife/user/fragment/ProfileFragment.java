package com.app.forkandknife.user.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.provider.SyncStateContract;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.login.LoginManager;
import com.google.gson.JsonObject;
import com.app.forkandknife.AccountManageActivity;
import com.app.forkandknife.R;
import com.app.forkandknife.adapter.CustomSpinnerAdapter;
import com.app.forkandknife.adapter.LanguageAdapter;
import com.app.forkandknife.global.BaseFragment;
import com.app.forkandknife.global.Constant;
import com.app.forkandknife.model.EventBusMessage;
import com.app.forkandknife.model.UserInfo;
import com.app.forkandknife.restaurant.activity.LoginActivity;
import com.app.forkandknife.webservice.RestClient;

import org.greenrobot.eventbus.EventBus;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static butterknife.internal.Utils.arrayOf;

public class ProfileFragment extends BaseFragment {

    int SELECT_PICTURE = 1;

    @BindView(R.id.iv_image)
    CircleImageView profileImage;
    Uri selectedImageUri;
    String selectedImagePath;

    @BindView(R.id.spinner_language)
    Spinner spinnerLanguage;

    @BindView(R.id.editText_full_name)
    EditText editTextFullName;
    @BindView(R.id.editText_email)
    EditText editTextEmail;

    @BindView(R.id.editText_mobile)
    EditText editTextMobile;

    @BindView(R.id.editText_point)
    EditText editTextPoint;


    ArrayList<String> language =new ArrayList<>();
    LanguageAdapter languageAdapter;

    private RequestOptions requestOptions = new RequestOptions();
    int CAMERA_REQUEST = 101;
    ProgressDialog progressDialog ;

    SharedPreferences preferences;
    @SuppressLint("StringFormatInvalid")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View containerView=inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this,containerView);

        language.add("English");
        language.add("العربية");

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getActivity().getString(R.string.loading));

        requestOptions.placeholder(R.drawable.img_photo_sample);
        requestOptions.error(R.drawable.img_photo_sample);
        requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true);

        // Profile image
        if (UserInfo.getInstance().getLoginDataModel().getUserImage() != null && !UserInfo.getInstance().getLoginDataModel().getUserImage().isEmpty()){
            Glide.with(getActivity())
                    .setDefaultRequestOptions(requestOptions)
                    .load(RestClient.API_URL + "user/image/" + String.valueOf(UserInfo.getInstance().getLoginDataModel().getId()))
                    .into(profileImage);
        }


        editTextEmail.setText(UserInfo.getInstance().getLoginDataModel().getEmail());
        editTextFullName.setText(UserInfo.getInstance().getLoginDataModel().getName());
        editTextMobile.setText(UserInfo.getInstance().getLoginDataModel().getMobile());
        editTextPoint.setText(String.valueOf(UserInfo.getInstance().getLoginDataModel().getPoints()) + " " + getString(R.string.point) );
        languageAdapter =new LanguageAdapter(getActivity(),language);
        spinnerLanguage.setAdapter(languageAdapter);

        if (Constant.language == 0)
        {
            spinnerLanguage.setSelection(0);
        }else {
            spinnerLanguage.setSelection(1);

        }
        spinnerLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 1)
                {
                    Locale locale = new Locale("ar");
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getActivity().getBaseContext().getResources().updateConfiguration(config, getActivity().getBaseContext().getResources().getDisplayMetrics());
                    preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor =      preferences.edit();
                    editor.putString("Language", "ar");
                    editor.commit();
                    if (position != Constant.language)
                    {
                        setFragment(new ProfileFragment());
                        Constant.language = 1;
                        EventBus.getDefault().post(new EventBusMessage(EventBusMessage.MessageType.MODIFY_LOCALE));
                        Intent intent = getActivity().getIntent();
                        getActivity(). overridePendingTransition(0, 0);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        getActivity().finish();
                        getActivity().overridePendingTransition(0, 0);
                        startActivity(intent);
                        Constant.isReloaded = true;
                    }



                }else {
                    Locale locale = new Locale("en");
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getActivity().getBaseContext().getResources().updateConfiguration(config, getActivity().getBaseContext().getResources().getDisplayMetrics());
                    preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor =      preferences.edit();
                    editor.putString("Language", "en");
                    editor.commit();
                    if (position != Constant.language)
                    {
                        setFragment(new ProfileFragment());
                        Constant.language = 0;
                        EventBus.getDefault().post(new EventBusMessage(EventBusMessage.MessageType.MODIFY_LOCALE));
                        Intent intent = getActivity().getIntent();
                       getActivity(). overridePendingTransition(0, 0);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        getActivity().finish();
                        getActivity().overridePendingTransition(0, 0);
                        startActivity(intent);
                        Constant.isReloaded = true;
                    }
                }



            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return containerView;
    }

    @OnClick(R.id.MainLayout)
    public void onMainLayoutClicked(){
        hideKeyboard();
    }

    @OnClick(R.id.points_button)
    public void onPointClicked(){
        setFragment(new PointsFragment());
    }

    private void setFragment(Fragment fragment){
        FragmentManager fragmentManager= getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =   fragmentManager.beginTransaction();
        Bundle sendData = new Bundle();
        fragmentTransaction.replace(R.id.MainFrame,fragment,null);
        fragmentTransaction.commit();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    showDialog();
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(getActivity(), "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }

        // other 'case' lines to check for other
        // permissions this app might request
    }

    @OnClick(R.id.layout_signout)
    public void onSignoutClicked(){
        preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor =      preferences.edit();
        editor.putString("login", "0");
        editor.commit();
        Constant.isReloaded = false;
        if (!Constant.fbToken.isEmpty())
        {
            LoginManager.getInstance().logOut();
        }
        Intent intent = new Intent(getActivity(), AccountManageActivity.class);
        startActivity(intent);

        getActivity().finish();
    }


    @OnClick(R.id.iv_edit)
    public void onEditClicked(){
        setFragment(new EditProfileFragment());
    }

    @OnClick(R.id.layout_contact)
    public void onContactClicked(){
        setFragment(new ContactUsFragment());
    }

    @OnClick(R.id.iv_image)
    public void onProfileClicked(){
        // in onCreate or any event where your want the user to
        // select a file

        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.READ_CONTACTS)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), 1);
            }

        }else {

            showDialog();
        }
    }

    private void showDialog(){

        final Dialog dialog = new Dialog(getActivity());
        final View view  = getActivity().getLayoutInflater().inflate(R.layout.dialog_image_select, null);

        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(view);
        Window window = dialog.getWindow();

        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.BOTTOM;
        window.setAttributes(wlp);
        Button btnCamera = view.findViewById(R.id.btn_camera);
        Button btnGallery= view.findViewById(R.id.btn_gallery);
        Button btnCancel = view.findViewById(R.id.btn_cancel);

        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent cameraIntent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        });

        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, SELECT_PICTURE);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_PICTURE && resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                selectedImageUri = data.getData();

                selectedImagePath = getPath(selectedImageUri);
                File imgFile = new  File(selectedImagePath);


                if(imgFile.exists()){
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    profileImage.setImageBitmap(myBitmap);
                    uploadImage(imgFile);
                }
            }
        }
        else  if (requestCode == CAMERA_REQUEST ) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");


            // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
            Uri tempUri = getImageUri(getActivity().getApplicationContext(), photo);
            selectedImagePath = getPath(tempUri);
            File imgFile = new  File(selectedImagePath);


            if(imgFile.exists()){
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                profileImage.setImageBitmap(myBitmap);
                uploadImage(imgFile);
            }

            //     System.out.println(mImageCaptureUri);
        }
    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    private void uploadImage(File file)
    {
        progressDialog.show();
        progressDialog.setCancelable(false);
        String auth = "Bearer " + UserInfo.getInstance().getLoginDataModel().getAccessToken();

        final RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part filePart = MultipartBody.Part.createFormData("profile", file.getName(),requestBody);
        RestClient.getInstance().initRestClient(RestClient.API_URL).uploadImage(auth,UserInfo.getInstance().getLoginDataModel().getId(),filePart).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                progressDialog.dismiss();

                    if (response.code() == 200)
                    {
                        Toast.makeText(getActivity(),"Success",Toast.LENGTH_LONG).show();
                    }
                    else if (response.code() == 401)
                    {
                        SharedPreferences preferences;
                        preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor =      preferences.edit();
                        editor.putString("login", "0");
                        editor.commit();
                        Constant.isReloaded = false;
                        if (!Constant.fbToken.isEmpty())
                        {
                            LoginManager.getInstance().logOut();
                        }
                        Intent intent = new Intent(getActivity(), AccountManageActivity.class);
                        startActivity(intent);

                        getActivity().finish();
                    }


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getActivity(),t.getLocalizedMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public String getPath(Uri uri) {
        // just some safety built in
        if( uri == null ) {
            // TODO perform some logging or show user feedback
            return null;
        }
        // try to retrieve the image from the media store first
        // this will only work for images selected from gallery
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
        if( cursor != null ){
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            String path = cursor.getString(column_index);
            cursor.close();
            return path;
        }
        // this is our fallback here
        return uri.getPath();
    }

    @Override
    public void onResume() {
        super.onResume();

        if(getView() == null){
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    // handle back button's click listener

                    return true;
                }
                return false;
            }
        });
    }
}
