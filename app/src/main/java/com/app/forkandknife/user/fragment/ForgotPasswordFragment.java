package com.app.forkandknife.user.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.app.forkandknife.AccountManageActivity;
import com.app.forkandknife.global.Constant;
import com.facebook.login.LoginManager;
import com.google.gson.JsonObject;
import com.app.forkandknife.R;
import com.app.forkandknife.global.BaseFragment;
import com.app.forkandknife.model.EventBusMessage;
import com.app.forkandknife.model.ReservationData;
import com.app.forkandknife.model.UserInfo;
import com.app.forkandknife.webservice.RestClient;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ForgotPasswordFragment extends BaseFragment {

    @BindView(R.id.editText_mobile)
    EditText editTextMobile;

    @BindView(R.id.editText_new_password)
    EditText editTextNewPassword;

    @BindView(R.id.editText_confirm_password)
    EditText editTextConfirmPassword;
    ProgressDialog progressDialog ;

    boolean isMsgShown = false;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View containerView=inflater.inflate(R.layout.fragment_forgot_password, container, false);
        ButterKnife.bind(this,containerView);

        progressDialog = new ProgressDialog(getActivity());

        return  containerView;
    }

    @OnClick(R.id.btn_change)
    public void onChangeClicked(){
        if (isAllValid())
        {
            isMsgShown = false;
            HashMap <String,Object> map = new HashMap<>();
            map.put("mobile",editTextMobile.getText().toString().trim());
            map.put("password",editTextNewPassword.getText().toString().trim());
            RestClient.getInstance().initRestClient(RestClient.API_URL).changePassword(map).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    JsonObject resultObj = response.body();
                    if (response.code() == 200)
                    {
                        EventBus.getDefault().post(new EventBusMessage(EventBusMessage.MessageType.TO_LOGIN_FRAGMENT));
                    }else if (response.code() == 401)
                    {
                        SharedPreferences preferences;
                        preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor =      preferences.edit();
                        editor.putString("login", "0");
                        editor.commit();
                        Constant.isReloaded = false;
                        if (!Constant.fbToken.isEmpty())
                        {
                            LoginManager.getInstance().logOut();
                        }
                        Intent intent = new Intent(getActivity(), AccountManageActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    }

                    //   Toast.makeText(getActivity(),resultObj,Toast.LENGTH_LONG).show();
                    //    Toast.makeText(getActivity(),resultObj.get("message").toString(),Toast.LENGTH_LONG).show();




                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                }
            });
        }
    }

    @OnClick(R.id.MainLayout)
    public void onMainLayoutClicked(){
        hideKeyboard();
    }


    private boolean isAllValid(){
        if (!editTextMobile.getText().toString().trim().isEmpty() && !editTextNewPassword.getText().toString().trim().isEmpty() && !editTextConfirmPassword.getText().toString().trim().isEmpty())
        {
            if (editTextMobile.getText().toString().length()!= 12)
            {
                if (!isMsgShown)
                {
                    Toast.makeText(getActivity(),getString(R.string.enter_phone),Toast.LENGTH_LONG).show();
                    isMsgShown = true;
                }
                return false;
            }
            else if (!editTextConfirmPassword.getText().toString().trim().equals(editTextNewPassword.getText().toString().trim())){
                if (!isMsgShown)
                {
                    Toast.makeText(getActivity(),getString(R.string.not_match_password),Toast.LENGTH_LONG).show();
                    isMsgShown = true;
                }
                return  false;
            }
            else return true;
        }else {
            if (!isMsgShown)
            {
                Toast.makeText(getActivity(),getString(R.string.input_all),Toast.LENGTH_LONG).show();
                isMsgShown = true;
            }
            return false;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(getView() == null){
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    // handle back button's click listener
                    setFragment(new LoginFragment());

                    return true;
                }
                return false;
            }
        });
    }

    private void setFragment(Fragment fragment){
        FragmentManager fragmentManager= getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =   fragmentManager.beginTransaction();
        Bundle sendData = new Bundle();
        fragmentTransaction.replace(R.id.MainFrame,fragment,null);
        fragmentTransaction.commit();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
