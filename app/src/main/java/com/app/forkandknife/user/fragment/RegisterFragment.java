package com.app.forkandknife.user.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.Selection;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.app.forkandknife.AccountManageActivity;
import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.app.forkandknife.R;
import com.app.forkandknife.adapter.TimeAdapter;
import com.app.forkandknife.global.BaseFragment;
import com.app.forkandknife.global.Constant;
import com.app.forkandknife.global.GPSTracker;
import com.app.forkandknife.global.Utils;
import com.app.forkandknife.model.UserData;
import com.app.forkandknife.model.UserInfo;
import com.app.forkandknife.user.activity.TermsAndrConditionActivity;
import com.app.forkandknife.model.EventBusMessage;
import com.app.forkandknife.webservice.API;
import com.app.forkandknife.webservice.RestClient;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;
import static com.facebook.FacebookSdk.getFacebookDomain;


public class RegisterFragment extends BaseFragment {

    @BindView(R.id.tv_terms)
    TextView tvTerms;

    @BindView(R.id.editText_mobile)
    EditText editTextMobile;

    @BindView(R.id.editText_full_name)
    EditText editTextFullName;

    @BindView(R.id.editText_email)
    EditText editTextEmail;

    @BindView(R.id.editText_password)
    EditText editTextPassword;

    @BindView(R.id.editText_confirm_password)
    EditText editTextConfirmPassword;
    JSONObject request = new JSONObject();

    UserData userData = new UserData();
    boolean isMsgShown = false;



    String strConfirmPassword = "";
    GPSTracker gps;
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 1001;
    private static final int MY_PERMISSIONS_REQUEST_COARSE = 1002;
    private static final String TAG = "MapsActivity";
    private String[] permissions = new String[] {android.Manifest.permission.ACCESS_FINE_LOCATION};
    private String[] coarse_permissions = new String[] {Manifest.permission.ACCESS_COARSE_LOCATION};

    ProgressDialog progressDialog ;

    @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View containerView=inflater.inflate(R.layout.fragment_register, container, false);
        ButterKnife.bind(this,containerView);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.loading));

        editTextFullName.setText(UserInfo.getInstance().getName());
        editTextEmail.setText(UserInfo.getInstance().getEmail());

        setPrefix();
        setSpinnable();
        return  containerView;
    }

    @OnClick(R.id.MainLayout)
    public void onMainLayoutClicked(){
        hideKeyboard();
    }

    private void setSpinnable(){
        SpannableString ss = new SpannableString(getString(R.string.access_terms_condition));
        ClickableSpan clickableTerms = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                // show toast here
                Intent intent = new Intent(getActivity(), TermsAndrConditionActivity.class);
                startActivity(intent);
            }
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(true);
                ds.setColor(getActivity().getResources().getColor(R.color.color_underline));

            }
        };

        String termsText = getString(R.string.access_terms_condition);
        int startIdx = termsText.indexOf(getString(R.string.termandcondition));
        int endIdx = startIdx + getString(R.string.termandcondition).length();

        ss.setSpan(clickableTerms, startIdx, endIdx, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvTerms.setText(ss);
        tvTerms.setMovementMethod(LinkMovementMethod.getInstance());
        tvTerms.setHighlightColor(Color.BLACK);
    }

    public void setPrefix(){
        editTextMobile.setText("966");
        Selection.setSelection(editTextMobile.getText(), editTextMobile.getText().length());

        editTextMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().startsWith("966")){
                    editTextMobile.setText("966");
                    Selection.setSelection(editTextMobile.getText(), editTextMobile.getText().length());

                }

            }
        });
    }

    @OnClick(R.id.btn_submit)
    public void onSubmitClicked(){
        if (isAllValid())
        {
            UserInfo.getInstance().setEmail(editTextEmail.getText().toString().trim());
            UserInfo.getInstance().setName(editTextFullName.getText().toString().trim());
            UserInfo.getInstance().setMobile(editTextMobile.getText().toString().trim());
            UserInfo.getInstance().setPassword(editTextPassword.getText().toString().trim());
            checkLocationPermissions();
        }

    }

    private void checkLocationPermissions(){
        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(), permissions, MY_PERMISSIONS_REQUEST_LOCATION);
            }
        } else {
            // Permission has already been granted

        }
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    android.Manifest.permission.ACCESS_COARSE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(), coarse_permissions , MY_PERMISSIONS_REQUEST_COARSE);
            }
        } else {
            // Permission has already been granted
            gps = new GPSTracker(getApplicationContext(), getActivity());
            // Check if GPS enabled
            if (gps.canGetLocation()) {

                double latitude = gps.getLatitude();
                double longitude = gps.getLongitude();
                UserInfo.getInstance().setLatitude(String.valueOf(latitude));
                UserInfo.getInstance().setLongtitude(String.valueOf(longitude));
                final UserInfo userInfo = UserInfo.getInstance();
                try {
                    request.put(Constant.strMobile,UserInfo.getInstance().getMobile());
                    request.put(Constant.strUserType,1);
                    request.put(Constant.strDeviceType,2);
                    request.put(Constant.strName,UserInfo.getInstance().getName());
                    request.put(Constant.strPassword,UserInfo.getInstance().getPassword());
                    request.put(Constant.strEmail,UserInfo.getInstance().getEmail());
                    } catch (JSONException e) {
                    e.printStackTrace();
                }



                progressDialog.show();
                RestClient.getInstance().initRestClient(RestClient.API_BASE_URL).registerUser(UserInfo.getInstance()).enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        progressDialog.dismiss();
                        Log.d(TAG,response.toString());
                        JsonObject resultObj = response.body();

                        if (response.code() == 200){
                            JsonObject userJson = resultObj.getAsJsonObject("data");
                            Gson gson = new Gson();
                            Type type = new TypeToken<UserData>() {}.getType();
                            userData = gson.fromJson(userJson,type);
                            EventBus.getDefault().post(new EventBusMessage(EventBusMessage.MessageType.TO_VERIFY_SMS_FRAGMENT));
                        }
                        else if (response.code() == 401)
                        {
                            SharedPreferences preferences;
                            preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor =      preferences.edit();
                            editor.putString("login", "0");
                            editor.commit();
                            Constant.isReloaded = false;
                            if (!Constant.fbToken.isEmpty())
                            {
                                LoginManager.getInstance().logOut();
                            }
                            Intent intent = new Intent(getActivity(), AccountManageActivity.class);
                            startActivity(intent);

                            getActivity().finish();
                        }

                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(),getString(R.string.something_went_wrong),Toast.LENGTH_LONG).show();
                    }
                });
                // \n is for new line
            } else {
                // Can't get location.
                // GPS or network is not enabled.
                // Ask user to enable GPS/network in settings.
                gps.showSettingsAlert();
            }
        }
    }
    private boolean isValidPhone(){
        if (editTextMobile.getText().toString().trim().length() <12){
            return false;
        }else
        {
            return true;
        }
    }

    private boolean isPasswordMatch(){
        strConfirmPassword = editTextConfirmPassword.getText().toString().trim();
        if (strConfirmPassword.equals(editTextPassword.getText().toString().trim()))
        {
            return true;
        }
        else
            return false;
    }

    private boolean isAllValid(){
        if (isAllFilled() && isPasswordMatch() && isValidPhone() &&Utils.isEmailValid(editTextEmail.getText().toString().trim()))
        {
            return  true;
        }else if (!isAllFilled()){
            Toast.makeText(getActivity(),getString(R.string.input_all),Toast.LENGTH_LONG).show();
            isMsgShown  = true;
            return false;
        }else if (!Utils.isEmailValid(editTextEmail.getText().toString().trim()))
        {
            Toast.makeText(getActivity(),getString(R.string.ivalid_email),Toast.LENGTH_LONG).show();
            isMsgShown  = true;
            return false;
        }
        else if (!isValidPhone())
        {
            Toast.makeText(getActivity(),getString(R.string.enter_phone),Toast.LENGTH_LONG).show();
            isMsgShown  = true;
            return false;
        }else {

                return false;
        }
    }

    private boolean isAllFilled(){
        if (!editTextFullName.getText().toString().trim().isEmpty() && !editTextEmail.getText().toString().trim().isEmpty() && !editTextMobile.getText().toString().trim().isEmpty()
                && !editTextPassword.toString().trim().isEmpty() && !editTextConfirmPassword.getText().toString().trim().isEmpty())
        {
            return  true;
        }else return false;
    }

    @Override
    public void onResume() {
        super.onResume();

        if(getView() == null){
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    // handle back button's click listener
                    EventBus.getDefault().post(new EventBusMessage(EventBusMessage.MessageType.TO_LOGIN_FRAGMENT));

                    return true;
                }
                return false;
            }
        });
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
