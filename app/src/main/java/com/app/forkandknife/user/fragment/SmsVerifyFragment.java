package com.app.forkandknife.user.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.app.forkandknife.R;
import com.app.forkandknife.global.BaseFragment;
import com.app.forkandknife.global.Constant;
import com.app.forkandknife.model.EventBusMessage;
import com.app.forkandknife.model.LoginDataModel;
import com.app.forkandknife.model.SmsVerifyModel;
import com.app.forkandknife.user.activity.MainActivity;
import com.app.forkandknife.webservice.RestClient;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SmsVerifyFragment extends BaseFragment {

    @BindView(R.id.editText_sms_code)
    EditText editTextSMSCode;

    ProgressDialog progressDialog ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View containerView=inflater.inflate(R.layout.fragment_sms_verify, container, false);
        ButterKnife.bind(this,containerView);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();

        RestClient.getInstance().initRestClient(RestClient.API_SMS_URL).sendSms(Constant.loginDataMode.getId()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject resultObj = response.body();
                if (response.code() == 200)
                {
                    progressDialog.dismiss();
                }else if (response.code() == 401)
                {

                }


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
            }
        });

        return  containerView;
    }
    @OnClick(R.id.MainLayout)
    public void onMainLayoutClicked(){
        hideKeyboard();
    }

    @OnClick(R.id.btn_verify)
    public void onVerifyClicked(){
        progressDialog.setTitle(getString(R.string.loading));
        progressDialog.show();
        SmsVerifyModel smsVerifyModel = new SmsVerifyModel();
        if (editTextSMSCode.getText().toString().trim().length() ==4)
        {
            smsVerifyModel.setUserId(Constant.loginDataMode.getId());
            smsVerifyModel.setValue(editTextSMSCode.getText().toString().trim());
            RestClient.getInstance().initRestClient(RestClient.API_SMS_URL).verifySMS(smsVerifyModel).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    JsonObject resultObj = response.body();

                    if (resultObj.get("message").toString().trim().contains("success") && resultObj.has("data")){
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                }
            });
        }

    }
    @Override
    public void onResume() {
        super.onResume();

        if(getView() == null){
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    // handle back button's click listener
                    EventBus.getDefault().post(new EventBusMessage(EventBusMessage.MessageType.TO_LOGIN_FRAGMENT));

                    return true;
                }
                return false;
            }
        });
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
