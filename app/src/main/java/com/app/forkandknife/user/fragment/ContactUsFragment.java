package com.app.forkandknife.user.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.app.forkandknife.AccountManageActivity;
import com.app.forkandknife.global.Constant;
import com.facebook.login.LoginManager;
import com.google.gson.JsonObject;
import com.app.forkandknife.R;
import com.app.forkandknife.global.BaseFragment;
import com.app.forkandknife.model.UserInfo;
import com.app.forkandknife.webservice.RestClient;

import java.util.HashMap;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactUsFragment extends BaseFragment {

    @BindView(R.id.editText_title)
    EditText editTextTitie;

    @BindView(R.id.editText_description)
    EditText editTextDescription ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View containerView=inflater.inflate(R.layout.fragment_contact_us, container, false);
        ButterKnife.bind(this,containerView);

        return containerView;
    }

    @OnClick(R.id.MainLayout)
    public void onMainLayoutClicked(){
        hideKeyboard();
    }

    @OnClick(R.id.btn_submit)
    public void onSubmitClicked(){
        onContactSupport();
    }

    private void setFragment(Fragment fragment){
        FragmentManager fragmentManager= getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =   fragmentManager.beginTransaction();
        Bundle sendData = new Bundle();
        fragmentTransaction.replace(R.id.MainFrame,fragment,null);
        fragmentTransaction.commit();
    }

    @OnClick(R.id.iv_back)
    public void onBackClicked(){
        setFragment(new ProfileFragment());
    }

    private void onContactSupport(){
        String auth = "Bearer " + UserInfo.getInstance().getLoginDataModel().getAccessToken();

        HashMap<String,Object> map = new HashMap<>();
        map.put("title",editTextTitie.getText().toString().trim());
        map.put("description",editTextDescription.getText().toString().trim());
        map.put("userId", UserInfo.getInstance().getLoginDataModel().getId());
        RestClient.getInstance().initRestClient(RestClient.API_URL).contactSupport(auth,map).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 200)
                {
                    setFragment(new ProfileFragment());

                }else              if (response.code() == 401)
                {
                    SharedPreferences preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor =      preferences.edit();
                    editor.putString("login", "0");
                    editor.commit();
                    if (!Constant.fbToken.isEmpty())
                    {
                        LoginManager.getInstance().logOut();
                    }
                    Intent intent = new Intent(getActivity(), AccountManageActivity.class);
                    startActivity(intent);

                    getActivity().finish();
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();

        if(getView() == null){
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    // handle back button's click listener
                    setFragment(new ProfileFragment());

                    return true;
                }
                return false;
            }
        });
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
