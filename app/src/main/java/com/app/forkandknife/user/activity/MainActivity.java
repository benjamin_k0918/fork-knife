package com.app.forkandknife.user.activity;

import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.MapsInitializer;
import com.app.forkandknife.R;
import com.app.forkandknife.global.Constant;
import com.app.forkandknife.model.EventBusMessage;
import com.app.forkandknife.user.fragment.ForgotPasswordFragment;
import com.app.forkandknife.user.fragment.LoginFragment;
import com.app.forkandknife.user.fragment.MainFragment;
import com.app.forkandknife.user.fragment.ProfileFragment;
import com.app.forkandknife.user.fragment.RegisterFragment;
import com.app.forkandknife.user.fragment.ReservationFragment;
import com.app.forkandknife.user.fragment.SmsVerifyFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Locale;

import static com.facebook.FacebookSdk.getApplicationContext;

public class MainActivity extends AppCompatActivity {

    private TextView mTextMessage;
    BottomNavigationView navigation;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    setChildFragment(new MainFragment());
                    return true;
                case R.id.navigation_reservtion:
                    setChildFragment(new ReservationFragment());
                    return true;
                case R.id.navigation_notifications:
                    setChildFragment(new ProfileFragment());
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        EventBus.getDefault().register(this);
        MapsInitializer.initialize(getApplicationContext());

        mTextMessage = (TextView) findViewById(R.id.message);
        navigation = (BottomNavigationView) findViewById(R.id.bottom_navigation_bar);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        if (Constant.isReloaded)
        {
            navigation.setSelectedItemId(R.id.navigation_notifications);
        }
        else {
            navigation.setSelectedItemId(R.id.navigation_home);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void Event(EventBusMessage messageEvent) throws Exception {
        if (messageEvent != null) {
            int messageType = messageEvent.getMessageType();
            if (messageType == EventBusMessage.MessageType.MODIFY_LOCALE) {
                if (Constant.language == 1)
                {
                    navigation.getMenu().getItem(0).setTitle(getString(R.string.title_home_ar));
                    navigation.getMenu().getItem(1).setTitle(getString(R.string.reservation_ar));
                    navigation.getMenu().getItem(2).setTitle(getString(R.string.profile_ar));
                }else {
                    navigation.getMenu().getItem(0).setTitle(getString(R.string.title_home_en));
                    navigation.getMenu().getItem(1).setTitle(getString(R.string.reservation_en));
                    navigation.getMenu().getItem(2).setTitle(getString(R.string.profile_en));
                }
            }
        }
    }


    private void setChildFragment(Fragment fragment){
        FragmentTransaction fragmentTranaction= getSupportFragmentManager().beginTransaction();
        fragmentTranaction.replace(R.id.MainFrame, fragment, null);
        fragmentTranaction.commit();
    }

    @Override
    protected void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
