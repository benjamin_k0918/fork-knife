package com.app.forkandknife.user.fragment;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TimePicker;

import com.app.forkandknife.R;
import com.app.forkandknife.adapter.CuisineAdapter;
import com.app.forkandknife.global.BaseFragment;
import com.app.forkandknife.global.Utils;
import com.app.forkandknife.model.CuisineData;
import com.app.forkandknife.model.UserInfo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchRestaurantFragment extends BaseFragment {
    ArrayList<CuisineData> cuisineDataArrayList;
    LinearLayoutManager layoutManager;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    CuisineAdapter cuisineAdapter;

    @BindView(R.id.editText_time)
    EditText editTextTime;

    @BindView(R.id.editText_search)
    EditText editText_search;

    MainFragment mainFragment = new MainFragment();
    GlobeFragment globeFragment = new GlobeFragment();

    WindowManager.LayoutParams  layoutParams;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View containerView=inflater.inflate(R.layout.fragment_search_restaurant, container, false);
        ButterKnife.bind(this,containerView);

        layoutParams = new WindowManager.LayoutParams();
        layoutParams.gravity = Gravity.BOTTOM;

        setRecyclerView();

        return  containerView;
    }

    @OnClick(R.id.iv_globe)
    public void onGlobeClicked(){
        setFragment(globeFragment);
    }

    private String formatNumber(int num){
        String str = String.format("%02d", num);
        return  str;
    }

    @OnClick(R.id.MainLayout)
    public void onMainLayoutClicked(){
        hideKeyboard();
    }

    @OnClick(R.id.iv_select_time)
    public void onSelectTimeClicked(){
        // TODO Auto-generated method stub

        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                editTextTime.setText(Utils.formatNumber(selectedHour) + ":" + Utils.formatNumber(selectedMinute));
            }
        }, hour, minute,  DateFormat.is24HourFormat(getActivity()));//Yes 24 hour time
        mTimePicker.getWindow().setAttributes(layoutParams);

        mTimePicker.show();
    }

    @OnClick(R.id.editText_location)
    public void OnLocationClicked(){
        String uri = String.format(Locale.ENGLISH, "geo:%f,%f", 24.7136, 46.6753);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        getActivity().startActivity(intent);
    }



    private void setFragment(Fragment fragment){
        FragmentManager fragmentManager= getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =   fragmentManager.beginTransaction();
        Bundle sendData = new Bundle();
        fragmentTransaction.replace(R.id.MainFrame,fragment,null);
        fragmentTransaction.commit();
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void setRecyclerView(){
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

        recyclerView.setLayoutManager(layoutManager);

        cuisineAdapter = new CuisineAdapter(getActivity(), UserInfo.getInstance().getCuisineDataArrayList());
        recyclerView.setAdapter(cuisineAdapter);
    }


    @OnClick(R.id.btn_search)
    public void onSearchClicked(){
        SearchResultFragment searchResultFragment = new SearchResultFragment();
        FragmentManager fragmentManager= getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =   fragmentManager.beginTransaction();
        Bundle sendData = new Bundle();
        sendData.putBoolean("searchWithCuisine",false);
        sendData.putString("search",editText_search.getText().toString().trim());
        sendData.putString("time",editTextTime.getText().toString().trim());

        ArrayList<Integer> cuisineArray = new ArrayList<Integer>();
        for (int i = 0;i<UserInfo.getInstance().getCuisineDataArrayList().size();i++)
        {
            if (UserInfo.getInstance().getCuisineDataArrayList().get(i).getSelection())
            {
                cuisineArray.add(UserInfo.getInstance().getCuisineDataArrayList().get(i).getId());
            }
        }

        int[] cuisines =new int[cuisineArray.size()];
        if (!cuisineArray.isEmpty())
        {
            for (int i = 0;i<cuisineArray.size();i++)
            {
                cuisines[i] = cuisineArray.get(i);
            }
        }
        sendData.putIntArray("cuisine",cuisines);

        searchResultFragment.setArguments(sendData);

        fragmentTransaction.replace(R.id.MainFrame,searchResultFragment,null);
        fragmentTransaction.commit();
    }

    @OnClick(R.id.iv_back)
    public void onBackClicked(){
        setFragment(new MainFragment());
    }

    @Override
    public void onResume() {
        super.onResume();

        if(getView() == null){
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    // handle back button's click listener

                    setFragment(new MainFragment());

                    return true;
                }
                return false;
            }
        });
    }

}
