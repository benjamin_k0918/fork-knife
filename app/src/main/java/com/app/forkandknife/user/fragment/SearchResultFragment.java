package com.app.forkandknife.user.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.app.forkandknife.AccountManageActivity;
import com.app.forkandknife.global.Constant;
import com.app.forkandknife.model.UserData;
import com.app.forkandknife.model.UserInfo;
import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.app.forkandknife.R;
import com.app.forkandknife.adapter.RestaurantSearchResultAdapter;
import com.app.forkandknife.global.BaseFragment;
import com.app.forkandknife.model.RestaurantModel;
import com.app.forkandknife.webservice.RestClient;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchResultFragment extends BaseFragment {

    @BindView(R.id.listView)
    ListView listView;

    @BindView(R.id.editText_search_key)
    EditText editTextSearchKey;

    RestaurantSearchResultAdapter restaurantAdapter;
    ArrayList<RestaurantModel> restaurantDataArrayList;
    ArrayList<RestaurantModel> currentArrayList;

    public static int formerFragment = 0;
    SharedPreferences preferences;

    ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View containerView=inflater.inflate(R.layout.fragment_search_result, container, false);
        ButterKnife.bind(this,containerView);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.loading));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                UserInfo.getInstance().setCurrentRestaurant(restaurantDataArrayList.get(position));

                setFragment(new BookingRestaurantFragment());
            }
        });

        editTextSearchKey.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String key = editTextSearchKey.getText().toString().trim();
                if (!restaurantDataArrayList.isEmpty())
                {
                    currentArrayList = new ArrayList<>();
                    for (int  i = 0;i<restaurantDataArrayList.size();i++)
                    {
                        if (restaurantDataArrayList.get(i).getName().contains(key)){
                            currentArrayList.add(restaurantDataArrayList.get(i));
                        }
                    }
                    restaurantAdapter = new RestaurantSearchResultAdapter(getActivity(),currentArrayList);
                    listView.setAdapter(restaurantAdapter);
                }

            }
        });
        boolean searchByCuisine = getArguments().getBoolean("searchWithCuisine");
        if (searchByCuisine)
        {
            searchByCuisne();
        }else {
            searchRestaurant();
        }
        return containerView;
    }

    @OnClick(R.id.MainLayout)
    public void onMainLayoutClicked(){
        hideKeyboard();
    }

    private void searchRestaurant(){
        String auth = "Bearer " + UserInfo.getInstance().getLoginDataModel().getAccessToken();

        HashMap map = new HashMap();
        String search = getArguments().getString("search");
        String time = getArguments().getString("time");
        int []cuisineArray = getArguments().getIntArray("cuisine");
        map.put("search",search);
        map.put("time",time);
        map.put("cuisine",cuisineArray);
        map.put("pageNumber",1);
        map.put("pageSize",100);
    //    map.put("token",UserInfo.getInstance().getLoginDataModel().getAccessToken());
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();
        RestClient.getInstance().initRestClient(RestClient.API_URL).searchRestaurant(auth,map).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject resultObj = response.body();

                if (response.code() == 200) {
                    JsonArray restaurantJson = resultObj.getAsJsonObject("data").getAsJsonArray("rows");
                    Gson gson = new Gson();
                    Type type = new TypeToken<List<RestaurantModel>>() {
                    }.getType();
                    restaurantDataArrayList = gson.fromJson(restaurantJson, type);
                    setListView();
                    //   UserInfo.getInstance().setTopRestModels(restaurantModels);

                }else if (response.code() == 401)
                {
                    SharedPreferences preferences;
                    preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor =      preferences.edit();
                    editor.putString("login", "0");
                    editor.commit();
                    Constant.isReloaded = false;
                    if (!Constant.fbToken.isEmpty())
                    {
                        LoginManager.getInstance().logOut();
                    }
                    Intent intent = new Intent(getActivity(), AccountManageActivity.class);
                    startActivity(intent);

                    getActivity().finish();
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    private void searchByCuisne(){
        int cuisine = getArguments().getInt("id");
        int []cuisineArary = {cuisine};
        String auth = "Bearer " + UserInfo.getInstance().getLoginDataModel().getAccessToken();

        HashMap <String,Object> map  = new HashMap<>();
        map.put("search","");
        map.put("id",cuisineArary);
        map.put("pageNumber",1);
        map.put("pageSize",10);
    //    map.put("token",UserInfo.getInstance().getLoginDataModel().getAccessToken());
        progressDialog.show();
        RestClient.getInstance().initRestClient(RestClient.API_URL).searchRestaurant(auth,map).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject resultObj = response.body();

                if (response.code() == 200) {
                    JsonArray restaurantJson = resultObj.getAsJsonObject("data").getAsJsonArray("rows");
                    Gson gson = new Gson();
                    Type type = new TypeToken<List<RestaurantModel>>() {
                    }.getType();
                    restaurantDataArrayList = gson.fromJson(restaurantJson, type);
                    setListView();
                 //   UserInfo.getInstance().setTopRestModels(restaurantModels);

                } else if (response.code() == 401)
                {
                    SharedPreferences preferences;
                    preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor =      preferences.edit();
                    editor.putString("login", "0");
                    editor.commit();
                    Constant.isReloaded = false;
                    if (!Constant.fbToken.isEmpty())
                    {
                        LoginManager.getInstance().logOut();
                    }
                    Intent intent = new Intent(getActivity(), AccountManageActivity.class);
                    startActivity(intent);

                    getActivity().finish();
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }


    private void setListView(){


        if (progressDialog.isShowing())
        {
            progressDialog.dismiss();
        }
        restaurantAdapter = new RestaurantSearchResultAdapter(getActivity(),restaurantDataArrayList);
        listView.setAdapter(restaurantAdapter);
    }

    @OnClick(R.id.iv_back)
    public void onBackClicked(){
        if (formerFragment == 1)
        {
            setFragment(new MainFragment());
        }
        else {
            setFragment(new SearchRestaurantFragment());

        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if(getView() == null){
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    // handle back button's click listener
                    if (formerFragment == 1)
                    {
                        setFragment(new MainFragment());
                        formerFragment =0;
                    }
                    else {
                        setFragment(new SearchRestaurantFragment());
                    }

                    return true;
                }
                return false;
            }
        });
    }


    private void setFragment(Fragment fragment){
        FragmentManager fragmentManager= getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =   fragmentManager.beginTransaction();
        Bundle sendData = new Bundle();
        fragmentTransaction.replace(R.id.MainFrame,fragment,null);
        fragmentTransaction.commit();
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
