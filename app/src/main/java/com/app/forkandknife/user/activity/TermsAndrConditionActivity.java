package com.app.forkandknife.user.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.app.forkandknife.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TermsAndrConditionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_andr_condition);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.iv_back)
    public void onIvBackClicked(){
        finish();
    }

    @OnClick(R.id.btn_back)
    public void onBtnBackClicked(){
        finish();
    }
}
