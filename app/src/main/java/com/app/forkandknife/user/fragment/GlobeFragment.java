package com.app.forkandknife.user.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.forkandknife.AccountManageActivity;
import com.app.forkandknife.global.Constant;
import com.app.forkandknife.webservice.RestClient;
import com.facebook.login.LoginManager;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.app.forkandknife.R;
import com.app.forkandknife.adapter.RestaurantRecyclerAdapter;
import com.app.forkandknife.global.BaseFragment;
import com.app.forkandknife.model.RestaurantModel;
import com.app.forkandknife.model.UserInfo;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;
import static com.facebook.FacebookSdk.getApplicationId;


public class GlobeFragment extends BaseFragment implements OnMapReadyCallback {

    MapView mapView;
    GoogleMap map;

    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 1001;
    private static final String TAG = "MapsActivity";
    private String[] permissions = new String[] {android.Manifest.permission.ACCESS_FINE_LOCATION};

    LinearLayoutManager layoutManager;
    ArrayList<RestaurantModel> restaurantDataArrayList;

    RestaurantRecyclerAdapter restaurantRecyclerAdapter;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.iv_search)
    ImageView ivSearch;

    ProgressDialog progressDialog;

    MarkerOptions markerOptions;
    MarkerOptions makerOption2;
    MarkerOptions makerOption3;
    MarkerOptions currentMakerOptions;
    ArrayList currentRestModel = new ArrayList();

    @BindView(R.id.tv_empty)
    TextView tvEmpty;

    ArrayList<MarkerOptions> markerOptionsArrayList = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        MapsInitializer.initialize(getApplicationContext());
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View containerView=inflater.inflate(R.layout.fragment_globe, container, false);
        ButterKnife.bind(this,containerView);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getActivity().getResources().getString(R.string.loading));

        MapsInitializer.initialize(getApplicationContext());

        setRecyclerView();

        //set mapview
        mapView = (MapView) containerView.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);

        checkLocationPermissions();

        return containerView;
    }

    private void initMakers(){
        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.marker);

        // 24.7716034,46.6720063
        // 24.6965175,46.6920433
        // 24.6993022,46.6902476
        // 24.6989714,46.689688

        for (int i = 0;i<UserInfo.getInstance().getAllRestModel().size();i++)
        {
            markerOptionsArrayList.add(new MarkerOptions().position(new LatLng(Double.parseDouble(UserInfo.getInstance().getAllRestModel().get(i).getLatitude()),Double.parseDouble( UserInfo.getInstance().getAllRestModel().get(i).getLongitude())))
                    .title(UserInfo.getInstance().getAllRestModel().get(i).getName())
                    .snippet("")
                    .icon(icon));
        }
    }

    @OnClick(R.id.iv_search)
    public void onSearchClicked(){
        setFragment(new SearchRestaurantFragment());
    }



    private void checkLocationPermissions(){
        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(), permissions, MY_PERMISSIONS_REQUEST_LOCATION);
            }
        } else {
            // Permission has already been granted
            mapView.getMapAsync(this);
        }
    }

    private void setRecyclerView(){
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private float getRadius(LatLng northwest, LatLng southeast){

        Location loc1 =new Location("");
        Location loc2 = new Location("");
        loc1.setLongitude(northwest.longitude);
        loc1.setLatitude(northwest.latitude);

        loc2.setLatitude(southeast.latitude);
        loc2.setLongitude(southeast.longitude);
        float disinKm = loc1.distanceTo(loc2) / 2000;

        return disinKm;
    }

    private float getDistance(RestaurantModel restaurantModel,LatLng target)
    {
        Location loc1 =new Location("");
        Location loc2 = new Location("");
        loc1.setLongitude(Double.parseDouble(restaurantModel.getLongitude()));
        loc1.setLatitude(Double.parseDouble(restaurantModel.getLatitude()));
        loc2.setLongitude(target.longitude);
        loc2.setLatitude(target.latitude);

        float disinKm = loc1.distanceTo(loc2) / 1000;
        return disinKm;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        map.getUiSettings().setMyLocationButtonEnabled(false);

        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            map.setMyLocationEnabled(true);
        }


        currentMakerOptions = new MarkerOptions().position(new LatLng(Double.parseDouble(UserInfo.getInstance().getLatitude()),Double.parseDouble(UserInfo.getInstance().getLongtitude())))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.my_location));
        map.addMarker(currentMakerOptions);
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(UserInfo.getInstance().getLatitude()),Double.parseDouble(UserInfo.getInstance().getLongtitude())), 12));


        LatLngBounds bounds = map.getProjection().getVisibleRegion().latLngBounds;
        LatLng northeast = bounds.northeast;
        LatLng southwest = bounds.southwest;
        float radius = getRadius(northeast,southwest);
        for (int i = 0;i<UserInfo.getInstance().getAllRestModel().size();i++)
        {
            if (getDistance(UserInfo.getInstance().getAllRestModel().get(i),new LatLng(Double.parseDouble(UserInfo.getInstance().getLatitude()),Double.parseDouble(UserInfo.getInstance().getLongtitude()))) < radius/12)
            {
                currentRestModel.add(UserInfo.getInstance().getAllRestModel().get(i));
            }
        }

        restaurantRecyclerAdapter = new RestaurantRecyclerAdapter(getActivity(),currentRestModel);
        recyclerView.setAdapter(restaurantRecyclerAdapter);
        if (currentRestModel.isEmpty())
        {
            recyclerView.setVisibility(View.GONE);
            tvEmpty.setVisibility(View.VISIBLE);
        }else {
            recyclerView.setVisibility(View.VISIBLE);
            tvEmpty.setVisibility(View.GONE);
        }
        initMakers();
        for (int i = 0;i<UserInfo.getInstance().getAllRestModel().size();i++)
        {
            map.addMarker(markerOptionsArrayList.get(i));
        }
    }


    @OnClick(R.id.iv_reload)
    public void onReloadClicked(){
        if (map!= null)
        {
            currentRestModel = new ArrayList();

            HashMap<String,Object> newMap  = new HashMap<>();
            LatLngBounds bounds = map.getProjection().getVisibleRegion().latLngBounds;
            LatLng northeast = bounds.northeast;
            LatLng southwest = bounds.southwest;
            LatLng centerPos  = new LatLng((northeast.latitude + southwest.latitude)/2,(northeast.longitude + southwest.longitude)/2);
            String auth = "Bearer " + UserInfo.getInstance().getLoginDataModel().getAccessToken();

            newMap.put("latitude",centerPos.latitude);
            newMap.put("longitude",centerPos.longitude);
            float radius = getRadius(northeast,southwest);
            newMap.put("distance",radius);
            newMap.put("pageNumber",1);
            newMap.put("pageSize",20);

            RestClient.getInstance().initRestClient(RestClient.API_URL).searchByLocation(auth,newMap).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    JsonObject resultObj = response.body();
                    progressDialog.dismiss();
                    if (response.code() == 200) {
                        JsonArray restaurantJson = resultObj.getAsJsonObject("data").getAsJsonArray("rows");
                        Gson gson = new Gson();
                        Type type = new TypeToken<List<RestaurantModel>>() {
                        }.getType();
                        currentRestModel = gson.fromJson(restaurantJson, type);

                        restaurantRecyclerAdapter = new RestaurantRecyclerAdapter(getActivity(),currentRestModel);
                        recyclerView.setAdapter(restaurantRecyclerAdapter);
                        if (currentRestModel.isEmpty())
                        {
                            recyclerView.setVisibility(View.GONE);
                            tvEmpty.setVisibility(View.VISIBLE);
                        }else {
                            recyclerView.setVisibility(View.VISIBLE);
                            tvEmpty.setVisibility(View.GONE);
                        }

                    } else if (response.code() == 401)
                    {
                        SharedPreferences preferences;
                        preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor =      preferences.edit();
                        editor.putString("login", "0");
                        editor.commit();
                        Constant.isReloaded = false;
                        if (!Constant.fbToken.isEmpty())
                        {
                            LoginManager.getInstance().logOut();
                        }
                        Intent intent = new Intent(getActivity(), AccountManageActivity.class);
                        startActivity(intent);

                        getActivity().finish();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });
          //  map.p
/*            int count = 0;
            LatLngBounds bounds = map.getProjection().getVisibleRegion().latLngBounds;
            LatLng northeast = bounds.northeast;
            LatLng southwest = bounds.southwest;
            float radius = getRadius(northeast,southwest);
            for (int i = 0;i<UserInfo.getInstance().getAllRestModel().size();i++)
            {
                if (getDistance(UserInfo.getInstance().getAllRestModel().get(i),map.getCameraPosition().target) < radius)
                {
                    currentRestModel.add(UserInfo.getInstance().getAllRestModel().get(i));
                }
            }
            restaurantRecyclerAdapter = new RestaurantRecyclerAdapter(getActivity(),currentRestModel);
            recyclerView.setAdapter(restaurantRecyclerAdapter);
            if (currentRestModel.isEmpty())
            {
                recyclerView.setVisibility(View.GONE);
                tvEmpty.setVisibility(View.VISIBLE);
            }else {
                recyclerView.setVisibility(View.VISIBLE);
                tvEmpty.setVisibility(View.GONE);
            }*/
        }


    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
        if(getView() == null){
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    // handle back button's click listener
                    setFragment(new MainFragment());

                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }


    private void setFragment(Fragment fragment){
        FragmentManager fragmentManager= getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =   fragmentManager.beginTransaction();
        Bundle sendData = new Bundle();
        fragmentTransaction.replace(R.id.MainFrame,fragment,null);
        fragmentTransaction.commit();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }


    @OnClick(R.id.iv_back)
    public void onBack(){
        setFragment(new MainFragment());
    }

}
