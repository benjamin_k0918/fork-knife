package com.app.forkandknife.user.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.forkandknife.AccountManageActivity;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.app.forkandknife.R;
import com.app.forkandknife.adapter.ReservationAdapter;
import com.app.forkandknife.global.BaseFragment;
import com.app.forkandknife.global.Constant;
import com.app.forkandknife.model.ReservationData;
import com.app.forkandknife.model.RestaurantModel;
import com.app.forkandknife.model.RestaurantReservationModel;
import com.app.forkandknife.model.UserInfo;
import com.app.forkandknife.webservice.RestClient;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;

public class ReservationFragment extends BaseFragment {

    @BindView(R.id.listView)
    SwipeMenuListView listView;

    ReservationAdapter reservationAdapter;
    ArrayList<ReservationData> reservationDataArrayList;
    ArrayList<ReservationData> currentArrayList = new ArrayList<>();

    @BindView(R.id.editText_search_key)
    EditText editTextSearchKey;

    @BindView(R.id.refresh_layout)
    SwipyRefreshLayout swipeRefreshLayout;


    int pageNumber = 1;
    ProgressDialog progressDialog ;
    SharedPreferences preferences;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View containerView=inflater.inflate(R.layout.fragment_reservation, container, false);
        ButterKnife.bind(this,containerView);

        progressDialog = new ProgressDialog(getActivity());
        if (UserInfo.getInstance().getAllRestModel().isEmpty())
        {
            getAllRestaurant();
        }
        getReservationList();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (Constant.isFirstTimeToOpenReservation)
                {
                    UserInfo.getInstance().setReservationData(reservationDataArrayList.get(position));
                }
                else {
                    UserInfo.getInstance().setReservationData(currentArrayList.get(position));
                }
                setFragment(new ReservationDetailFragment());
            }
        });

        editTextSearchKey.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String currentString =  s.toString().trim();

                currentArrayList = new ArrayList<>();

                    for (int i = 0;i<reservationDataArrayList.size();i++)
                        if (reservationDataArrayList.get(i).getRestaurant().getName().toLowerCase().contains(currentString.toLowerCase())){
                            currentArrayList.add(reservationDataArrayList.get(i));
                        }

                    reservationAdapter = new ReservationAdapter(getActivity(),currentArrayList);
                    listView.setAdapter(reservationAdapter);


            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
                    if (currentArrayList.size() % 20 == 0)
                    {
                        pageNumber+= 1;
                        getReservationList();
                    }else {
                        getReservationList();
                    }
                    swipeRefreshLayout.setRefreshing(false);
                }


            }
        });
        setCreator();
        return containerView;
    }

    @OnClick(R.id.MainLayout)
    public void onMainLayoutClicked(){
        hideKeyboard();
    }

    private void setCreator(){
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(0xffcd0606));

                // set item width
                deleteItem.setWidth(200);
                // set a icon
                deleteItem.setTitleColor(getActivity().getResources().getColor(R.color.color_white));
                deleteItem.setTitle(getString(R.string.delete));

                deleteItem.setTitleSize(17);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };

        listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {

                    case 0:
                        // delete

                        hideReservation(position);
                        break;
                }
                // false : close the menu; true : not close the menu
                return false;
            }
        });

// set creator
        listView.setMenuCreator(creator);

        // left
        if (Constant.language ==1)
        {
            listView.setSwipeDirection(SwipeMenuListView.DIRECTION_RIGHT);

        }else {
            listView.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);

        }
    }

    private void hideReservation(int position){
        String auth = "Bearer " + UserInfo.getInstance().getLoginDataModel().getAccessToken();

        HashMap<String,Object> map = new HashMap<>();
        map.put("userId",UserInfo.getInstance().getLoginDataModel().getId());
        map.put("reservationId",currentArrayList.get(position).getId());
    //    map.put("token",UserInfo.getInstance().getLoginDataModel().getAccessToken());
        RestClient.getInstance().initRestClient(RestClient.API_URL).removeReservation(auth,map).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 200)
                {
                    getReservationList();
                }else {

                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    private void getAllRestaurant(){
        String auth = "Bearer " + UserInfo.getInstance().getLoginDataModel().getAccessToken();

        HashMap <String,Object> map = new HashMap<>();
        map.put("pageNumber",1);
        map.put("pageSize",100);
        RestClient.getInstance().initRestClient(RestClient.API_URL).getAllRestaurtant(auth,map).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                ArrayList<RestaurantModel> restaurantModels = new ArrayList<>();
                JsonObject resultObj = response.body();

                if (response.code() == 200){
                    JsonArray restaurantJson = resultObj.getAsJsonObject("data").getAsJsonArray("rows");
                    Gson gson = new Gson();
                    Type type = new TypeToken<List<RestaurantModel>>() {}.getType();
                    restaurantModels = gson.fromJson(restaurantJson,type);

                    UserInfo.getInstance().setAllRestModel(restaurantModels);
                    //    EventBus.getDefault().post(new EventBusMessage(EventBusMessage.MessageType.TO_VERIFY_SMS_FRAGMENT));
                }else if (response.code() == 401)
                {
                    SharedPreferences preferences;
                    preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor =      preferences.edit();
                    editor.putString("login", "0");
                    editor.commit();
                    Constant.isReloaded = false;
                    if (!Constant.fbToken.isEmpty())
                    {
                        LoginManager.getInstance().logOut();
                    }
                    Intent intent = new Intent(getActivity(), AccountManageActivity.class);
                    startActivity(intent);

                    getActivity().finish();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    private void setListView(){
        if (progressDialog.isShowing())
        {
            progressDialog.dismiss();
        }
        reservationAdapter = new ReservationAdapter(getActivity(),currentArrayList);
        listView.setAdapter(reservationAdapter);
    }

    private void getReservationList(){
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();
        String auth = "Bearer " + UserInfo.getInstance().getLoginDataModel().getAccessToken();

        HashMap<String,Object> map = new HashMap<>();
        map.put("userId", UserInfo.getInstance().getLoginDataModel().getId());
        map.put("pageNumber",pageNumber);
        map.put("pageSize",20);
    //    map.put("token",UserInfo.getInstance().getLoginDataModel().getAccessToken());

        RestClient.getInstance().initRestClient(RestClient.API_URL).searchAllReservations(auth,map).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject resultObj = response.body();

                if (response.code() == 200) {
                    JsonArray restaurantJson = resultObj.getAsJsonObject("data").getAsJsonArray("rows");
                    Gson gson = new Gson();
                    Type type = new TypeToken<List<ReservationData>>() {
                    }.getType();
                    reservationDataArrayList = new ArrayList<>();
                    reservationDataArrayList = gson.fromJson(restaurantJson, type);
                    Constant.isFirstTimeToOpenReservation = false;

                    reservationDataArrayList = gson.fromJson(restaurantJson, type);
                    if (currentArrayList.size() %20 == 0)
                    {
                        currentArrayList.addAll(reservationDataArrayList);
                    }else {
                        if (currentArrayList.size() / 20 == 0)
                        {
                            currentArrayList = new ArrayList<>();
                            currentArrayList.addAll(reservationDataArrayList);
                        }else {
                            int size = currentArrayList.size()/20;
                            currentArrayList = new ArrayList<ReservationData>( currentArrayList.subList(0,20*size-1));
                            currentArrayList.addAll(reservationDataArrayList);
                        }


                    }

                    currentArrayList = reservationDataArrayList;
                    setListView();
                    //   UserInfo.getInstance().setTopRestModels(restaurantModels);

                }else if (response.code() == 401)
                {
                    SharedPreferences preferences;
                    preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor =      preferences.edit();
                    editor.putString("login", "0");
                    editor.commit();
                    Constant.isReloaded = false;
                    if (!Constant.fbToken.isEmpty())
                    {
                        LoginManager.getInstance().logOut();
                    }
                    Intent intent = new Intent(getActivity(), AccountManageActivity.class);
                    startActivity(intent);

                    getActivity().finish();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    private void setFragment(Fragment fragment){
        FragmentManager fragmentManager= getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =   fragmentManager.beginTransaction();
        Bundle sendData = new Bundle();
        fragmentTransaction.replace(R.id.MainFrame,fragment,null);
        fragmentTransaction.commit();
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onResume() {
        super.onResume();

        if(getView() == null){
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    // handle back button's click listener

                    return true;
                }
                return false;
            }
        });
    }
}
