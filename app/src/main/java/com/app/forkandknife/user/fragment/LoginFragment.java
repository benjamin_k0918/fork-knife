package com.app.forkandknife.user.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.share.internal.LegacyNativeDialogParameters;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.app.forkandknife.AccountManageActivity;
import com.app.forkandknife.R;
import com.app.forkandknife.global.BaseFragment;
import com.app.forkandknife.global.Constant;
import com.app.forkandknife.global.GPSTracker;
import com.app.forkandknife.global.Utils;
import com.app.forkandknife.model.EventBusMessage;
import com.app.forkandknife.model.LoginDataModel;
import com.app.forkandknife.model.LoginRequest;
import com.app.forkandknife.model.RestOwnerDate;
import com.app.forkandknife.model.UserData;
import com.app.forkandknife.model.UserInfo;
import com.app.forkandknife.restaurant.activity.LoginActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.app.forkandknife.restaurant.activity.RestaurantActivity;
import com.app.forkandknife.user.activity.MainActivity;
import com.app.forkandknife.webservice.RestClient;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.LOCATION_SERVICE;
import static android.support.constraint.Constraints.TAG;
import static com.facebook.FacebookSdk.getApplicationContext;


public class LoginFragment extends BaseFragment {

    @BindView(R.id.tv_forgot_password)
    TextView tvForgotPassword;

    @BindView(R.id.tv_new_account)
    TextView tvNewAccount;

    @BindView(R.id.editText_mobile)
    EditText editTextMobile;

    @BindView(R.id.editText_password)
    EditText editTextPassword;

    GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 9001;

    private CallbackManager mCallbackManager;

    @BindView(R.id.button_facebook_login)
    LoginButton loginButton;

    String id = "";

    SharedPreferences preferences;

    String fbUserName = "";
    String fbUserEmail = "";
    String fbId = "";
    String fbAccessToken = null;

    LoginDataModel loginDataModel = new LoginDataModel();

    ProgressDialog progressDialog;
    GPSTracker gps;
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 1001;
    private static final int MY_PERMISSIONS_REQUEST_COARSE = 1002;
    private static final String TAG = "MapsActivity";
    private String[] permissions = new String[] {android.Manifest.permission.ACCESS_FINE_LOCATION};
    private String[] coarse_permissions = new String[] {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
    boolean isMsgShown = false;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View containerView=inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this,containerView);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.loading));

        printKeyHash();


        // Initialize the MapSDK
        MapsInitializer.initialize(getApplicationContext());

        // Initialize the Google SignIn
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);

        // Initialize the Facebook SDK
        FacebookSdk.sdkInitialize(getApplicationContext());
        initFacebookSDK();

        //Check location permission
        checkPermissions();

        editTextMobile.setText("966");
        Selection.setSelection(editTextMobile.getText(), editTextMobile.getText().length());

        setPrefixOnMobile();

        // Load user data from preference
        loadUserData();

        return  containerView;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_COARSE: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    getCurrentLocation();
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(getActivity(), "Permission denied to access your location", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }

    }

    private void loadUserData(){
        preferences = getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", android.content.Context.MODE_PRIVATE);
        String language  = preferences.getString("Language","en");
        String isLoggedIn = preferences.getString("login","0");

        if (isLoggedIn.equals("1"))
        {
            UserInfo.getInstance().setLoginDataModel(new LoginDataModel());
            UserInfo.getInstance().getLoginDataModel().setId((int)Integer.parseInt(preferences.getString("id","0")));
            UserInfo.getInstance().getLoginDataModel().setAccountType(Integer.parseInt(preferences.getString("account_type","0")));
            UserInfo.getInstance().getLoginDataModel().setDeviceType(Integer.parseInt(preferences.getString("device_type","0")));
            UserInfo.getInstance().getLoginDataModel().setPoints(Integer.parseInt(preferences.getString("points","0")));
            UserInfo.getInstance().getLoginDataModel().setStatus(Integer.parseInt(preferences.getString("status","0")));
            UserInfo.getInstance().getLoginDataModel().setUserType(Integer.parseInt(preferences.getString("user_type","1")));
            UserInfo.getInstance().getLoginDataModel().setAccessToken(preferences.getString("access_token",""));
            UserInfo.getInstance().getLoginDataModel().setEmail(preferences.getString("email",""));
            UserInfo.getInstance().getLoginDataModel().setMobile(preferences.getString("mobile",""));
            UserInfo.getInstance().getLoginDataModel().setName(preferences.getString("name",""));
            UserInfo.getInstance().getLoginDataModel().setUserImage(preferences.getString("image",""));
            if (language.equals("ar"))
            {
                Locale locale = new Locale("ar");
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getApplicationContext().getResources().updateConfiguration(config, getApplicationContext().getResources().getDisplayMetrics());
                Constant.language = 1;
            }else {
                Locale locale = new Locale("en");
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getApplicationContext().getResources().updateConfiguration(config, getApplicationContext().getResources().getDisplayMetrics());
                Constant.language = 0;
            }

            Intent intent = new Intent(getActivity(), MainActivity.class);
            startActivity(intent);
            getActivity().finish();
        }
        else if (isLoggedIn.equals("2"))
        {
            Constant.isRestLoggedin = true;
            UserInfo.getInstance().setRestOwnerDate(new RestOwnerDate());
            UserInfo.getInstance().getRestOwnerDate().setId((int)Integer.parseInt(preferences.getString("id","0")));
            UserInfo.getInstance().getRestOwnerDate().setAccountType(Integer.parseInt(preferences.getString("account_type","0")));
            UserInfo.getInstance().getRestOwnerDate().setStatus(Integer.parseInt(preferences.getString("status","0")));
            UserInfo.getInstance().getRestOwnerDate().setUserType(Integer.parseInt(preferences.getString("user_type","1")));
            UserInfo.getInstance().getRestOwnerDate().setAccessToken(preferences.getString("access_token",""));
            UserInfo.getInstance().getRestOwnerDate().setEmail(preferences.getString("email",""));
            UserInfo.getInstance().getRestOwnerDate().setName(preferences.getString("name",""));
            UserInfo.getInstance().getRestOwnerDate().setUserImage(preferences.getString("image",""));
            if (language.equals("ar"))
            {
                Locale locale = new Locale("ar");
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getApplicationContext().getResources().updateConfiguration(config, getApplicationContext().getResources().getDisplayMetrics());
                Constant.language = 1;
            }else {
                Locale locale = new Locale("en");
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getApplicationContext().getResources().updateConfiguration(config, getApplicationContext().getResources().getDisplayMetrics());
                Constant.language = 0;

            }
            Intent intent = new Intent(getActivity(),RestaurantActivity.class);
            startActivity(intent);
            getActivity().finish();
        }
    }

    @OnClick(R.id.MainLayout)
    public void onMainLayoutClicked(){
        hideKeyboard();
    }

    // Check if location permission is granted
    private void checkPermissions(){
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                requestPermissions(coarse_permissions, MY_PERMISSIONS_REQUEST_COARSE);
            }
        } else { // Permission granted
            getCurrentLocation();
        }
    }

    // get the current user location
    public void getCurrentLocation() {
        // Permission has already been granted
        gps = new GPSTracker(getApplicationContext(), getActivity());

        // Check if GPS enabled
        if (gps.canGetLocation()) {
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();

            UserInfo.getInstance().setLatitude(String.valueOf(latitude));
            UserInfo.getInstance().setLongtitude(String.valueOf(longitude));
        }
        else {
            // Can't get location.
            // GPS or network is not enabled.
            // Ask user to enable GPS/network in settings.
            gps.showSettingsAlert();
        }
    }


    private void checkLocationPermissionsAndCallAPI(){

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            Toast.makeText(getActivity(),getString(R.string.error_access_location),Toast.LENGTH_LONG).show();
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                requestPermissions(coarse_permissions , MY_PERMISSIONS_REQUEST_COARSE);
            } else {
                // No explanation needed, we can request the permission.
                requestPermissions(coarse_permissions , MY_PERMISSIONS_REQUEST_COARSE);
            }
        } else {
            // Check if GPS enabled
            if (gps.canGetLocation()) {
                progressDialog.show();

                if (Constant.token.isEmpty())
                {
                    Constant.token = preferences.getString("token","");
                }

                LoginRequest.getInstance().setLatitude(String.valueOf(UserInfo.getInstance().getLatitude()));
                LoginRequest.getInstance().setLongtitude(String.valueOf(UserInfo.getInstance().getLongtitude()));
                LoginRequest.getInstance().setToken(Constant.token);
                LoginRequest.getInstance().setUserType(1);

                RestClient.getInstance().initRestClient(RestClient.API_BASE_URL).login(LoginRequest.getInstance()).enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                  //'      Log.d("3e434r3434",response.body().toString());
                        JsonObject resultObj = response.body();
                        progressDialog.dismiss();
                        if (response.code()==200){
                            JsonObject userJson = resultObj.getAsJsonObject("data");
                            Gson gson = new Gson();
                            Type type = new TypeToken<LoginDataModel>() {}.getType();
                            loginDataModel = gson.fromJson(userJson,type);
                            LoginDataModel.getInstance().setUserType(loginDataModel.getUserType());
                            Constant.loginDataMode = loginDataModel;
                            UserInfo.getInstance().setLoginDataModel(loginDataModel);
                            Constant.selectedPoint = UserInfo.getInstance().getLoginDataModel().getPoints();
                            UserInfo.getInstance().getLoginDataModel().setMobile(editTextMobile.getText().toString().trim());

                            EventBus.getDefault().post(new EventBusMessage(EventBusMessage.MessageType.TO_VERIFY_SMS_FRAGMENT));
                        }else if (response.code() == 422){
                            if (!isMsgShown)
                            {
                                Toast.makeText(getActivity(),getActivity().getString(R.string.invalid_mobile_pass),Toast.LENGTH_LONG).show();
                            }else {
                                isMsgShown = false;
                            }

                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(),t.getLocalizedMessage().toString(),Toast.LENGTH_LONG).show();

                    }
                });

                // \n is for new line
            } else {
                // Can't get location.
                // GPS or network is not enabled.
                // Ask user to enable GPS/network in settings.
                gps.showSettingsAlert();
            }
        }
    }



    private void printKeyHash() {
        // Add code to print out the key hash
        try {
            PackageInfo info = getApplicationContext().getPackageManager().getPackageInfo("com.theappchief.forkandknife", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("KeyHash:", e.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("KeyHash:", e.toString());
        }
    }

    private void  setPrefixOnMobile(){

        editTextMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().startsWith("966")){
//                    editTextMobile.setText("966");
//                    Selection.setSelection(editTextMobile.getText(), editTextMobile.getText().length());
                }
            }
        });
    }

    private void initFacebookSDK() {

        mCallbackManager = CallbackManager.Factory.create();
        loginButton.setReadPermissions("email", "public_profile");
        loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);


                fbAccessToken = loginResult.getAccessToken().getToken();
                Constant.fbToken = fbAccessToken;
                Log.d(TAG, "Facebook : accessToken = " + fbAccessToken);

                // Show the loading progressBar

                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        Log.d(TAG, "*** Facebook Login result = " + response.toString());
                        try {
                            // Facebook Id
                            if (object.has("id")) {
                                fbId = object.getString("id");
                            }

                            // User name
                            if (object.has("name")) {
                                fbUserName = object.getString("name");
                                UserInfo.getInstance().setName(fbUserName);
                            }

                            // User email
                            if (object.has("email")) {
                                fbUserEmail = object.getString("email");
                                UserInfo.getInstance().setEmail(fbUserEmail);
                            }
                            EventBus.getDefault().post(new EventBusMessage(EventBusMessage.MessageType.TO_REGISTER_FRAGMENT));

                            // Go to Register page with full name & email

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
            }
        });
    }


    @OnClick(R.id.tv_new_account)
    public void onNewAccountClicked(){

        EventBus.getDefault().post(new EventBusMessage(EventBusMessage.MessageType.TO_REGISTER_FRAGMENT));
    }

    @OnClick(R.id.tv_forgot_password)
    public void onForgotPasswordClicked(){
        EventBus.getDefault().post(new EventBusMessage(EventBusMessage.MessageType.TO_FORGOT_PASSOWRD_FRAGMENT));
    }

    @OnClick(R.id.iv_facebook)
    public void onFaceBookClicked(){
        loginButton.performClick();

    }

    @OnClick(R.id.iv_restaurant)
    public void onRestaurantLoginClicked(){
        Intent intent = new Intent(getActivity(),LoginActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    @OnClick(R.id.relativeLayout_login)
    public void onLoginClicked(){
        isMsgShown = false;
        if (isAllValid()) {
            LoginRequest.getInstance().setAccountType(0);
            LoginRequest.getInstance().setMobile(editTextMobile.getText().toString().trim());
            LoginRequest.getInstance().setDeviceType(2);
            LoginRequest.getInstance().setPassword(editTextPassword.getText().toString().trim());
            LoginRequest.getInstance().setUserType(1);
            checkLocationPermissionsAndCallAPI();

        }
    }

    private boolean isAllFilled(){
        if (!editTextMobile.getText().toString().trim().isEmpty() && !editTextPassword.getText().toString().trim().isEmpty() &&isPasswordLengthRight() )
            return  true;
        else if (editTextMobile.getText().toString().trim().length()<4){
            if (!isMsgShown)
            {
                Toast.makeText(getActivity(),getActivity().getString(R.string.enter_phone),Toast.LENGTH_SHORT).show();
                isMsgShown = true;
            }

            return false;
        }else {
            if (!isMsgShown)
            {
                Toast.makeText(getActivity(),getActivity().getString(R.string.enter_password),Toast.LENGTH_SHORT).show();
                isMsgShown = true;
            }
            return false;

        }
    }


    private boolean isAllValid(){
        if (isAllFilled() && Utils.isMobileValid(editTextMobile.getText().toString().trim()))
        {
            return true;
        }else if (!isPasswordLengthRight()){
            if (!isMsgShown)
            {
                Toast.makeText(getActivity(),getActivity().getString(R.string.MSG_INVALID_PASSWORD_LENGTH),Toast.LENGTH_SHORT).show();
                isMsgShown = true;
            }

            return false;
        }else if (!Utils.isEmailValid(editTextMobile.getText().toString().trim()))
        {
            if (!isMsgShown)
            {
                Toast.makeText(getActivity(),getActivity().getString(R.string.enter_phone),Toast.LENGTH_SHORT).show();
                isMsgShown = true;
            }
            return false;

        }else {
            return false;
        }
    }
    private boolean isPasswordLengthRight(){
        if (editTextPassword.getText().toString().trim().length()<4)
        {
            return false;
        }else {
            return true;
        }
    }

    @OnClick(R.id.iv_google)
    public void onGoogleClicked(){
        // Configure Google Sign In
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onResume() {
        super.onResume();

        if(getView() == null){
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    // handle back button's click listener

                    return true;
                }
                return false;
            }
        });
    }

    // Handle the GoogleSignIn
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            String email = account.getEmail();
            String fullname = account.getDisplayName();

            // Go to Register page with email & user name

        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());

            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);

            handleSignInResult(task);
            if (task.isSuccessful())
            {
                UserInfo.getInstance().setEmail(task.getResult().getEmail());
                UserInfo.getInstance().setName(task.getResult().getGivenName() + " " + task.getResult().getFamilyName());
                EventBus.getDefault().post(new EventBusMessage(EventBusMessage.MessageType.TO_REGISTER_FRAGMENT));
            }

        }

        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
