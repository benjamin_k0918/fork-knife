package com.app.forkandknife.user.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.forkandknife.AccountManageActivity;
import com.app.forkandknife.model.UserData;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;
import com.app.forkandknife.R;
import com.app.forkandknife.adapter.ImageSliderAdapter;
import com.app.forkandknife.global.BaseFragment;
import com.app.forkandknife.global.Constant;
import com.app.forkandknife.model.ImageModel;
import com.app.forkandknife.model.RestaurantModel;
import com.app.forkandknife.model.UserInfo;
import com.app.forkandknife.service.PicassoImageLoadingService;
import com.app.forkandknife.webservice.RestClient;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ss.com.bannerslider.Slider;

public class ReservationDetailFragment extends BaseFragment {


    @BindView(R.id.carouselView)
    CarouselView carouselView;

    @BindView(R.id.iv_rating)
    SimpleRatingBar ratingBar;

    @BindView(R.id.tv_name)
    TextView tvName;

    @BindView(R.id.tv_guest_number)
    TextView tvGuestNumber;

    @BindView(R.id.tv_loaction)
    TextView tvLocation;

    @BindView(R.id.tv_date)
    TextView tvDate;

    @BindView(R.id.tv_distance)
    TextView tvDistance;

    @BindView(R.id.tv_discount)
    TextView tvDiscount;

    @BindView(R.id.tv_cancel)
    TextView tvCancel;

    @BindView(R.id.iv_cancel)
    ImageView  ivCancel;

    @BindView(R.id.iv_rate)
    ImageView ivRate;

    @BindView(R.id.tv_rate)
    TextView tvRate;



    private RequestOptions requestOptions = new RequestOptions();
    RestaurantModel restaurantSearchResultModel;

    String[] restImageList ;
    ArrayList<ImageModel> imageModels = new ArrayList<>();
  //  String imageBaseURl  = "http://139.59.125.227:3001/restaurant/image/";
  String imageBaseURl  = RestClient.API_URL+ "restaurant/image/";

    ProgressDialog progressDialog ;

    SharedPreferences preferences;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View containerView=inflater.inflate(R.layout.fragment_reservation_detail, container, false);
        ButterKnife.bind(this,containerView);

        progressDialog = new ProgressDialog(getActivity());


        requestOptions.placeholder(R.drawable.restaurant_logo);
        requestOptions.error(R.drawable.restaurant_logo);
        getAllRestaurantImage();
        if (!UserInfo.getInstance().getAllRestModel().isEmpty()){
            for (int i =0;i<UserInfo.getInstance().getAllRestModel().size();i++)
            {
                if (UserInfo.getInstance().getAllRestModel().get(i).getId().equals(UserInfo.getInstance().getReservationData().getRestaurantId())){
                    restaurantSearchResultModel = UserInfo.getInstance().getAllRestModel().get(i);
                    UserInfo.getInstance().setCurrentRestaurant(restaurantSearchResultModel);
                }
            }
        }

        Location loc1 = new Location("");
        if (UserInfo.getInstance().getLatitude() != null && !UserInfo.getInstance().getLatitude().isEmpty())
        {
            loc1.setLatitude(Double.parseDouble(UserInfo.getInstance().getLatitude()));

        }
        if (UserInfo.getInstance().getLongtitude() != null && !UserInfo.getInstance().getLongtitude().isEmpty())
        {
            loc1.setLongitude(Double.parseDouble(UserInfo.getInstance().getLongtitude()));

        }

        Location loc2 = new Location("");
        loc2.setLatitude(Double.parseDouble(restaurantSearchResultModel.getLatitude()));
        loc2.setLongitude(Double.parseDouble(restaurantSearchResultModel.getLongitude()));
        float disinKm = loc1.distanceTo(loc2) / 1000;
        tvDistance.setText(String.format("%.1f", disinKm) + " km");



        ratingBar.setRating(restaurantSearchResultModel.getRating());
        tvName.setText(UserInfo.getInstance().getReservationData().getRestaurant().getName());
        tvGuestNumber.setText(String.valueOf(UserInfo.getInstance().getReservationData().getNumberOfPeople()));
        tvDate.setText(UserInfo.getInstance().getReservationData().getReservationDate());
        if (UserInfo.getInstance().getReservationData().getStatus()== 2 || UserInfo.getInstance().getReservationData().getStatus()== 3) {
            setAlpha(ivCancel,tvCancel);
        }
        else {
            setAlpha(ivRate,tvRate);
        }
//        tvDiscount.setText(getActivity().getString(R.string.sar,UserInfo.getInstance().getReservationData().getDiscount()));
        tvDiscount.setText(String.valueOf(UserInfo.getInstance().getReservationData().getDiscount()) + " " + getString(R.string.currency_sar));
        getLocationFromLatLng();

        return  containerView;
    }

    @OnClick(R.id.MainLayout)
    public void onMainLayoutClicked(){
        hideKeyboard();
    }


    private void setAlpha(ImageView view, TextView tv)
    {
        view.setAlpha(0.4f);
        tv.setAlpha(0.4f);
    }

    private void getAllRestaurantImage(){
        HashMap<String,Object> map =  new HashMap<>();
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();
        String auth = "Bearer " + UserInfo.getInstance().getLoginDataModel().getAccessToken();

        map.put("restaurantId",UserInfo.getInstance().getReservationData().getRestaurantId());
    //    map.put("token",UserInfo.getInstance().getLoginDataModel().getAccessToken());
        RestClient.getInstance().initRestClient(RestClient.API_URL).getAllRestaurantImage(auth,map).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject resultObj = response.body();
                progressDialog.dismiss();

                    if (response.code() == 200){
                        JsonArray userJson = resultObj.getAsJsonArray("data");
                        Gson gson = new Gson();
                        Type type = new TypeToken<List<ImageModel>>() {}.getType();
                        imageModels = gson.fromJson(userJson,type);
                        if (!imageModels.isEmpty()){
                            restImageList = new String[imageModels.size()];
                            for (int i = 0;i<imageModels.size();i++)
                            {
                                restImageList[i]= imageBaseURl +UserInfo.getInstance().getCurrentRestaurant().getId() + "/index/" + imageModels.get(i).getIndex();
                            }
                            carouselView.setImageListener(imageListener);
                            carouselView.setPageCount(restImageList.length);
                        }else {
                            restImageList = new String[1];
                            carouselView.setImageListener(imageListener);
                            carouselView.setPageCount(restImageList.length);
                        }

                    }
                    else if (response.code() == 401)
                    {
                        SharedPreferences preferences;
                        preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor =      preferences.edit();
                        editor.putString("login", "0");
                        editor.commit();
                        Constant.isReloaded = false;
                        if (!Constant.fbToken.isEmpty())
                        {
                            LoginManager.getInstance().logOut();
                        }
                        Intent intent = new Intent(getActivity(), AccountManageActivity.class);
                        startActivity(intent);

                        getActivity().finish();
                    }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            Glide.with(getActivity()).setDefaultRequestOptions(requestOptions).load(restImageList[position]).into(imageView);

        }
    };

    private void getLocationFromLatLng(){
        Geocoder geocoder;

        geocoder = new Geocoder(getActivity(), Locale.getDefault());

        try {
            List<Address> addresses = geocoder.getFromLocation(Double.parseDouble(restaurantSearchResultModel.getLatitude()), Double.parseDouble(restaurantSearchResultModel.getLongitude()), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            if (addresses.size()>0)
            {
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();
                tvLocation.setText(address);
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void CancelReservation(){
        String auth = "Bearer " + UserInfo.getInstance().getLoginDataModel().getAccessToken();


            HashMap<String,Object> map = new HashMap<>();
            map.put("userId",UserInfo.getInstance().getLoginDataModel().getId());
            map.put("reservationId",UserInfo.getInstance().getReservationData().getId());
            RestClient.getInstance().initRestClient(RestClient.API_URL).cancelReservation(auth,map).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.code() == 200)
                    {
                        setFragment(new ReservationFragment());
                    }else if (response.code() == 401)
                    {
                        SharedPreferences preferences;
                        preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor =      preferences.edit();
                        editor.putString("login", "0");
                        editor.commit();
                        Constant.isReloaded = false;
                        if (!Constant.fbToken.isEmpty())
                        {
                            LoginManager.getInstance().logOut();
                        }
                        Intent intent = new Intent(getActivity(), AccountManageActivity.class);
                        startActivity(intent);

                        getActivity().finish();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                }
            });


    }


    @OnClick(R.id.layout_location)
    public void onLocationClicked(){
        String uri = String.format(Locale.ENGLISH, "geo:%f,%f",Double.parseDouble(restaurantSearchResultModel.getLatitude()) ,Double.parseDouble(restaurantSearchResultModel.getLongitude()));
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        getActivity().startActivity(intent);
    }

    @OnClick(R.id.layout_cancel)
    public void onLayoutCancel(){
            showCalcelDialog();
    }

    public void showCalcelDialog(){

        if (UserInfo.getInstance().getReservationData().getStatus()!= 2 && UserInfo.getInstance().getReservationData().getStatus()!= 3) {

            final Dialog dialog = new Dialog(getActivity());
            final View view  = getActivity().getLayoutInflater().inflate(R.layout.dialog_cancel_reservation, null);

            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(view);

            Window window = dialog.getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();

            wlp.gravity = Gravity.BOTTOM;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
            window.setAttributes(wlp);

            Button btnYes = view.findViewById(R.id.btn_yes);
            Button btnNo = view.findViewById(R.id.btr_no);
            btnNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            btnYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CancelReservation();
                    dialog.dismiss();

                }
            });
            dialog.show();
        }


    }

    @OnClick(R.id.layout_rate)
    public void onLayoutRate(){
        if (UserInfo.getInstance().getReservationData().getStatus()== 2 || UserInfo.getInstance().getReservationData().getStatus()== 3) {
            setFragment(new RateRestaurantFragment());
        }
    }

    private void setFragment(Fragment fragment){
        FragmentManager fragmentManager= getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =   fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.MainFrame,fragment,null);
        fragmentTransaction.commit();
    }

    @OnClick(R.id.iv_back)
    public void onBackClicked(){
        setFragment(new ReservationFragment());
    }

    @Override
    public void onResume() {
        super.onResume();

        if(getView() == null){
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    // handle back button's click listener
                    setFragment(new ReservationFragment());

                    return true;
                }
                return false;
            }
        });
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
