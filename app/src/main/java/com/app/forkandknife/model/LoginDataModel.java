package com.app.forkandknife.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginDataModel {
    @SerializedName("mobile")
    @Expose
    private String mobile = "";
    @SerializedName("email")
    @Expose
    private String email = "";
    @SerializedName("status")
    @Expose
    private Integer status = 1;
    @SerializedName("userType")
    @Expose
    private Integer userType = 2;
    @SerializedName("userImage")
    @Expose
    private String userImage;
    @SerializedName("points")
    @Expose
    private Integer points = 0;
    @SerializedName("deviceType")
    @Expose
    private Integer deviceType = 2;
    @SerializedName("name")
    @Expose
    private String name = "";
    @SerializedName("id")
    @Expose
    private Integer id =0;
    @SerializedName("accountType")
    @Expose
    private Integer accountType = 1;
    @SerializedName("accessToken")
    @Expose
    private String accessToken = "";

    static LoginDataModel instance = null;

    public static LoginDataModel getInstance(){
        if (instance == null){
            instance = new LoginDataModel();
        }
        return instance;
    }



    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Integer getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(Integer deviceType) {
        this.deviceType = deviceType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAccountType() {
        return accountType;
    }

    public void setAccountType(Integer accountType) {
        this.accountType = accountType;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
