package com.app.forkandknife.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserData implements Serializable{
/*
    var username : String!
    var password : String!
    var email : String!
    var userId : Int!
    var mobile : String!
    var type : UserType!
    var points = 0
    var restaurantInfo : RestaurantData?
    var userImage = ""*/

    @SerializedName("name")
    String username;

 //   @SerializedName("")
    String password;

    @SerializedName("email")
    String email;

    @SerializedName("id")
    int userId;

    @SerializedName("mobile")
    String mobile;

    @SerializedName("userType")
    int type;

    @SerializedName("points")
    int points  = 0;
    String userImage;

    public UserData() {
    }

    public UserData(String username, String password, String email, int userId, String mobile, int type, int points, String userImage) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.userId = userId;
        this.mobile = mobile;
        this.type = type;
        this.points = points;
        this.userImage = userImage;
    }

    static UserData instance = null;

    public static UserData getInstance(){
        if (instance == null){
            instance = new UserData();
        }
        return instance;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public int getUserId() {
        return userId;
    }

    public String getMobile() {
        return mobile;
    }

    public int getType() {
        return type;
    }

    public int getPoints() {
        return points;
    }


    public String getUserImage() {
        return userImage;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setPoints(int points) {
        this.points = points;
    }



    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }
}
