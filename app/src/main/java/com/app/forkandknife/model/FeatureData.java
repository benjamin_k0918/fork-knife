package com.app.forkandknife.model;

public class FeatureData {

    String name;
    boolean isSelected;

    public FeatureData(String name, boolean isSelected) {
        this.name = name;
        this.isSelected = isSelected;
    }

    public FeatureData(String name) {
        this.name = name;
        this.isSelected = false;
    }

    public String getName() {
        return name;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
