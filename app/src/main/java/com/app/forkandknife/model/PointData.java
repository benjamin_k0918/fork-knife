package com.app.forkandknife.model;

import com.google.gson.annotations.SerializedName;

public class PointData {

    @SerializedName("id")
    int id ;
    @SerializedName("value")
    int value;
    @SerializedName("status")
    int status;
    @SerializedName("description")
    String description;
    @SerializedName("arabicDescription")
    String arabicDescription;

    public PointData() {
    }

    public PointData(int value, String description) {
        this.value = value;
        this.description = description;
    }

    public PointData(int id, int value, int status, String description) {
        this.id = id;
        this.value = value;
        this.status = status;
        this.description = description;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public int getValue() {
        return value;
    }

    public int getStatus() {
        return status;
    }

    public String getDescription() {
        return description;
    }

    public String getArabicDescription() {
        return arabicDescription;
    }

    public void setArabicDescription(String arabicDescription) {
        this.arabicDescription = arabicDescription;
    }
}
