package com.app.forkandknife.model;

import java.util.ArrayList;
import java.util.HashMap;

public class UserInfo {
    String mobile = "";
    String latitude = "";
    String password = "";
    String longtitude = "";
    String email = "";
    String name = "";
    int userType= 1;
    int deviceType = 2;

    ArrayList<PointData> pointData;

    UserData userData;

    LoginDataModel loginDataModel;
    ArrayList<RestaurantModel> topRestModels= new ArrayList<>();
    ArrayList<RestaurantModel> allRestModel = new ArrayList<>();
    RestaurantModel currentRestaurant = new RestaurantModel();
    ArrayList<CuisineData> cuisineDataArrayList = new ArrayList<>();
    RestOwnerDate restOwnerDate = new RestOwnerDate();

    HashMap <String,Object> newRestMap = new HashMap<>();

    public HashMap<String, Object> getNewRestMap() {
        return newRestMap;
    }

    public void setNewRestMap(HashMap<String, Object> newRestMap) {
        this.newRestMap = newRestMap;
    }

    RestaurantReservationModel currentRestReservation = new RestaurantReservationModel();

    public RestOwnerDate getRestOwnerDate() {
        return restOwnerDate;
    }

    public void setRestOwnerDate(RestOwnerDate restOwnerDate) {
        this.restOwnerDate = restOwnerDate;
    }

    public ArrayList<CuisineData> getCuisineDataArrayList() {
        return cuisineDataArrayList;
    }

    public void setCuisineDataArrayList(ArrayList<CuisineData> cuisineDataArrayList) {
        this.cuisineDataArrayList = cuisineDataArrayList;
    }

    public RestaurantModel getCurrentRestaurant() {
        return currentRestaurant;
    }

    public void setCurrentRestaurant(RestaurantModel currentRestaurant) {
        this.currentRestaurant = currentRestaurant;
    }

    ReservationData reservationData = new ReservationData();

    public ReservationData getReservationData() {
        return reservationData;
    }


    public void setReservationData(ReservationData reservationData) {
        this.reservationData = reservationData;
    }

    public ArrayList<RestaurantModel> getAllRestModel() {
        return allRestModel;
    }


    public void setAllRestModel(ArrayList<RestaurantModel> allRestModel) {
        this.allRestModel = allRestModel;
    }

    public ArrayList<RestaurantModel> getTopRestModels() {
        return topRestModels;
    }

    public void setTopRestModels(ArrayList<RestaurantModel> topRestModels) {
        this.topRestModels = topRestModels;
    }

    public LoginDataModel getLoginDataModel() {
        return loginDataModel;
    }

    public void setLoginDataModel(LoginDataModel loginDataModel) {
        this.loginDataModel = loginDataModel;
    }

    public UserData getUserData() {
        return userData;
    }


    public void setUserData(UserData userData) {
        this.userData = userData;
    }

    public int getUserType() {
        return userType;
    }

    public ArrayList<PointData> getPointData() {
        return pointData;
    }

    public void setPointData(ArrayList<PointData> pointData) {
        this.pointData = pointData;
    }

    public int getDeviceType() {
        return deviceType;
    }


    public void setUserType(int userType) {
        this.userType = userType;
    }

    public void setDeviceType(int deviceType) {
        this.deviceType = deviceType;
    }



    public UserInfo() {
    }

    public UserInfo(String latitude, String password, String longtitude, String email, String mobile, String name, int type) {
        this.latitude = latitude;
        this.password = password;
        this.longtitude = longtitude;
        this.email = email;
        this.mobile = mobile;
        this.name = name;
        this.userType = type;
    }

    static UserInfo instance = null;

    public static UserInfo getInstance(){
        if (instance == null){
            instance = new UserInfo();
        }
        return instance;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongtitude() {
        return longtitude;
    }

    public String getEmail() {
        return email;
    }

    public String getMobile() {
        return mobile;
    }

    public String getName() {
        return name;
    }

    public int getType() {
        return userType;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongtitude(String longtitude) {
        this.longtitude = longtitude;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(int type) {
        this.userType = type;
    }

    public static void setInstance(UserInfo instance) {
        UserInfo.instance = instance;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RestaurantReservationModel getCurrentRestReservation() {
        return currentRestReservation;
    }

    public void setCurrentRestReservation(RestaurantReservationModel currentRestReservation) {
        this.currentRestReservation = currentRestReservation;
    }
}
