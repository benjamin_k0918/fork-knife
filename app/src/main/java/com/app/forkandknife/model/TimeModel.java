package com.app.forkandknife.model;

public class TimeModel {
    String time;
    boolean selection;

    public TimeModel() {
    }

    public String getTime() {
        return time;
    }

    public TimeModel(String time, boolean selection) {
        this.time = time;
        this.selection = selection;
    }

    public boolean isSelection() {
            return selection;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setSelection(boolean selection) {
        this.selection = selection;
    }
}
