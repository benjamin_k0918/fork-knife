package com.app.forkandknife.model;

public class LimitationModel {
    int limit ;

    public LimitationModel(int limit) {
        this.limit = limit;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }
}
