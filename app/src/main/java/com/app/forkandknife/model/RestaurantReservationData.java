package com.app.forkandknife.model;

import org.json.JSONObject;

public class RestaurantReservationData {
/*    var _id:Int!
    var reservationDate : String!
    var userId:Int!
    var restaurantId:Int!
    var comment:String!
    var status:ReservationStatus!
    var numberOfPeople:Int!
    var points = 0
    var discount = 0
    var clientInfo:JSON!*/

    int id;
    String reservationDate;
    int userId;
    int restaurantId;
    String comment;
    int status;
    int numberOfPeople;
    int points;
    int discount;
    JSONObject clientInfo;

    public void setId(int id) {
        this.id = id;
    }

    public void setReservationDate(String reservationDate) {
        this.reservationDate = reservationDate;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setRestaurantId(int restaurantId) {
        this.restaurantId = restaurantId;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setNumberOfPeople(int numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public void setClientInfo(JSONObject clientInfo) {
        this.clientInfo = clientInfo;
    }


    public int getId() {
        return id;
    }

    public String getReservationDate() {
        return reservationDate;
    }

    public int getUserId() {
        return userId;
    }

    public int getRestaurantId() {
        return restaurantId;
    }

    public String getComment() {
        return comment;
    }

    public int getStatus() {
        return status;
    }

    public int getNumberOfPeople() {
        return numberOfPeople;
    }

    public int getPoints() {
        return points;
    }

    public int getDiscount() {
        return discount;
    }

    public JSONObject getClientInfo() {
        return clientInfo;
    }

    public RestaurantReservationData(int id, String reservationDate, int userId, int restaurantId, String comment, int status, int numberOfPeople, int points, int discount, JSONObject clientInfo) {
        this.id = id;
        this.reservationDate = reservationDate;
        this.userId = userId;
        this.restaurantId = restaurantId;
        this.comment = comment;
        this.status = status;
        this.numberOfPeople = numberOfPeople;
        this.points = points;
        this.discount = discount;
        this.clientInfo = clientInfo;

    }
}
