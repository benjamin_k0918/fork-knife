package com.app.forkandknife.model;

public class    ImageFileModel {
    String url;
    boolean selection;

    public ImageFileModel(String url, boolean selection) {
        this.url = url;
        this.selection = selection;
    }

    public String getUrl() {
        return url;
    }

    public boolean isSelection() {
        return selection;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setSelection(boolean selection) {
        this.selection = selection;
    }
}
