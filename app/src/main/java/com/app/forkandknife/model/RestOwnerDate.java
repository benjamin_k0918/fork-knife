package com.app.forkandknife.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RestOwnerDate {

    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("userType")
    @Expose
    private Integer userType;
    @SerializedName("userImage")
    @Expose
    private Object userImage;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("accountType")
    @Expose
    private Integer accountType;
    @SerializedName("restaurant")
    @Expose
    private Restaurant restaurant;
    @SerializedName("accessToken")
    @Expose
    private String accessToken;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public Object getUserImage() {
        return userImage;
    }

    public void setUserImage(Object userImage) {
        this.userImage = userImage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAccountType() {
        return accountType;
    }

    public void setAccountType(Integer accountType) {
        this.accountType = accountType;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
    public class Restaurant {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("loyaltyPoints")
        @Expose
        private Integer loyaltyPoints;
        @SerializedName("arabicName")
        @Expose
        private Object arabicName;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("mobile")
        @Expose
        private String mobile;
        @SerializedName("longitude")
        @Expose
        private String longitude;
        @SerializedName("latitude")
        @Expose
        private String latitude;
        @SerializedName("createdDate")
        @Expose
        private String createdDate;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("ownerId")
        @Expose
        private Integer ownerId;
        @SerializedName("personCost")
        @Expose
        private String personCost;
        @SerializedName("cost")
        @Expose
        private String cost;
        @SerializedName("parking")
        @Expose
        private Integer parking;
        @SerializedName("outdoorSeating")
        @Expose
        private Integer outdoorSeating;
        @SerializedName("acceptVisa")
        @Expose
        private Integer acceptVisa;
        @SerializedName("dressCode")
        @Expose
        private Integer dressCode;
        @SerializedName("allowSmoking")
        @Expose
        private Integer allowSmoking;
        @SerializedName("reviews")
        @Expose
        private Integer reviews;
        @SerializedName("rating")
        @Expose
        private Integer rating;
        @SerializedName("defaultImage")
        @Expose
        private String defaultImage;
        @SerializedName("openTime")
        @Expose
        private String openTime;
        @SerializedName("closeTime")
        @Expose
        private String closeTime;
        @SerializedName("saturday")
        @Expose
        private String saturday;
        @SerializedName("sunday")
        @Expose
        private String sunday;
        @SerializedName("monday")
        @Expose
        private String monday;
        @SerializedName("tuesday")
        @Expose
        private String tuesday;
        @SerializedName("wednesday")
        @Expose
        private String wednesday;
        @SerializedName("thursday")
        @Expose
        private String thursday;
        @SerializedName("friday")
        @Expose
        private String friday;
        @SerializedName("requestType")
        @Expose
        private Integer requestType;
        @SerializedName("cuisines")
        @Expose
        private List<Integer> cuisines = null;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Object getArabicName() {
            return arabicName;
        }

        public void setArabicName(Object arabicName) {
            this.arabicName = arabicName;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(String createdDate) {
            this.createdDate = createdDate;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public Integer getOwnerId() {
            return ownerId;
        }

        public void setOwnerId(Integer ownerId) {
            this.ownerId = ownerId;
        }

        public String getPersonCost() {
            return personCost;
        }

        public void setPersonCost(String personCost) {
            this.personCost = personCost;
        }

        public String getCost() {
            return cost;
        }

        public void setCost(String cost) {
            this.cost = cost;
        }

        public Integer getParking() {
            return parking;
        }

        public void setParking(Integer parking) {
            this.parking = parking;
        }

        public Integer getOutdoorSeating() {
            return outdoorSeating;
        }

        public void setOutdoorSeating(Integer outdoorSeating) {
            this.outdoorSeating = outdoorSeating;
        }

        public Integer getAcceptVisa() {
            return acceptVisa;
        }

        public void setAcceptVisa(Integer acceptVisa) {
            this.acceptVisa = acceptVisa;
        }

        public Integer getDressCode() {
            return dressCode;
        }

        public void setDressCode(Integer dressCode) {
            this.dressCode = dressCode;
        }

        public Integer getAllowSmoking() {
            return allowSmoking;
        }

        public void setAllowSmoking(Integer allowSmoking) {
            this.allowSmoking = allowSmoking;
        }

        public Integer getReviews() {
            return reviews;
        }

        public void setReviews(Integer reviews) {
            this.reviews = reviews;
        }

        public Integer getRating() {
            return rating;
        }

        public void setRating(Integer rating) {
            this.rating = rating;
        }

        public String getDefaultImage() {
            return defaultImage;
        }

        public Integer getLoyaltyPoints() {
            return loyaltyPoints;
        }

        public void setLoyaltyPoints(Integer loyaltyPoints) {
            this.loyaltyPoints = loyaltyPoints;
        }

        public void setDefaultImage(String defaultImage) {
            this.defaultImage = defaultImage;
        }

        public String getOpenTime() {
            return openTime;
        }

        public void setOpenTime(String openTime) {
            this.openTime = openTime;
        }

        public String getCloseTime() {
            return closeTime;
        }

        public void setCloseTime(String closeTime) {
            this.closeTime = closeTime;
        }

        public String getSaturday() {
            return saturday;
        }

        public void setSaturday(String saturday) {
            this.saturday = saturday;
        }

        public String getSunday() {
            return sunday;
        }

        public void setSunday(String sunday) {
            this.sunday = sunday;
        }

        public String getMonday() {
            return monday;
        }

        public void setMonday(String monday) {
            this.monday = monday;
        }

        public String getTuesday() {
            return tuesday;
        }

        public void setTuesday(String tuesday) {
            this.tuesday = tuesday;
        }

        public String getWednesday() {
            return wednesday;
        }

        public void setWednesday(String wednesday) {
            this.wednesday = wednesday;
        }

        public String getThursday() {
            return thursday;
        }

        public void setThursday(String thursday) {
            this.thursday = thursday;
        }

        public String getFriday() {
            return friday;
        }

        public void setFriday(String friday) {
            this.friday = friday;
        }

        public Integer getRequestType() {
            return requestType;
        }

        public void setRequestType(Integer requestType) {
            this.requestType = requestType;
        }

        public List<Integer> getCuisines() {
            return cuisines;
        }

        public void setCuisines(List<Integer> cuisines) {
            this.cuisines = cuisines;
        }

    }
}

