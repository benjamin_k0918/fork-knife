package com.app.forkandknife.model;

public class SmsVerifyModel {
    int userId;
    String value;

    public int getUserId() {
        return userId;
    }

    public String getValue() {
        return value;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
