package com.app.forkandknife.model;

public class LoginRequest {
    String mobile;
    String password;
    String latitude;
    String longtitude;
    int deviceType = 2;
    int userType = 1;
    int accountType = 1;

    String token = "";


    static LoginRequest instance = null;

    public static LoginRequest getInstance(){
        if (instance == null){
            instance = new LoginRequest();
        }
        return instance;
    }


    public LoginRequest() {
    }

    public LoginRequest(String mobile, String password, String latitude, String longtitude, int deviceType, int userType, int accountType) {
        this.mobile = mobile;
        this.password = password;
        this.latitude = latitude;
        this.longtitude = longtitude;
        this.deviceType = deviceType;
        this.userType = userType;
        this.accountType = accountType;
    }

    public String getMobile() {
        return mobile;
    }

    public String getPassword() {
        return password;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongtitude() {
        return longtitude;
    }

    public int getDeviceType() {
        return deviceType;
    }

    public int getUserType() {
        return userType;
    }

    public int getAccountType() {
        return accountType;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongtitude(String longtitude) {
        this.longtitude = longtitude;
    }

    public void setDeviceType(int deviceType) {
        this.deviceType = deviceType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public void setAccountType(int accountType) {
        this.accountType = accountType;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
