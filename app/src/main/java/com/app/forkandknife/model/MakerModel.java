package com.app.forkandknife.model;

import android.graphics.Bitmap;

import com.google.android.gms.maps.model.LatLng;

public class MakerModel {
    LatLng latLng;
    String title;
    Bitmap bitmap;

    public LatLng getLatLng() {
        return latLng;
    }

    public String getTitle() {
        return title;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
}
