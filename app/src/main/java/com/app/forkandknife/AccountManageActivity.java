package com.app.forkandknife;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.app.forkandknife.model.UserInfo;
import com.app.forkandknife.user.activity.MainActivity;
import com.app.forkandknife.user.fragment.ForgotPasswordFragment;
import com.app.forkandknife.user.fragment.LoginFragment;
import com.app.forkandknife.user.fragment.RegisterFragment;
import com.app.forkandknife.user.fragment.SmsVerifyFragment;
import com.app.forkandknife.global.Constant;
import com.app.forkandknife.model.EventBusMessage;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Locale;

public class AccountManageActivity extends AppCompatActivity {

    @BindView(R.id.MainFrame)
    FrameLayout mainFrame;

    @BindView(R.id.iv_back)
    ImageView ivBack;

    @BindView(R.id.tv_title)
    TextView tvTitle;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_manage);
        ButterKnife.bind(this);

        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        FirebaseInstanceId.getInstance().getToken();
/*        editor.putString("login", "1");
        editor.putInt("id",loginDataModel.getId());
        editor.putInt("account_type",loginDataModel.getAccountType());
        editor.putInt("device_type",loginDataModel.getDeviceType());
        editor.putInt("points",loginDataModel.getPoints());
        editor.putInt("status",loginDataModel.getStatus());
        editor.putInt("user_type",loginDataModel.getUserType());
        editor.putString("access_token",loginDataModel.getAccessToken());
        editor.putString("email",loginDataModel.getEmail());
        editor.putString("mobile",loginDataModel.getMobile());
        editor.putString("name",loginDataModel.getName());
        editor.putString("image",loginDataModel.getUserImage());*/

        EventBus.getDefault().register(this);
        setChildFragment(new LoginFragment(),this.getString(R.string.login), Constant.CurrentFragmentType.LoginFragment);


    }

    private void setChildFragment(Fragment fragment, String title,int FragmentType){
        tvTitle.setText(title);
        FragmentTransaction fragmentTranaction= getSupportFragmentManager().beginTransaction();
        fragmentTranaction.replace(R.id.MainFrame, fragment, null);
        fragmentTranaction.commit();
        if (FragmentType == Constant.CurrentFragmentType.LoginFragment)
        {
            ivBack.setVisibility(View.GONE);
        }else {
            ivBack.setVisibility(View.VISIBLE);
        }
        Constant.currentFragment = FragmentType;
    }

    @OnClick(R.id.iv_back)
    public void onBackClicked(){
        setChildFragment(new LoginFragment(),this.getString(R.string.login), Constant.CurrentFragmentType.LoginFragment);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void Event(EventBusMessage messageEvent) throws Exception {
        if (messageEvent != null) {
            int messageType = messageEvent.getMessageType();
            if (messageType == EventBusMessage.MessageType.TO_REGISTER_FRAGMENT) {
                setChildFragment(new RegisterFragment(),this.getString(R.string.newaccount),Constant.CurrentFragmentType.RegisterFragment);
            }else if (messageType == EventBusMessage.MessageType.TO_FORGOT_PASSOWRD_FRAGMENT) {
                setChildFragment(new ForgotPasswordFragment(),this.getString(R.string.forgot_password),Constant.CurrentFragmentType.ForgotPasswordFragment);
            }else if (messageType ==EventBusMessage.MessageType.TO_VERIFY_SMS_FRAGMENT)
            {
                setChildFragment(new SmsVerifyFragment(),this.getString(R.string.sms_verify),Constant.CurrentFragmentType.SMSVerifyFragment);
            }else if (messageType == EventBusMessage.MessageType.TO_LOGIN_FRAGMENT){
                setChildFragment(new LoginFragment(),this.getString(R.string.login),Constant.CurrentFragmentType.LoginFragment);

            }
        }
    }

    @Override
    protected void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.MainFrame);
        fragment.onActivityResult(requestCode, resultCode, data);
    }
}
